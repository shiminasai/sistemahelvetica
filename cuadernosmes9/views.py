# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Avg, Sum, F

from cuadernosmes9.models import *
from cuadernosmes0.choices_static import *

from collections import OrderedDict, Counter
import numpy as np
from itertools import chain

# Create your views here.

def _queryset_filtrado_cuaderno9(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['entrevistado__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['entrevistado__organizacion_apoyo'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]
    # for key, value in request.session.items():
    #     print('{} => {}'.format(key, value))

    return CuadernoMesNueve.objects.filter(**params)


def riqueza_sombra(request, template="cuadernonueve/sombra_riqueza.html"):
    filtro = _queryset_filtrado_cuaderno9(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        puntos = []
        for obj in filtro:
            cnt1 = Punto1.objects.filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).values_list('especies__id', flat=True)
            cnt2 = Punto2.objects.filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).values_list('especies__id', flat=True)
            cnt3 = Punto3.objects.filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).values_list('especies__id', flat=True)
            lista = list(cnt1) + list(cnt2) + list(cnt3)

            reducida_lista = list(set(lista))
            formula_riqueza = len(reducida_lista) #(len(reducida_lista) * 1000) / float(1890)
            puntos.append(formula_riqueza)

        # media arítmetica
        promedio2 = np.mean(puntos)
        # mediana
        mediana2 = np.median(puntos)
        # Desviación típica
        desviacion2 = np.std(puntos)
        #minimo
        try:
            minimo = min(puntos)
        except:
            minimo = 0
        #maximo
        try:
            maximo = max(puntos)
        except:
            maximo = 0

        #rangos
        grafo_riqueza = crear_rangos(request, puntos, minimo, maximo, step=2)

        years[anio[1]] = numero_parcelas,puntos,grafo_riqueza,promedio2,mediana2,desviacion2,minimo,maximo

    return render(request, template, locals())

def densidad_sombra(request, template="cuadernonueve/densidad_sombra.html"):
    filtro = _queryset_filtrado_cuaderno9(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        total_puntos = []
        for obj in filtro:
            total1 = Punto1.objects.exclude(especies__id__in=[11,60,88,91]).filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).aggregate(pi=Sum('arbol_peque'),
                                                                       mi=Sum('arbol_mediana'),
                                                                       gi=Sum('arbol_grande'), )
            try:
                suma_total1 = sum(total1.itervalues())
            except:
                pass

            total2 = Punto2.objects.exclude(especies__id__in=[11,60,88,91]).filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).aggregate(pi=Sum('arbol_peque'),
                                                                       mi=Sum('arbol_mediana'),
                                                                       gi=Sum('arbol_grande'), )
            try:
                suma_total2 = sum(total2.itervalues())
            except:
                pass

            total3 = Punto3.objects.exclude(especies__id__in=[11,60,88,91]).filter(cuaderno_nueve=obj,cuaderno_nueve__year = anio[0]).aggregate(pi=Sum('arbol_peque'),
                                                                       mi=Sum('arbol_mediana'),
                                                                       gi=Sum('arbol_grande'), )
            try:
                suma_total3 = sum(total3.itervalues())
            except:
                suma_total3 = 0

            try:
                gran_suma = suma_total1 + suma_total2 + suma_total3
            except:
                gran_suma = 0

            try:
                densidad_total = (gran_suma  * float(10000)) / float(2700)
            except:
                densidad_total = 0
            #print "gran suma: %s - encuesta: %s " % (densidad_total, obj)

            total_puntos.append(densidad_total)
        # media arítmetica
        promedio2 = np.mean(total_puntos)
        # mediana
        mediana2 = np.median(total_puntos)
        # Desviación típica
        desviacion2 = np.std(total_puntos)
        #minimo
        try:
            minimo = min(total_puntos)
        except:
            minimo = 0
        #maximo
        try:
            maximo = max(total_puntos)
        except:
            maximo = 0
        #rangos
        grafo_densidad = crear_rangos(request, total_puntos, minimo, maximo, step=15)

        years[anio[1]] = numero_parcelas,promedio2,mediana2,desviacion2,minimo,maximo,total_puntos,grafo_densidad

    return render(request, template, locals())

def dominancia_sombra(request, template="cuadernonueve/dominancia_sombra.html"):
    filtro = _queryset_filtrado_cuaderno9(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        CUANTO_ESPECIES = Especies.objects.exclude(id__in=[11,60,88]).count()
        dict_especie_todo = OrderedDict()

        for obj in Especies.objects.exclude(id__in=[11,60,88]):
            cnt_p1 = filtro.filter(punto1__especies=obj,year = anio[0]).aggregate(pi=Sum('punto1__arbol_peque'),
                                                                   mi=Sum('punto1__arbol_mediana'),
                                                                   gi=Sum('punto1__arbol_grande'))

            cnt_p2 = filtro.filter(punto2__especies=obj,year = anio[0]).aggregate(pi=Sum('punto2__arbol_peque'),
                                                                  mi=Sum('punto2__arbol_mediana'),
                                                                  gi=Sum('punto2__arbol_grande'))

            cnt_p3 = filtro.filter(punto3__especies=obj,year = anio[0]).aggregate(pi=Sum('punto3__arbol_peque'),
                                                                mi=Sum('punto3__arbol_mediana'),
                                                                gi=Sum('punto3__arbol_grande'))

            dict_especie_todo[obj] = [cnt_p1,cnt_p2,cnt_p3]

        todo = {}
        SUMA_TOTAL_ESPECIE = 0
        for k, myLIst in dict_especie_todo.items():
            pe = [item['pi'] for item in myLIst if item['pi'] is not None]
            me = [item['mi'] for item in myLIst if item['mi'] is not None]
            ga = [item['gi'] for item in myLIst if item['gi'] is not None]
            suma_total = sum([sum(pe),sum(me),sum(ga)])
            if suma_total > 0:
                todo[k] = suma_total
                SUMA_TOTAL_ESPECIE += suma_total

        dominancia_todo = sorted(todo.iteritems(), key=lambda (k,v): (v,k), reverse=True)

        years[anio[1]] = numero_parcelas,CUANTO_ESPECIES,SUMA_TOTAL_ESPECIE,dominancia_todo

    return render(request, template, locals())

def dimensiones_sombra(request, template="cuadernonueve/dimenciones_especies_sombra.html"):
    filtro = _queryset_filtrado_cuaderno9(request)

    if request.GET.get('usos'):
        uso = request.GET['usos']
        MODELO_ESPECIES = Especies.objects.exclude(id__in=[11,60,88]).filter(tipo_uso__contains=uso)
    else:
        MODELO_ESPECIES = Especies.objects.exclude(id__in=[11,60,88])

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        altura_p1 = []
        diametro_p1 = []
        anchura_p1 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto1__especies=obj,year = anio[0]).count()
            cnt_p1 = filtro.filter(punto1__especies=obj,year = anio[0]).aggregate(pi=Sum('punto1__arbol_peque'),
                                                                   mi=Sum('punto1__arbol_mediana'),
                                                                   gi=Sum('punto1__arbol_grande'))
            if conteo > 0:
                for k,v in cnt_p1.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)

        altura_p2 = []
        diametro_p2 = []
        anchura_p2 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto2__especies=obj,year = anio[0]).count()
            cnt_p2 = filtro.filter(punto2__especies=obj,year = anio[0]).aggregate(pi=Sum('punto2__arbol_peque'),
                                                                   mi=Sum('punto2__arbol_mediana'),
                                                                   gi=Sum('punto2__arbol_grande'))
            if conteo > 0:
                for k,v in cnt_p2.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)

        altura_p3 = []
        diametro_p3 = []
        anchura_p3 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto3__especies=obj,year = anio[0]).count()
            cnt_p3 = filtro.filter(punto3__especies=obj,year = anio[0]).aggregate(pi=Sum('punto3__arbol_peque'),
                                                                   mi=Sum('punto3__arbol_mediana'),
                                                                   gi=Sum('punto3__arbol_grande'))
            if conteo > 0:
                for k,v in cnt_p3.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)

        try:
            altura_total = altura_p1 + altura_p2 + altura_p3
        except:
            altura_total = 0
        try:
            diametro_total = diametro_p1 + diametro_p2 + diametro_p3
        except:
            diametro_total = 0
        try:
            anchura_total = anchura_p1 + anchura_p2 + anchura_p3
        except:
            anchura_total = 0

        #con esto trabajo estan las listas completas
        todas_alturas = list(chain.from_iterable(altura_total))
        todas_diametro = list(chain.from_iterable(diametro_total))
        todas_anchura = list(chain.from_iterable(anchura_total))

        #promedio, rango, desviacion estandar, media de altura
        try:
            promedio_altura = np.mean(todas_alturas)
        except:
            promedio_altura = 0
        try:
            desviacion_altura = np.std(todas_alturas)
        except:
            desviacion_altura = 0
        try:
            media_altura = np.median(todas_alturas)
        except:
            media_altura = 0
        try:
            minimo_altura = min(todas_alturas)
            maximo_altura = max(todas_alturas)
        except:
            minimo_altura = 0
            maximo_altura = 0

        try:
            grafo_altura = crear_rangos(request, todas_alturas, minimo_altura, maximo_altura, step=3)
        except:
            grafo_altura = 0

        #promedio, rango, desviacion estandar, media de diametro
        try:
            promedio_diametro = np.mean(todas_diametro)
        except:
            promedio_diametro = 0
        try:
            desviacion_diametro = np.std(todas_diametro)
        except:
            desviacion_diametro = 0
        try:
            media_diametro = np.median(todas_diametro)
        except:
            media_diametro = 0
        try:
            minimo_diametro = min(todas_diametro)
            maximo_diametro = max(todas_diametro)
        except:
            minimo_diametro = 0
            maximo_diametro = 0

        try:
            grafo_diametro = crear_rangos(request, todas_diametro, minimo_diametro, maximo_diametro, step=16)
        except:
            grafo_diametro = 0

        #promedio, rango, desviacion estandar, media de anchura
        try:
            promedio_anchura = np.mean(todas_anchura)
        except:
            promedio_anchura = 0
        try:
            desviacion_anchura = np.std(todas_anchura)
        except:
            desviacion_anchura = 0
        try:
            media_anchura = np.median(todas_anchura)
        except:
            media_anchura = 0
        try:
            minimo_anchura = min(todas_anchura)
            maximo_anchura = max(todas_anchura)
        except:
            minimo_anchura = 0
            maximo_anchura = 0

        try:
            grafo_anchura = crear_rangos(request, todas_anchura, minimo_anchura, maximo_anchura, step=2)
        except:
            grafo_anchura = 0

        years[anio[1]] = [numero_parcelas,promedio_altura,desviacion_altura,media_altura,minimo_altura,maximo_altura,
                            promedio_diametro,desviacion_diametro,media_diametro,minimo_diametro,maximo_diametro,
                            promedio_anchura,desviacion_anchura,media_anchura,minimo_anchura,maximo_anchura,
                            grafo_altura,grafo_diametro,grafo_anchura]

    return render(request, template, locals())

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo
