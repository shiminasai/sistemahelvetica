# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-01-20 17:39
from __future__ import unicode_literals

from django.db import migrations, models
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cuadernosmes9', '0002_auto_20190118_1953'),
    ]

    operations = [
        migrations.AddField(
            model_name='especies',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='fotoEspecies'),
        ),
        migrations.AddField(
            model_name='especies',
            name='g_altura',
            field=models.FloatField(blank=True, null=True, verbose_name='Altura en (mt)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='g_ancho',
            field=models.FloatField(blank=True, null=True, verbose_name='Ancho copa en (mt)s'),
        ),
        migrations.AddField(
            model_name='especies',
            name='g_diametro',
            field=models.FloatField(blank=True, null=True, verbose_name='Diametro en (cm)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='m_altura',
            field=models.FloatField(blank=True, null=True, verbose_name='Altura en (mt)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='m_ancho',
            field=models.FloatField(blank=True, null=True, verbose_name='Ancho copa en (mt)s'),
        ),
        migrations.AddField(
            model_name='especies',
            name='m_diametro',
            field=models.FloatField(blank=True, null=True, verbose_name='Diametro en (cm)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='nombre_cientifico',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Nombre cientifico de la especie'),
        ),
        migrations.AddField(
            model_name='especies',
            name='p_altura',
            field=models.FloatField(blank=True, null=True, verbose_name='Altura en (mt)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='p_ancho',
            field=models.FloatField(blank=True, null=True, verbose_name='Ancho copa en (mt)s'),
        ),
        migrations.AddField(
            model_name='especies',
            name='p_diametro',
            field=models.FloatField(blank=True, null=True, verbose_name='Diametro en (cm)'),
        ),
        migrations.AddField(
            model_name='especies',
            name='tipo',
            field=models.IntegerField(blank=True, choices=[(b'A', b'Perennifolia'), (b'B', b'Caducifolia')], null=True),
        ),
        migrations.AddField(
            model_name='especies',
            name='tipo_uso',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, choices=[(b'A', b'Le\xc3\xb1a'), (b'B', b'Fruta'), (b'C', b'Madera'), (b'D', b'Sombra'), (b'E', b'Nutrientes ')], max_length=9, null=True, verbose_name='Tipo de uso'),
        ),
    ]
