from django.conf.urls import url
from .views import *

urlpatterns =  [
    url(r'^sombra/riqueza-sombra/', riqueza_sombra, name='riqueza-sombra'),
    url(r'^sombra/densidad-sombra/', densidad_sombra, name='densidad-sombra'),
    url(r'^sombra/dominancia-sombra/', dominancia_sombra, name='dominancia-sombra'),
    url(r'^sombra/dimensiones-sombra/', dimensiones_sombra, name='dimensiones-sombra'),
]
