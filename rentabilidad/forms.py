# -*- coding: utf-8 -*-
from django import forms
from lineabase.models import Entrevistados, Encuestadores, OrganizacionApoyo
from lugar.models import (Pais, Departamento,
                                                 Municipio, Comunidad)
from .models import EncuestaRentabilidad
from configuracion.models import LineaTiempoCostos

from dal import autocomplete

class EntrevistadoForm(forms.ModelForm):
    class Meta:
        model = EncuestaRentabilidad
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class EntrevistadosForm(forms.ModelForm):

    class Meta:
        model = Entrevistados
        fields = '__all__'
        widgets = {
                'departamento': autocomplete.ModelSelect2(url='depart-autocomplete',
                                                           forward=['pais']),
                 'municipio': autocomplete.ModelSelect2(url='municipio-autocomplete',
                                                           forward=['departamento']),
                 'comunidad': autocomplete.ModelSelect2(url='comunidad-autocomplete',
                                                           forward=['municipio']),
            }

def fecha_choice():
    years = []
    for en in EncuestaRentabilidad.objects.order_by('fecha').values_list('fecha', flat=True):
        years.append((en.year,en.year))
    return list(sorted(set(years)))

def ciclo_choice():
    ciclos = []
    for en in Ciclo.objects.all():
        ciclos.append((en.nombre,en.nombre))
    return list(sorted(set(ciclos)))

CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Mujer'),
    (2, 'Hombre')
)

CHOICE_TAMANO_PARCELA1 = (
    ('', '-------'),
    (1, 'Pequeña (0.1 a 2 mz)'),
    (2, 'Mediana (2.1 a 5 mz)'),
    (3, 'Grande (Más de 5 mz)'),
)

CHOICE_RIEGO1 = (
    ('', '-------'),
    (1, 'No hay riego'),
    (2, 'Riego de baja intensidad'),
    (3, 'Riego de alta intensidad'),
)

CHOICE_TIPO_SISTEMA_AGROFORESTAL1 = (
    ('', '-------'),
    (1, 'SAF Cacao Sencillo'),
    (2, 'SAF Cacao Diversificado'),
    (3, 'SAF Cacao + Café Sencillo'),
    (4, 'SAF Cacao + Café Diversificado'),
)

CHOICE_VALORACION_SUELO1 = (
    ('', '-------'),
    (1, 'Óptimo'),
    (2, 'Medio óptimo'),
    (3, 'No óptimo'),
)

CHOICE_TIPO_MANEJO1 = (
    ('', '-------'),
    (1, 'Orgánico'),
    (2, 'Orgánico-químico'),
    (3, 'Químico'),
)

CHOICE_INTENSIDAD_MANEJO1 = (
    ('', '-------'),
    (1, 'No intensivo'),
    (2, 'Medio intensivo'),
    (3, 'Intensivo'),
)

class ConsultaFichaForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ConsultaFichaForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
                                                                        'data-placeholder':'Escoge....', 'tabindex':'-1','area-hidden': 'true'})
        self.fields['pais'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['modelo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['parcela'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['riego'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['saf'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['valoracion'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['manejo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['intensidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['productor'].widget.attrs.update({'class': 'form-control'})
    fecha = forms.MultipleChoiceField(choices=fecha_choice(), label="Años", required=True)
    pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
    parcela = forms.ChoiceField(choices=CHOICE_TAMANO_PARCELA1,
                                                                    label="Tamaño de parcela", required=False)
    riego = forms.ChoiceField(choices=CHOICE_RIEGO1, required=False)
    saf = forms.ChoiceField(choices=CHOICE_TIPO_SISTEMA_AGROFORESTAL1,
                                                label="Tipo SAF", required=False)
    valoracion = forms.ChoiceField(choices=CHOICE_VALORACION_SUELO1,
                                                            label="Valoracion de suelo", required=False)
    manejo = forms.ChoiceField(choices=CHOICE_TIPO_MANEJO1, required=False)
    intensidad = forms.ChoiceField(choices=CHOICE_INTENSIDAD_MANEJO1, required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
    productor = forms.CharField(max_length=250, required=False)
    modelo = forms.ModelChoiceField(queryset=LineaTiempoCostos.objects.all(), required=True)
