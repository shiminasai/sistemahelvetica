# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User

from lugar.models import Pais, Departamento, Municipio, Comunidad
from lineabase.models import Encuestadores, Entrevistados
from cuadernosmes0.models import (ClonesVariedades,
                                                                    TiposMusaceas, EspeciesArbolesServicios,
                                                                    EspeciesFrutales,
                                                                    EspeciesMaderables)
from .choices_static import *
from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money
from configuracion.models import LineaTiempoCostos

# Create your models here.

@python_2_unicode_compatible
class EncuestaRentabilidad(models.Model):
    fecha = models.DateField()
    encuestador = models.ForeignKey(Encuestadores, on_delete=models.CASCADE,
                                                                    verbose_name='Técnico',
                                                                    related_name='encuestador_renta')
    entrevistado = models.ForeignKey(Entrevistados, on_delete=models.CASCADE,
                                                                    verbose_name='Productor',
                                                                    related_name='productor_renta')
    modelo = models.ForeignKey(LineaTiempoCostos, on_delete=models.CASCADE)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_renta')

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(EncuestaRentabilidad, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s' % (self.entrevistado.nombre)

    class Meta:
        verbose_name_plural = 'ENCUESTAS'

class Preguntas1(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    valoracion = models.IntegerField(choices=CHOICE_VALORACION_SUELO,
                                                            verbose_name='3. Valoración de suelo')
    manejo = models.IntegerField(choices=CHOICE_TIPO_MANEJO,
                                                            verbose_name='4. tipo de manejo')
    intensidad = models.IntegerField(choices=CHOICE_INTENSIDAD_MANEJO,
                                                            verbose_name='5. Intensidad de manejo')
    tamanio =models.IntegerField(choices=CHOICE_TAMANO_PARCELA,
                                                            verbose_name='6. Tamaño de parcela')

    class Meta:
        verbose_name_plural = 'Preguntas'


class Preguntas2(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    tipo_sistema = models.IntegerField(choices=CHOICE_TIPO_SISTEMA_AGROFORESTAL,
                                                            verbose_name='7. Tipo de sistema agroforestal')
    descripcion = models.IntegerField(choices=CHOICE_DESCRIPCION_SISTEMA_AGRO,
                                                            verbose_name='8. Descripción de sistema agroforestal')
    riego = models.IntegerField(choices=CHOICE_RIEGO,
                                                            verbose_name='9. Riego')

    class Meta:
        verbose_name_plural = 'Preguntas'

class Preguntas3(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    pendiente = models.IntegerField(choices=CHOICE_PENDIENTE)

    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        super(Preguntas3, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Área de parcela y inclinación'

# @python_2_unicode_compatible
# class CacaoClonesVariedad(models.Model):
#     nombre = models.CharField(max_length=250)

#     def __str__(self):
#         return u'%s' % (self.nombre)

#     class Meta:
#         verbose_name_plural = 'Cacao Clones o Variedad'

@python_2_unicode_compatible
class CafeClonesVariedad(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return u'%s' % (self.nombre)

    class Meta:
        verbose_name_plural = 'Café Clones o Variedad'
        ordering = ['nombre']

# @python_2_unicode_compatible
# class TipoMusaceas(models.Model):
#     nombre = models.CharField(max_length=250)

#     def __str__(self):
#         return u'%s' % (self.nombre)

#     class Meta:
#         verbose_name_plural = 'Tipo Musaceas'

# @python_2_unicode_compatible
# class EspeciesFrutales(models.Model):
#     nombre = models.CharField(max_length=250)

#     def __str__(self):
#         return u'%s' % (self.nombre)

#     class Meta:
#         verbose_name_plural = 'Especies de frutales'

# @python_2_unicode_compatible
# class EspeciesMaderables(models.Model):
#     nombre = models.CharField(max_length=250)

#     def __str__(self):
#         return u'%s' % (self.nombre)

#     class Meta:
#         verbose_name_plural = 'Especies Maderables'

# @python_2_unicode_compatible
# class ArbolesServicios(models.Model):
#     nombre = models.CharField(max_length=250)

#     def __str__(self):
#         return u'%s' % (self.nombre)

#     class Meta:
#         verbose_name_plural = 'Árboles de Servicios'

@python_2_unicode_compatible
class GranosBasicos(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return u'%s' % (self.nombre)

    class Meta:
        verbose_name_plural = 'Granos básicos'
        ordering = ['nombre']

class InventarioCacao(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(ClonesVariedades, on_delete=models.CASCADE)
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Cacao'

class InventarioCafe(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(CafeClonesVariedad, on_delete=models.CASCADE)
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Café'

class InventarioMusaceas(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(TiposMusaceas, on_delete=models.CASCADE, verbose_name='Tipo')
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Musaceas'

class InventarioFrutales(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(EspeciesFrutales, on_delete=models.CASCADE, verbose_name='Especies')
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Frutales'

class InventarioMaderables(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(EspeciesMaderables, on_delete=models.CASCADE, verbose_name='Especies')
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Maderables'

class InventarioArboles(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(EspeciesArbolesServicios, on_delete=models.CASCADE, verbose_name='Especies')
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Árboles de servicios'

class InventarioGranosBasicos(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    clon = models.ForeignKey(GranosBasicos, on_delete=models.CASCADE, verbose_name='Tipo')
    cantidad = models.FloatField()
    edad = models.FloatField()

    class Meta:
        verbose_name_plural = 'Granos básicos'

class ValorManoObra(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    valor = models.FloatField()
    valor_usd = models.FloatField(null=True, blank=True, editable=False)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.valor_usd = convert_money(Money(self.valor, str(self.get_moneda_display())), 'USD')
        super(ValorManoObra, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Valor de Mano de Obra'

#TODO:
class CacaoEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CacaoEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Cacao Establecimiento'

class CacaoDesarrollo(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CacaoDesarrollo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Cacao Desarrollo'

class CacaoProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CacaoProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Cacao producción'

class CosechaCacao(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_CACAO)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha de Cacao (Producción)'

#MUSACEAS

class MusaceasEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(MusaceasEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Musaceas Establecimiento'

class MusaceasProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(MusaceasProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Musaceas Producción'

class CosechaMusaceas(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_MUSACEAS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaMusaceas, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha de Musaceas (Producción)'

#COSTOS FRUTALES
class FrutalesEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(FrutalesEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Frutales Establecimiento'

class FrutalesDesarrollo(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(FrutalesDesarrollo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Frutales Desarrollo'

class FrutalesProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(FrutalesProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Frutales Producción'

class CosechaFrutales(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_FRUTALES)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaFrutales, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha de Frutales (Producción)'

#COSTOS MADERABLES

class MaderaEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(MaderaEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Maderable Establecimiento'

class MaderaDesarrollo(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(MaderaDesarrollo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Maderable Desarrollo'

class MaderaProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(MaderaProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Maderable Producción'

class CosechaMaderable(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_MADERABLES)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaMaderable, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha de Maderables (Raleo/Aprovechamiento)'

#ARBOLES DE SERVICIOS

class ArbolesServicioEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_ARBOLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(ArbolesServicioEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Árboles de servicios Establecimiento'

class ArbolesServicioDesarrollo(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_ARBOLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(ArbolesServicioDesarrollo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Árboles de servicios Desarrollo'

class ArbolesServicioProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_ARBOLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(ArbolesServicioProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Árboles de servicios Producción'

class CosechaArbolesServicios(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_ARBOLES)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaArbolesServicios, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha de Árboles de servicio (producción)'

#GRANOS BASICOS

class GranosBasicosProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_GRANOS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(GranosBasicosProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Granos basico producción'

class CosechaGranos(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_GRANOS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaGranos, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha Granos Básicos (producción)'

#COSTO CAFE

class CafeEstablecimiento(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CAFE_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CafeEstablecimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Café Establecimiento'

class CafeDesarrollo(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CAFE_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CafeDesarrollo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Café Desarrollo'

class CafeProduccion(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    frecuencia = models.IntegerField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CAFE_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_moneda_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_moneda_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_moneda_display())), 'USD')
        super(CafeProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Costo Café Producción'

class CosechaCafe(models.Model):
    encuesta = models.ForeignKey(EncuestaRentabilidad, on_delete=models.CASCADE)
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS_CAFE)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    vende = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)
    ingreso = models.FloatField('Ingreso por ventas', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        self.ingreso = self.cantidad * self.precio_usd
        super(CosechaCafe, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Cosecha Café (producción)'
