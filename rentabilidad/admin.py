# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
from .forms import *

class InlinePreguntas1(admin.TabularInline):
    model = Preguntas1
    extra = 1
    max_num = 1

class InlinePreguntas2(admin.TabularInline):
    model = Preguntas2
    extra = 1
    max_num = 1

class InlinePreguntas3(admin.TabularInline):
    model = Preguntas3
    extra = 1
    max_num = 1

class InlineInventarioCacao(admin.TabularInline):
    model = InventarioCacao
    extra = 1

class InlineInventarioCafe(admin.TabularInline):
    model = InventarioCafe
    extra = 1

class InlineInventarioMusaceas(admin.TabularInline):
    model = InventarioMusaceas
    extra = 1

class InlineInventarioFrutales(admin.TabularInline):
    model = InventarioFrutales
    extra = 1

class InlineInventarioMaderables(admin.TabularInline):
    model = InventarioMaderables
    extra = 1

class InlineInventarioArboles(admin.TabularInline):
    model = InventarioArboles
    extra = 1

class InlineInventarioGranosBasicos(admin.TabularInline):
    model = InventarioGranosBasicos
    extra = 1


class InlineValorManoObra(admin.TabularInline):
    model = ValorManoObra
    extra = 1
    max_num = 1

class InlineCacaoEstablecimiento(admin.TabularInline):
    model = CacaoEstablecimiento
    extra = 1

class InlineCacaoDesarrollo(admin.TabularInline):
    model = CacaoDesarrollo
    extra = 1

class InlineCacaoProduccion(admin.TabularInline):
    model = CacaoProduccion
    extra = 1

class InlineCosechaCacao(admin.TabularInline):
    model = CosechaCacao
    extra = 1

class InlineMusaceasEstablecimiento(admin.TabularInline):
    model = MusaceasEstablecimiento
    extra = 1

class InlineMusaceasProduccion(admin.TabularInline):
    model = MusaceasProduccion
    extra = 1

class InlineCosechaMusaceas(admin.TabularInline):
    model = CosechaMusaceas
    extra = 1

class InlineFrutalesEstablecimiento(admin.TabularInline):
    model = FrutalesEstablecimiento
    extra = 1

class InlineFrutalesDesarrollo(admin.TabularInline):
    model = FrutalesDesarrollo
    extra = 1

class InlineFrutalesProduccion(admin.TabularInline):
    model = FrutalesProduccion
    extra = 1

class InlineCosechaFrutales(admin.TabularInline):
    model = CosechaFrutales
    extra = 1

class InlineMaderaEstablecimiento(admin.TabularInline):
    model = MaderaEstablecimiento
    extra = 1

class InlineMaderaDesarrollo(admin.TabularInline):
    model = MaderaDesarrollo
    extra = 1

class InlineMaderaProduccion(admin.TabularInline):
    model = MaderaProduccion
    extra = 1

class InlineCosechaMaderable(admin.TabularInline):
    model = CosechaMaderable
    extra = 1

class InlineArbolesServicioEstablecimiento(admin.TabularInline):
    model = ArbolesServicioEstablecimiento
    extra = 1

class InlineArbolesServicioDesarrollo(admin.TabularInline):
    model = ArbolesServicioDesarrollo
    extra = 1

class InlineArbolesServicioProduccion(admin.TabularInline):
    model = ArbolesServicioProduccion
    extra = 1

class InlineCosechaArbolesServicios(admin.TabularInline):
    model = CosechaArbolesServicios
    extra = 1

class InlineGranosBasicosProduccion(admin.TabularInline):
    model = GranosBasicosProduccion
    extra = 1

class InlineCosechaGranos(admin.TabularInline):
    model = CosechaGranos
    extra = 1

class InlineCafeEstablecimiento(admin.TabularInline):
    model = CafeEstablecimiento
    extra = 1

class InlineCafeDesarrollo(admin.TabularInline):
    model = CafeDesarrollo
    extra = 1

class InlineCafeProduccion(admin.TabularInline):
    model = CafeProduccion
    extra = 1

class InlineCosechaCafe(admin.TabularInline):
    model = CosechaCafe
    extra = 1

class EncuestaAdmin(admin.ModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(EncuestaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(EncuestaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user',)
    list_display = ('entrevistado', 'fecha','get_sexo' ,'get_pais','encuestador',
                            'get_organizacion_apoya','user')
    search_fields = ['entrevistado__nombre']
    list_filter = ['year']
    inlines = [InlinePreguntas1,InlinePreguntas2,InlinePreguntas3,InlineInventarioCacao,
                    InlineInventarioCafe,InlineInventarioMusaceas,InlineInventarioFrutales,
                    InlineInventarioMaderables,InlineInventarioArboles,
                    InlineInventarioGranosBasicos,InlineValorManoObra,InlineCacaoEstablecimiento,
                    InlineCacaoDesarrollo,InlineCacaoProduccion,InlineCosechaCacao,
                    InlineMusaceasEstablecimiento,InlineMusaceasProduccion,
                    InlineCosechaMusaceas,InlineFrutalesEstablecimiento,InlineFrutalesDesarrollo,
                    InlineFrutalesProduccion,InlineCosechaFrutales,InlineMaderaEstablecimiento,
                    InlineMaderaDesarrollo,InlineMaderaProduccion,InlineCosechaMaderable,
                    InlineArbolesServicioEstablecimiento,InlineArbolesServicioDesarrollo,
                    InlineArbolesServicioProduccion,InlineCosechaArbolesServicios,
                    InlineGranosBasicosProduccion,InlineCosechaGranos,InlineCafeEstablecimiento,
                    InlineCafeDesarrollo,InlineCafeProduccion,InlineCosechaCafe]

    def get_pais(self, obj):
        return obj.entrevistado.pais
    get_pais.short_description = 'Pais'
    get_pais.admin_order_field = 'entrevistado__pais'

    def get_sexo(self, obj):
        return obj.entrevistado.get_sexo_display()
    get_sexo.short_description = 'Sexo'
    get_sexo.admin_order_field = 'entrevistado__sexo'

    def get_organizacion_apoya(self, obj):
        return obj.entrevistado.organizacion_apoyo
    get_organizacion_apoya.short_description = 'Organizacion que apoya'
    get_organizacion_apoya.admin_order_field = 'entrevistado__organizacion_apoyo'
# Register your models here.

# Register your models here.
admin.site.register(EncuestaRentabilidad, EncuestaAdmin)
admin.site.register(CafeClonesVariedad)
admin.site.register(GranosBasicos)
# Register your models here.
