# -*- coding: utf-8 -*-
"""sistemahelvetica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from cuadernosmes0.views import IndexView

admin.site.site_title = "Sitio Administrativo de CCI"
admin.site.site_header = "CCI Administración"

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^linea-base/', include('lineabase.urls')),
    url(r'^cuaderno-mes-seis/', include('cuadernosmes6.urls')),
    url(r'^cuaderno-mes-nueve/', include('cuadernosmes9.urls')),
    url(r'^cuaderno-mes-tres/', include('cuadernosmes3.urls')),
    url(r'^cuaderno-mes-cero/', include('cuadernosmes0.urls')),
    url(r'^transformacion/', include('transformacion.urls')),
    url(r'^rentabilidad/', include('rentabilidad.urls')),
    url(r'^nested_admin/', include('nested_admin.urls')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL , document_root = settings.STATIC_ROOT )
    urlpatterns += static(settings.MEDIA_URL , document_root = settings.MEDIA_ROOT )
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))

urlpatterns += staticfiles_urlpatterns()
