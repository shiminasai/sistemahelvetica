# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from lineabase.forms import EntrevistadoForm
from .models import *

class CacaoPunto1Inlines(admin.TabularInline):
    model = CacaoPunto1
    extra = 1

class MusaceasPunto1Inlines(admin.TabularInline):
    model = MusaceasPunto1
    extra = 1

class FrutalesPunto1Inlines(admin.TabularInline):
    model = FrutalesPunto1
    extra = 1

class MaderablesPunto1Inlines(admin.TabularInline):
    model = MaderablesPunto1
    extra = 1

class ArbolesPunto1Inlines(admin.TabularInline):
    model = ArbolesPunto1
    extra = 1

class CacaoPunto2Inlines(admin.TabularInline):
    model = CacaoPunto2
    extra = 1

class MusaceasPunto2Inlines(admin.TabularInline):
    model = MusaceasPunto2
    extra = 1

class FrutalesPunto2Inlines(admin.TabularInline):
    model = FrutalesPunto2
    extra = 1

class MaderablesPunto2Inlines(admin.TabularInline):
    model = MaderablesPunto2
    extra = 1

class ArbolesPunto2Inlines(admin.TabularInline):
    model = ArbolesPunto2
    extra = 1

class CacaoPunto3Inlines(admin.TabularInline):
    model = CacaoPunto3
    extra = 1

class MusaceasPunto3Inlines(admin.TabularInline):
    model = MusaceasPunto3
    extra = 1

class FrutalesPunto3Inlines(admin.TabularInline):
    model = FrutalesPunto3
    extra = 1

class MaderablesPunto3Inlines(admin.TabularInline):
    model = MaderablesPunto3
    extra = 1

class ArbolesPunto3Inlines(admin.TabularInline):
    model = ArbolesPunto3
    extra = 1

class PlagasEnfermedadesInlines(admin.TabularInline):
    model = PlagasEnfermedades
    extra = 1
    max_num = 1

class DatosPiso1Inlines(admin.TabularInline):
    model = DatosPiso1
    extra = 1

class DatosPiso2Inlines(admin.TabularInline):
    model = DatosPiso2
    extra = 1

class DatosPiso3Inlines(admin.TabularInline):
    model = DatosPiso3
    extra = 1

class AccionesManejoPisoInlines(admin.TabularInline):
    model = AccionesManejoPiso
    extra = 1

class CostoManoObraDiaInlines(admin.TabularInline):
    model = CostoManoObraDia
    extra = 1
    max_num = 1

class CostoCacaoInlines(admin.TabularInline):
    model = CostoCacao
    extra = 1

class CostoMusaceasInlines(admin.TabularInline):
    model = CostoMusaceas
    extra = 1

class CostoFrutalesInlines(admin.TabularInline):
    model = CostoFrutales
    extra = 1

class CostoMaderablesInlines(admin.TabularInline):
    model = CostoMaderables
    extra = 1

class CostoSombraTemporalInlines(admin.TabularInline):
    model = CostoSombraTemporal
    extra = 1

class DatosCosechaInlines(admin.TabularInline):
    model = DatosCosecha
    extra = 1

class CuadernoMesSeisAdmin(admin.ModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(CuadernoMesSeisAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CuadernoMesSeisAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('entrevistado', 'get_org_apoyo', 'ciclo','year')
    search_fields = ['entrevistado__nombre']
    list_filter = ['year']
    inlines = [CacaoPunto1Inlines,MusaceasPunto1Inlines,
                     FrutalesPunto1Inlines,MaderablesPunto1Inlines,ArbolesPunto1Inlines,
                     CacaoPunto2Inlines,MusaceasPunto2Inlines,
                     FrutalesPunto2Inlines,MaderablesPunto2Inlines,ArbolesPunto2Inlines,
                     CacaoPunto3Inlines,MusaceasPunto3Inlines,
                     FrutalesPunto3Inlines,MaderablesPunto3Inlines,ArbolesPunto3Inlines,
                     PlagasEnfermedadesInlines,DatosPiso1Inlines,
                     DatosPiso2Inlines,DatosPiso3Inlines,AccionesManejoPisoInlines,
                     CostoManoObraDiaInlines,CostoCacaoInlines,CostoMusaceasInlines,
                     CostoFrutalesInlines,CostoMaderablesInlines,
                     CostoSombraTemporalInlines,DatosCosechaInlines]

    def get_org_apoyo(self, obj):
        return obj.entrevistado.organizacion_apoyo.nombre
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'entrevistado__organizacion_apoyo__nombre'

    class Media:
        # css = {
        #     'all': ('/static/admin/css/admin_mes_3.css',)

        # }
        js = ('/static/admin/js/admin_mes_6.js',)

# Register your models here.
admin.site.register(CuadernoMesSeis, CuadernoMesSeisAdmin)
#admin.site.register()
