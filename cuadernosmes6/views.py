# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Avg, Sum, F

from cuadernosmes6.models import *
from cuadernosmes0.choices_static import *
from cuadernosmes9.views import _queryset_filtrado_cuaderno9

from collections import OrderedDict, Counter
import numpy as np
from itertools import chain

# Create your views here.

def _queryset_filtrado_cuaderno6(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['entrevistado__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['entrevistado__organizacion_apoyo'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]
    # for key, value in request.session.items():
    #     print('{} => {}'.format(key, value))

    return CuadernoMesSeis.objects.filter(**params)


def orientacion_composicion_piso(request, template="cuadernoseis/orientacion_composicion_piso.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_composicion = OrderedDict()
        for obj in CHOICE_UNIDAD_DATOS_POTENCIALMENTE:
            suma = filtro.filter(datospiso1__potencialmente=obj[0],year = anio[0]).aggregate(total=Sum('datospiso1__conteo'))['total']
            tabla_composicion[obj[1]] = suma

        for obj in CHOICE_UNIDAD_DATOS_COBERTURA:
            suma = filtro.filter(datospiso2__cobertura_noble=obj[0],year = anio[0]).aggregate(total=Sum('datospiso2__conteo'))['total']
            tabla_composicion[obj[1]] = suma

        for obj in CHOICE_UNIDAD_DATOS_MULCH:
            suma = filtro.filter(datospiso3__mulch=obj[0],year = anio[0]).aggregate(total=Sum('datospiso3__conteo'))['total']
            tabla_composicion[obj[1]] = suma

        VAR_TOTAL = 0
        for k,v in tabla_composicion.items():
            try:
                VAR_TOTAL += v
            except:
                VAR_TOTAL = 0
        print(tabla_composicion)

        years[anio[1]] = numero_parcelas,tabla_composicion,VAR_TOTAL,tabla_composicion

    return render(request, template, locals())

def propuesta_piso(request, template="cuadernoseis/propuesta_piso.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_propuesta = OrderedDict()
        for obj in CHOICE_MANEJO_PISO:
            conteo_piso = filtro.filter(accionesmanejopiso__manejo_piso=obj[0],year = anio[0]).count()
            tabla_propuesta[obj[1]] = (conteo_piso,)

        grafo_manejo = OrderedDict()
        for obj in CHOICE_MANEJO_PISO:
            ene = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='A',year = anio[0]).count()
            feb = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='B',year = anio[0]).count()
            mar = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='C',year = anio[0]).count()
            abr = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='D',year = anio[0]).count()
            may = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='E',year = anio[0]).count()
            jun = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='F',year = anio[0]).count()
            jul = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='G',year = anio[0]).count()
            ago = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='H',year = anio[0]).count()
            sep = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='I',year = anio[0]).count()
            octu = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='J',year = anio[0]).count()
            nov = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='K',year = anio[0]).count()
            dic = filtro.filter(accionesmanejopiso__manejo_piso=obj[0], accionesmanejopiso__meses__contains='L',year = anio[0]).count()
            grafo_manejo[obj[1]] = [ene,feb,mar,abr,may,jun,jul,ago,sep,octu,nov,dic]

        years[anio[1]] = numero_parcelas,grafo_manejo,tabla_propuesta


    return render(request, template, locals())


def estado_cacao(request, template="cuadernoseis/estado_cacao.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)

    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_cacao_mes_6 = OrderedDict()
        for obj in ClonesVariedades.objects.all():
            conteo1 = filtro.filter(cacaopunto1__clones_variedad=obj,year = anio[0]).count()
            conteo2 = filtro.filter(cacaopunto2__clones_variedad=obj,year = anio[0]).count()
            conteo3 = filtro.filter(cacaopunto3__clones_variedad=obj,year = anio[0]).count()
            total_conteo = conteo1 + conteo2 + conteo3

            if total_conteo >= 1:
                punto1 = filtro.filter(cacaopunto1__clones_variedad=obj,year = anio[0]).values_list('cacaopunto1__altura', flat=True)
                punto2 = filtro.filter(cacaopunto2__clones_variedad=obj,year = anio[0]).values_list('cacaopunto2__altura', flat=True)
                punto3 = filtro.filter(cacaopunto3__clones_variedad=obj,year = anio[0]).values_list('cacaopunto3__altura', flat=True)
                lista_total = list(chain(punto1, punto2, punto3))

                conteo1_plaga = filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__plaga=1,year = anio[0]).count()
                conteo2_plaga = filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__plaga=1,year = anio[0]).count()
                conteo3_plaga = filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__plaga=1,year = anio[0]).count()
                total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

                conteo1_enfermedad = filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__enfermedad=1,year = anio[0]).count()
                conteo2_enfermedad = filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__enfermedad=1,year = anio[0]).count()
                conteo3_enfermedad = filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__enfermedad=1,year = anio[0]).count()
                total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

                conteo1_nutricional= filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__nutricional=1,year = anio[0]).count()
                conteo2_nutricional= filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__nutricional=1,year = anio[0]).count()
                conteo3_nutricional= filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__nutricional=1,year = anio[0]).count()
                total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

                conteo1_ramificacion= filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__ramificacion=1,year = anio[0]).count()
                conteo2_ramificacion= filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__ramificacion=1,year = anio[0]).count()
                conteo3_ramificacion= filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__ramificacion=1,year = anio[0]).count()
                total_conteo_ramificacion = conteo1_ramificacion + conteo2_ramificacion + conteo3_ramificacion

                conteo1_floracion= filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__floracion=1,year = anio[0]).count()
                conteo2_floracion= filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__floracion=1,year = anio[0]).count()
                conteo3_floracion= filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__floracion=1,year = anio[0]).count()
                total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

                conteo1_fructificacion= filtro.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__fructificacion=1,year = anio[0]).count()
                conteo2_fructificacion= filtro.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__fructificacion=1,year = anio[0]).count()
                conteo3_fructificacion= filtro.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__fructificacion=1,year = anio[0]).count()
                total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

                
                tabla_cacao_mes_6[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
                                                                                total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
                                                                            total_conteo_ramificacion, total_conteo_floracion, total_conteo_fructificacion)

        years[anio[1]] = tabla_cacao_mes_6

    #salidas del mes 9 sobre cacao
    # tabla_cacao_mes_9 = OrderedDict()
    # for obj in ClonesVariedades.objects.all():
    #     conteo1 = filtro2.filter(cacaopunto1__clones_variedad=obj).count()
    #     conteo2 = filtro2.filter(cacaopunto2__clones_variedad=obj).count()
    #     conteo3 = filtro2.filter(cacaopunto3__clones_variedad=obj).count()
    #     total_conteo = conteo1 + conteo2 + conteo3

    #     punto1 = filtro2.filter(cacaopunto1__clones_variedad=obj).values_list('cacaopunto1__altura', flat=True)
    #     punto2 = filtro2.filter(cacaopunto2__clones_variedad=obj).values_list('cacaopunto2__altura', flat=True)
    #     punto3 = filtro2.filter(cacaopunto3__clones_variedad=obj).values_list('cacaopunto3__altura', flat=True)
    #     lista_total = list(chain(punto1, punto2, punto3))

    #     conteo1_plaga = filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__plaga=1).count()
    #     conteo2_plaga = filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__plaga=1).count()
    #     conteo3_plaga = filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__plaga=1).count()
    #     total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

    #     conteo1_enfermedad = filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__enfermedad=1).count()
    #     conteo2_enfermedad = filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__enfermedad=1).count()
    #     conteo3_enfermedad = filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__enfermedad=1).count()
    #     total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

    #     conteo1_nutricional= filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__nutricional=1).count()
    #     conteo2_nutricional= filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__nutricional=1).count()
    #     conteo3_nutricional= filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__nutricional=1).count()
    #     total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

    #     conteo1_ramificacion= filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__ramificacion=1).count()
    #     conteo2_ramificacion= filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__ramificacion=1).count()
    #     conteo3_ramificacion= filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__ramificacion=1).count()
    #     total_conteo_ramificacion = conteo1_ramificacion + conteo2_ramificacion + conteo3_ramificacion

    #     conteo1_floracion= filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__floracion=1).count()
    #     conteo2_floracion= filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__floracion=1).count()
    #     conteo3_floracion= filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__floracion=1).count()
    #     total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

    #     conteo1_fructificacion= filtro2.filter(cacaopunto1__clones_variedad=obj, cacaopunto1__fructificacion=1).count()
    #     conteo2_fructificacion= filtro2.filter(cacaopunto2__clones_variedad=obj, cacaopunto2__fructificacion=1).count()
    #     conteo3_fructificacion= filtro2.filter(cacaopunto3__clones_variedad=obj, cacaopunto3__fructificacion=1).count()
    #     total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

    #     tabla_cacao_mes_9[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
    #                                                                     total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
    #                                                                     total_conteo_ramificacion, total_conteo_floracion, total_conteo_fructificacion)

    return render(request, template, locals())

def estado_musaceas(request, template="cuadernoseis/estado_musaceas.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)
    # numero_parcelas = filtro.count()
    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_musaceas_mes_6 = OrderedDict()
        for obj in TiposMusaceas.objects.all():
            conteo1 = filtro.filter(musaceaspunto1__musaceas=obj,year = anio[0]).count()
            conteo2 = filtro.filter(musaceaspunto2__musaceas=obj,year = anio[0]).count()
            conteo3 = filtro.filter(musaceaspunto3__musaceas=obj,year = anio[0]).count()
            total_conteo = conteo1 + conteo2 + conteo3

            if total_conteo >= 1:

                punto1 = filtro.filter(musaceaspunto1__musaceas=obj,year = anio[0]).values_list('musaceaspunto1__altura', flat=True)
                punto2 = filtro.filter(musaceaspunto2__musaceas=obj,year = anio[0]).values_list('musaceaspunto2__altura', flat=True)
                punto3 = filtro.filter(musaceaspunto3__musaceas=obj,year = anio[0]).values_list('musaceaspunto3__altura', flat=True)
                lista_total = list(chain(punto1, punto2, punto3))

                conteo1_plaga = filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__plaga=1,year = anio[0]).count()
                conteo2_plaga = filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__plaga=1,year = anio[0]).count()
                conteo3_plaga = filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__plaga=1,year = anio[0]).count()
                total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

                conteo1_enfermedad = filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__enfermedad=1,year = anio[0]).count()
                conteo2_enfermedad = filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__enfermedad=1,year = anio[0]).count()
                conteo3_enfermedad = filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__enfermedad=1,year = anio[0]).count()
                total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

                conteo1_nutricional= filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__nutricional=1,year = anio[0]).count()
                conteo2_nutricional= filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__nutricional=1,year = anio[0]).count()
                conteo3_nutricional= filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__nutricional=1,year = anio[0]).count()
                total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

                conteo1_foliar= filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__foliar=1,year = anio[0]).count()
                conteo2_foliar= filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__foliar=1,year = anio[0]).count()
                conteo3_foliar= filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__foliar=1,year = anio[0]).count()
                total_conteo_foliar = conteo1_foliar + conteo2_foliar + conteo3_foliar

                conteo1_floracion= filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__floracion=1,year = anio[0]).count()
                conteo2_floracion= filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__floracion=1,year = anio[0]).count()
                conteo3_floracion= filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__floracion=1,year = anio[0]).count()
                total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

                conteo1_fructificacion= filtro.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__fructificacion=1,year = anio[0]).count()
                conteo2_fructificacion= filtro.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__fructificacion=1,year = anio[0]).count()
                conteo3_fructificacion= filtro.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__fructificacion=1,year = anio[0]).count()
                total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

                tabla_musaceas_mes_6[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
                                                                                total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
                                                                                total_conteo_foliar, total_conteo_floracion, total_conteo_fructificacion)
        years[anio[1]] = tabla_musaceas_mes_6

    #salidas del mes 9 sobre cacao
    # tabla_musaceas_mes_9 = OrderedDict()
    # for obj in TiposMusaceas.objects.all():
    #     conteo1 = filtro2.filter(musaceaspunto1__musaceas=obj).count()
    #     conteo2 = filtro2.filter(musaceaspunto2__musaceas=obj).count()
    #     conteo3 = filtro2.filter(musaceaspunto3__musaceas=obj).count()
    #     total_conteo = conteo1 + conteo2 + conteo3

    #     punto1 = filtro2.filter(musaceaspunto1__musaceas=obj).values_list('musaceaspunto1__altura', flat=True)
    #     punto2 = filtro2.filter(musaceaspunto2__musaceas=obj).values_list('musaceaspunto2__altura', flat=True)
    #     punto3 = filtro2.filter(musaceaspunto3__musaceas=obj).values_list('musaceaspunto3__altura', flat=True)
    #     lista_total = list(chain(punto1, punto2, punto3))

    #     conteo1_plaga = filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__plaga=1).count()
    #     conteo2_plaga = filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__plaga=1).count()
    #     conteo3_plaga = filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__plaga=1).count()
    #     total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

    #     conteo1_enfermedad = filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__enfermedad=1).count()
    #     conteo2_enfermedad = filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__enfermedad=1).count()
    #     conteo3_enfermedad = filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__enfermedad=1).count()
    #     total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

    #     conteo1_nutricional= filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__nutricional=1).count()
    #     conteo2_nutricional= filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__nutricional=1).count()
    #     conteo3_nutricional= filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__nutricional=1).count()
    #     total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

    #     conteo1_foliar= filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__foliar=1).count()
    #     conteo2_foliar= filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__foliar=1).count()
    #     conteo3_foliar= filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__foliar=1).count()
    #     total_conteo_foliar = conteo1_foliar + conteo2_foliar + conteo3_foliar

    #     conteo1_floracion= filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__floracion=1).count()
    #     conteo2_floracion= filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__floracion=1).count()
    #     conteo3_floracion= filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__floracion=1).count()
    #     total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

    #     conteo1_fructificacion= filtro2.filter(musaceaspunto1__musaceas=obj, musaceaspunto1__fructificacion=1).count()
    #     conteo2_fructificacion= filtro2.filter(musaceaspunto2__musaceas=obj, musaceaspunto2__fructificacion=1).count()
    #     conteo3_fructificacion= filtro2.filter(musaceaspunto3__musaceas=obj, musaceaspunto3__fructificacion=1).count()
    #     total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

    #     tabla_musaceas_mes_9[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
    #                                                                     total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
    #                                                                     total_conteo_foliar, total_conteo_floracion, total_conteo_fructificacion)

    return render(request, template, locals())

def estado_frutales(request, template="cuadernoseis/estado_frutales.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)
    # numero_parcelas = filtro.count()
    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_frutales_mes_6 = OrderedDict()
        for obj in EspeciesFrutales.objects.all():
            conteo1 = filtro.filter(frutalespunto1__especies=obj,year = anio[0]).count()
            conteo2 = filtro.filter(frutalespunto2__especies=obj,year = anio[0]).count()
            conteo3 = filtro.filter(frutalespunto3__especies=obj,year = anio[0]).count()
            total_conteo = conteo1 + conteo2 + conteo3

            if total_conteo >= 1:

                punto1 = filtro.filter(frutalespunto1__especies=obj,year = anio[0]).values_list('frutalespunto1__altura', flat=True)
                punto2 = filtro.filter(frutalespunto2__especies=obj,year = anio[0]).values_list('frutalespunto2__altura', flat=True)
                punto3 = filtro.filter(frutalespunto3__especies=obj,year = anio[0]).values_list('frutalespunto3__altura', flat=True)
                lista_total = list(chain(punto1, punto2, punto3))

                conteo1_plaga = filtro.filter(frutalespunto1__especies=obj, frutalespunto1__plaga=1,year = anio[0]).count()
                conteo2_plaga = filtro.filter(frutalespunto2__especies=obj, frutalespunto2__plaga=1,year = anio[0]).count()
                conteo3_plaga = filtro.filter(frutalespunto3__especies=obj, frutalespunto3__plaga=1,year = anio[0]).count()
                total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

                conteo1_enfermedad = filtro.filter(frutalespunto1__especies=obj, frutalespunto1__enfermedad=1,year = anio[0]).count()
                conteo2_enfermedad = filtro.filter(frutalespunto2__especies=obj, frutalespunto2__enfermedad=1,year = anio[0]).count()
                conteo3_enfermedad = filtro.filter(frutalespunto3__especies=obj, frutalespunto3__enfermedad=1,year = anio[0]).count()
                total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

                conteo1_nutricional= filtro.filter(frutalespunto1__especies=obj, frutalespunto1__nutricional=1,year = anio[0]).count()
                conteo2_nutricional= filtro.filter(frutalespunto2__especies=obj, frutalespunto2__nutricional=1,year = anio[0]).count()
                conteo3_nutricional= filtro.filter(frutalespunto3__especies=obj, frutalespunto3__nutricional=1,year = anio[0]).count()
                total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

                conteo1_crecimiento= filtro.filter(frutalespunto1__especies=obj, frutalespunto1__crecimiento=1,year = anio[0]).count()
                conteo2_crecimiento= filtro.filter(frutalespunto2__especies=obj, frutalespunto2__crecimiento=1,year = anio[0]).count()
                conteo3_crecimiento= filtro.filter(frutalespunto3__especies=obj, frutalespunto3__crecimiento=1,year = anio[0]).count()
                total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento

                conteo1_floracion= filtro.filter(frutalespunto1__especies=obj, frutalespunto1__floracion=1,year = anio[0]).count()
                conteo2_floracion= filtro.filter(frutalespunto2__especies=obj, frutalespunto2__floracion=1,year = anio[0]).count()
                conteo3_floracion= filtro.filter(frutalespunto3__especies=obj, frutalespunto3__floracion=1,year = anio[0]).count()
                total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

                conteo1_fructificacion= filtro.filter(frutalespunto1__especies=obj, frutalespunto1__fructificacion=1,year = anio[0]).count()
                conteo2_fructificacion= filtro.filter(frutalespunto2__especies=obj, frutalespunto2__fructificacion=1,year = anio[0]).count()
                conteo3_fructificacion= filtro.filter(frutalespunto3__especies=obj, frutalespunto3__fructificacion=1,year = anio[0]).count()
                total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

                tabla_frutales_mes_6[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
                                                                                total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
                                                                            total_conteo_crecimiento, total_conteo_floracion, total_conteo_fructificacion)
        years[anio[1]] = tabla_frutales_mes_6

    #salidas del mes 9 sobre cacao
    # tabla_frutales_mes_9 = OrderedDict()
    # for obj in EspeciesFrutales.objects.all():
    #     conteo1 = filtro2.filter(frutalespunto1__especies=obj).count()
    #     conteo2 = filtro2.filter(frutalespunto2__especies=obj).count()
    #     conteo3 = filtro2.filter(frutalespunto3__especies=obj).count()
    #     total_conteo = conteo1 + conteo2 + conteo3

    #     punto1 = filtro2.filter(frutalespunto1__especies=obj).values_list('frutalespunto1__altura', flat=True)
    #     punto2 = filtro2.filter(frutalespunto2__especies=obj).values_list('frutalespunto2__altura', flat=True)
    #     punto3 = filtro2.filter(frutalespunto3__especies=obj).values_list('frutalespunto3__altura', flat=True)
    #     lista_total = list(chain(punto1, punto2, punto3))

    #     conteo1_plaga = filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__plaga=1).count()
    #     conteo2_plaga = filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__plaga=1).count()
    #     conteo3_plaga = filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__plaga=1).count()
    #     total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

    #     conteo1_enfermedad = filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__enfermedad=1).count()
    #     conteo2_enfermedad = filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__enfermedad=1).count()
    #     conteo3_enfermedad = filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__enfermedad=1).count()
    #     total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

    #     conteo1_nutricional= filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__nutricional=1).count()
    #     conteo2_nutricional= filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__nutricional=1).count()
    #     conteo3_nutricional= filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__nutricional=1).count()
    #     total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

    #     conteo1_crecimiento= filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__crecimiento=1).count()
    #     conteo2_crecimiento= filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__crecimiento=1).count()
    #     conteo3_crecimiento= filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__crecimiento=1).count()
    #     total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento

    #     conteo1_floracion= filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__floracion=1).count()
    #     conteo2_floracion= filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__floracion=1).count()
    #     conteo3_floracion= filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__floracion=1).count()
    #     total_conteo_floracion = conteo1_floracion + conteo2_floracion + conteo3_floracion

    #     conteo1_fructificacion= filtro2.filter(frutalespunto1__especies=obj, frutalespunto1__fructificacion=1).count()
    #     conteo2_fructificacion= filtro2.filter(frutalespunto2__especies=obj, frutalespunto2__fructificacion=1).count()
    #     conteo3_fructificacion= filtro2.filter(frutalespunto3__especies=obj, frutalespunto3__fructificacion=1).count()
    #     total_conteo_fructificacion = conteo1_fructificacion + conteo2_fructificacion + conteo3_fructificacion

    #     tabla_frutales_mes_9[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
    #                                                                     total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
    #                                                                     total_conteo_crecimiento, total_conteo_floracion, total_conteo_fructificacion)

    return render(request, template, locals())

def estado_maderables(request, template="cuadernoseis/estado_maderables.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)
    # numero_parcelas = filtro.count()
    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_maderables_mes_6 = OrderedDict()
        for obj in EspeciesMaderables.objects.all():
            conteo1 = filtro.filter(maderablespunto1__especies=obj,year = anio[0]).count()
            conteo2 = filtro.filter(maderablespunto2__especies=obj,year = anio[0]).count()
            conteo3 = filtro.filter(maderablespunto3__especies=obj,year = anio[0]).count()
            total_conteo = conteo1 + conteo2 + conteo3

            if total_conteo >= 1:
                punto1 = filtro.filter(maderablespunto1__especies=obj,year = anio[0]).values_list('maderablespunto1__altura', flat=True)
                punto2 = filtro.filter(maderablespunto2__especies=obj,year = anio[0]).values_list('maderablespunto2__altura', flat=True)
                punto3 = filtro.filter(maderablespunto3__especies=obj,year = anio[0]).values_list('maderablespunto3__altura', flat=True)
                lista_total = list(chain(punto1, punto2, punto3))

                punto1_grosor = filtro.filter(maderablespunto1__especies=obj,year = anio[0]).values_list('maderablespunto1__grosor', flat=True)
                punto2_grosor = filtro.filter(maderablespunto2__especies=obj,year = anio[0]).values_list('maderablespunto2__grosor', flat=True)
                punto3_grosor = filtro.filter(maderablespunto3__especies=obj,year = anio[0]).values_list('maderablespunto3__grosor', flat=True)
                lista_total_grosor = list(chain(punto1_grosor, punto2_grosor, punto3_grosor))

                conteo1_plaga = filtro.filter(maderablespunto1__especies=obj, maderablespunto1__plaga=1,year = anio[0]).count()
                conteo2_plaga = filtro.filter(maderablespunto2__especies=obj, maderablespunto2__plaga=1,year = anio[0]).count()
                conteo3_plaga = filtro.filter(maderablespunto3__especies=obj, maderablespunto3__plaga=1,year = anio[0]).count()
                total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

                conteo1_enfermedad = filtro.filter(maderablespunto1__especies=obj, maderablespunto1__enfermedad=1,year = anio[0]).count()
                conteo2_enfermedad = filtro.filter(maderablespunto2__especies=obj, maderablespunto2__enfermedad=1,year = anio[0]).count()
                conteo3_enfermedad = filtro.filter(maderablespunto3__especies=obj, maderablespunto3__enfermedad=1,year = anio[0]).count()
                total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

                conteo1_nutricional= filtro.filter(maderablespunto1__especies=obj, maderablespunto1__nutricional=1,year = anio[0]).count()
                conteo2_nutricional= filtro.filter(maderablespunto2__especies=obj, maderablespunto2__nutricional=1,year = anio[0]).count()
                conteo3_nutricional= filtro.filter(maderablespunto3__especies=obj, maderablespunto3__nutricional=1,year = anio[0]).count()
                total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

                conteo1_crecimiento= filtro.filter(maderablespunto1__especies=obj, maderablespunto1__crecimiento=1,year = anio[0]).count()
                conteo2_crecimiento= filtro.filter(maderablespunto2__especies=obj, maderablespunto2__crecimiento=1,year = anio[0]).count()
                conteo3_crecimiento= filtro.filter(maderablespunto3__especies=obj, maderablespunto3__crecimiento=1,year = anio[0]).count()
                total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento


                tabla_maderables_mes_6[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
                                                                                np.mean(lista_total_grosor),np.median(lista_total_grosor),np.std(lista_total_grosor),
                                                                                total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
                                                                                total_conteo_crecimiento,)
        years[anio[1]] = tabla_maderables_mes_6

    #salidas del mes 9 sobre cacao
    # tabla_maderables_mes_9 = OrderedDict()
    # for obj in EspeciesMaderables.objects.all():
    #     conteo1 = filtro2.filter(maderablespunto1__especies=obj).count()
    #     conteo2 = filtro2.filter(maderablespunto2__especies=obj).count()
    #     conteo3 = filtro2.filter(maderablespunto3__especies=obj).count()
    #     total_conteo = conteo1 + conteo2 + conteo3

    #     punto1 = filtro2.filter(maderablespunto1__especies=obj).values_list('maderablespunto1__altura', flat=True)
    #     punto2 = filtro2.filter(maderablespunto2__especies=obj).values_list('maderablespunto2__altura', flat=True)
    #     punto3 = filtro2.filter(maderablespunto3__especies=obj).values_list('maderablespunto3__altura', flat=True)
    #     lista_total = list(chain(punto1, punto2, punto3))

    #     punto1_grosor = filtro2.filter(maderablespunto1__especies=obj).values_list('maderablespunto1__grosor', flat=True)
    #     punto2_grosor = filtro2.filter(maderablespunto2__especies=obj).values_list('maderablespunto2__grosor', flat=True)
    #     punto3_grosor = filtro2.filter(maderablespunto3__especies=obj).values_list('maderablespunto3__grosor', flat=True)
    #     lista_total_grosor = list(chain(punto1_grosor, punto2_grosor, punto3_grosor))

    #     conteo1_plaga = filtro2.filter(maderablespunto1__especies=obj, maderablespunto1__plaga=1).count()
    #     conteo2_plaga = filtro2.filter(maderablespunto2__especies=obj, maderablespunto2__plaga=1).count()
    #     conteo3_plaga = filtro2.filter(maderablespunto3__especies=obj, maderablespunto3__plaga=1).count()
    #     total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

    #     conteo1_enfermedad = filtro2.filter(maderablespunto1__especies=obj, maderablespunto1__enfermedad=1).count()
    #     conteo2_enfermedad = filtro2.filter(maderablespunto2__especies=obj, maderablespunto2__enfermedad=1).count()
    #     conteo3_enfermedad = filtro2.filter(maderablespunto3__especies=obj, maderablespunto3__enfermedad=1).count()
    #     total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

    #     conteo1_nutricional= filtro2.filter(maderablespunto1__especies=obj, maderablespunto1__nutricional=1).count()
    #     conteo2_nutricional= filtro2.filter(maderablespunto2__especies=obj, maderablespunto2__nutricional=1).count()
    #     conteo3_nutricional= filtro2.filter(maderablespunto3__especies=obj, maderablespunto3__nutricional=1).count()
    #     total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

    #     conteo1_crecimiento= filtro2.filter(maderablespunto1__especies=obj, maderablespunto1__crecimiento=1).count()
    #     conteo2_crecimiento= filtro2.filter(maderablespunto2__especies=obj, maderablespunto2__crecimiento=1).count()
    #     conteo3_crecimiento= filtro2.filter(maderablespunto3__especies=obj, maderablespunto3__crecimiento=1).count()
    #     total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento


    #     tabla_maderables_mes_9[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
    #                                                                     np.mean(lista_total_grosor),np.median(lista_total_grosor),np.std(lista_total_grosor),
    #                                                                     total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
    #                                                                     total_conteo_crecimiento,)

    return render(request, template, locals())

def estado_arboles_servicios(request, template="cuadernoseis/estado_arboles.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)
    # numero_parcelas = filtro.count()
    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_arboles_mes_6 = OrderedDict()
        for obj in EspeciesArbolesServicios.objects.all():
            conteo1 = filtro.filter(arbolespunto1__especies=obj,year = anio[0]).count()
            conteo2 = filtro.filter(arbolespunto2__especies=obj,year = anio[0]).count()
            conteo3 = filtro.filter(arbolespunto3__especies=obj,year = anio[0]).count()
            total_conteo = conteo1 + conteo2 + conteo3

            if total_conteo >= 1:
                punto1 = filtro.filter(arbolespunto1__especies=obj,year = anio[0]).values_list('arbolespunto1__altura', flat=True)
                punto2 = filtro.filter(arbolespunto2__especies=obj,year = anio[0]).values_list('arbolespunto2__altura', flat=True)
                punto3 = filtro.filter(arbolespunto3__especies=obj,year = anio[0]).values_list('arbolespunto3__altura', flat=True)
                lista_total = list(chain(punto1, punto2, punto3))

                conteo1_plaga = filtro.filter(arbolespunto1__especies=obj, arbolespunto1__plaga=1,year = anio[0]).count()
                conteo2_plaga = filtro.filter(arbolespunto2__especies=obj, arbolespunto2__plaga=1,year = anio[0]).count()
                conteo3_plaga = filtro.filter(arbolespunto3__especies=obj, arbolespunto3__plaga=1,year = anio[0]).count()
                total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

                conteo1_enfermedad = filtro.filter(arbolespunto1__especies=obj, arbolespunto1__enfermedad=1,year = anio[0]).count()
                conteo2_enfermedad = filtro.filter(arbolespunto2__especies=obj, arbolespunto2__enfermedad=1,year = anio[0]).count()
                conteo3_enfermedad = filtro.filter(arbolespunto3__especies=obj, arbolespunto3__enfermedad=1,year = anio[0]).count()
                total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

                conteo1_nutricional= filtro.filter(arbolespunto1__especies=obj, arbolespunto1__nutricional=1,year = anio[0]).count()
                conteo2_nutricional= filtro.filter(arbolespunto2__especies=obj, arbolespunto2__nutricional=1,year = anio[0]).count()
                conteo3_nutricional= filtro.filter(arbolespunto3__especies=obj, arbolespunto3__nutricional=1,year = anio[0]).count()
                total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

                conteo1_crecimiento= filtro.filter(arbolespunto1__especies=obj, arbolespunto1__crecimiento=1,year = anio[0]).count()
                conteo2_crecimiento= filtro.filter(arbolespunto2__especies=obj, arbolespunto2__crecimiento=1,year = anio[0]).count()
                conteo3_crecimiento= filtro.filter(arbolespunto3__especies=obj, arbolespunto3__crecimiento=1,year = anio[0]).count()
                total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento

                tabla_arboles_mes_6[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
                                                                                total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
                                                                            total_conteo_crecimiento)

        years[anio[1]] = tabla_arboles_mes_6

    #salidas del mes 9 sobre cacao
    # tabla_arboles_mes_9 = OrderedDict()
    # for obj in EspeciesArbolesServicios.objects.all():
    #     conteo1 = filtro2.filter(arbolespunto1__especies=obj).count()
    #     conteo2 = filtro2.filter(arbolespunto2__especies=obj).count()
    #     conteo3 = filtro2.filter(arbolespunto3__especies=obj).count()
    #     total_conteo = conteo1 + conteo2 + conteo3

    #     punto1 = filtro2.filter(arbolespunto1__especies=obj).values_list('arbolespunto1__altura', flat=True)
    #     punto2 = filtro2.filter(arbolespunto2__especies=obj).values_list('arbolespunto2__altura', flat=True)
    #     punto3 = filtro2.filter(arbolespunto3__especies=obj).values_list('arbolespunto3__altura', flat=True)
    #     lista_total = list(chain(punto1, punto2, punto3))

    #     conteo1_plaga = filtro2.filter(arbolespunto1__especies=obj, arbolespunto1__plaga=1).count()
    #     conteo2_plaga = filtro2.filter(arbolespunto2__especies=obj, arbolespunto2__plaga=1).count()
    #     conteo3_plaga = filtro2.filter(arbolespunto3__especies=obj, arbolespunto3__plaga=1).count()
    #     total_conteo_plaga = conteo1_plaga + conteo2_plaga + conteo3_plaga

    #     conteo1_enfermedad = filtro2.filter(arbolespunto1__especies=obj, arbolespunto1__enfermedad=1).count()
    #     conteo2_enfermedad = filtro2.filter(arbolespunto2__especies=obj, arbolespunto2__enfermedad=1).count()
    #     conteo3_enfermedad = filtro2.filter(arbolespunto3__especies=obj, arbolespunto3__enfermedad=1).count()
    #     total_conteo_enfermedad = conteo1_enfermedad + conteo2_enfermedad + conteo3_enfermedad

    #     conteo1_nutricional= filtro2.filter(arbolespunto1__especies=obj, arbolespunto1__nutricional=1).count()
    #     conteo2_nutricional= filtro2.filter(arbolespunto2__especies=obj, arbolespunto2__nutricional=1).count()
    #     conteo3_nutricional= filtro2.filter(arbolespunto3__especies=obj, arbolespunto3__nutricional=1).count()
    #     total_conteo_nutricional = conteo1_nutricional + conteo2_nutricional + conteo3_nutricional

    #     conteo1_crecimiento= filtro2.filter(arbolespunto1__especies=obj, arbolespunto1__crecimiento=1).count()
    #     conteo2_crecimiento= filtro2.filter(arbolespunto2__especies=obj, arbolespunto2__crecimiento=1).count()
    #     conteo3_crecimiento= filtro2.filter(arbolespunto3__especies=obj, arbolespunto3__crecimiento=1).count()
    #     total_conteo_crecimiento = conteo1_crecimiento + conteo2_crecimiento + conteo3_crecimiento

    #     tabla_arboles_mes_9[obj.nombre] = (total_conteo,np.mean(lista_total),np.median(lista_total),np.std(lista_total),
    #                                                                     total_conteo_plaga, total_conteo_enfermedad, total_conteo_nutricional,
    #                                                                     total_conteo_crecimiento)

    return render(request, template, locals())


def estado_problemas(request, template="cuadernoseis/estado_problemas.html"):
    filtro = _queryset_filtrado_cuaderno6(request)
    filtro2 = _queryset_filtrado_cuaderno9(request)
    # numero_parcelas = filtro.count()
    numero_parcelas2 = filtro2.count()

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        grafo_cacao_mes6 = OrderedDict()
        for obj in CHOICE_PLAGA_ENFERMEDAD_CACA0:
            conteo = filtro.filter(plagasenfermedades__cacao__contains=obj[0],year = anio[0]).count()
            grafo_cacao_mes6[obj[1]] = conteo

        #print grafo_cacao_mes6

        grafo_musaceas_mes6 = OrderedDict()
        for obj in CHOICE_PLAGA_ENFERMEDAD_MUSACEAS:
            conteo = filtro.filter(plagasenfermedades__musaceas__contains=obj[0],year = anio[0]).count()
            grafo_musaceas_mes6[obj[1]] = conteo

        grafo_frutales_mes6 = OrderedDict()
        for obj in CHOICE_PLAGA_ENFERMEDAD_FRUTALES:
            conteo = filtro.filter(plagasenfermedades__frutales__contains=obj[0],year = anio[0]).count()
            grafo_frutales_mes6[obj[1]] = conteo

        grafo_maderables_mes6 = OrderedDict()
        for obj in CHOICE_PLAGA_ENFERMEDAD_MADERABLES:
            conteo = filtro.filter(plagasenfermedades__maderables__contains=obj[0],year = anio[0]).count()
            grafo_maderables_mes6[obj[1]] = conteo

        grafo_arboles_servicios_mes6 = OrderedDict()
        for obj in CHOICE_ARBOLES_MADERABLES:
            conteo = filtro.filter(plagasenfermedades__arboles_servicios__contains=obj[0],year = anio[0]).count()
            grafo_arboles_servicios_mes6[obj[1]] = conteo

        years[anio[1]] = grafo_cacao_mes6,grafo_musaceas_mes6,grafo_frutales_mes6,grafo_maderables_mes6,grafo_arboles_servicios_mes6

    #ahora para mes 9

    # grafo_cacao_mes9 = OrderedDict()
    # for obj in CHOICE_PLAGA_ENFERMEDAD_CACA0:
    #     conteo = filtro2.filter(plagasenfermedades__cacao__contains=obj[0]).count()
    #     grafo_cacao_mes9[obj[1]] = conteo

    # grafo_musaceas_mes9 = OrderedDict()
    # for obj in CHOICE_PLAGA_ENFERMEDAD_MUSACEAS:
    #     conteo = filtro2.filter(plagasenfermedades__musaceas__contains=obj[0]).count()
    #     grafo_musaceas_mes9[obj[1]] = conteo

    # grafo_frutales_mes9 = OrderedDict()
    # for obj in CHOICE_PLAGA_ENFERMEDAD_FRUTALES:
    #     conteo = filtro2.filter(plagasenfermedades__frutales__contains=obj[0]).count()
    #     grafo_frutales_mes9[obj[1]] = conteo

    # grafo_maderables_mes9 = OrderedDict()
    # for obj in CHOICE_PLAGA_ENFERMEDAD_MADERABLES:
    #     conteo = filtro2.filter(plagasenfermedades__maderables__contains=obj[0]).count()
    #     grafo_maderables_mes9[obj[1]] = conteo

    # grafo_arboles_servicios_mes9 = OrderedDict()
    # for obj in CHOICE_ARBOLES_MADERABLES:
    #     conteo = filtro2.filter(plagasenfermedades__arboles_servicios__contains=obj[0]).count()
    #     grafo_arboles_servicios_mes9[obj[1]] = conteo

    return render(request, template, locals())
