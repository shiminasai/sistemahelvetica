# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Cuadernosmes6Config(AppConfig):
    name = 'cuadernosmes6'
    verbose_name = "Cuadernos mes 6"
