from django.conf.urls import url
from .views import *

urlpatterns =  [
    url(r'^sistema/parcela-sistema/', tamanio_parcelas, name='parcela-sistema'),
    url(r'^sistema/distribucion-sistema/', distribucion_parcelas, name='distribucion-sistema'),
    url(r'^sistema/composicion-sistema/', composicion_parcelas, name='composicion-sistema'),
    url(r'^sistema/hoyos-sistema/', hoyos_sistema, name='hoyos-sistema'),
    url(r'^sistema/riego-sistema/', riego_sistema, name='riego-sistema'),
]
