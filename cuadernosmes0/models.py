# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from .choices_static import *
from lineabase.models import (Entrevistados,
                                                         Encuestadores,
                                                         Organizacion)

from multiselectfield import MultiSelectField
# Create your models here.

@python_2_unicode_compatible
class Ciclo(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Ciclos"

@python_2_unicode_compatible
class Years(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Años"


@python_2_unicode_compatible
class CuadernoMesCero(models.Model):
    year = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    fecha = models.DateField()
    ciclo = models.ForeignKey(Ciclo, on_delete=models.CASCADE)
    entrevistado = models.ForeignKey(Entrevistados,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre productor/a')
    encuestador = models.ForeignKey(Encuestadores,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre del técnico')
    # organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE,
    #                                                                 verbose_name='Nombre de la organización',
    #                                                                 null=True, blank=True)
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICES_UNIDADES)
    sistema_agro = MultiSelectField('Tipo de sistema agroforestal (SC)',
                                choices=CHOICE_TIPO_SISTEMA_AGRO, null=True)
    #Datos de siembra
    surco_cacao = models.FloatField('Número de surco/hilera cacao')
    surco_platano = models.FloatField('Número de surco/hilera de plátano')
    surco_frutales = models.FloatField('Número de surco/hilera de frutales')
    surco_sombra = models.FloatField('Número de surco/hilera de Árboles de servicio')
    surco_maderable = models.FloatField('Número de surco/hilera de maderables')
    orientacion = models.IntegerField(choices=CHOICE_ORIENTACION)
    #croquis
    croquis = models.ImageField(upload_to='croquis/', null=True, blank=True)

    years = models.IntegerField(editable=False, verbose_name='Año')
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.years = self.fecha.year
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1
        if self.unidad == 3:
            self.area_ha = self.area / 16
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000

        super(CuadernoMesCero, self).save(*args, **kwargs)

    def __str__(self):
        return self.entrevistado.nombre

    class Meta:
        verbose_name = 'Cuaderno Mes 0'
        verbose_name_plural = 'Cuadernos de los meses 0'

@python_2_unicode_compatible
class ClonesVariedades(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Cacao Clones o Variedad'
        ordering = ['nombre']

class Cacao(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Cacao'

class CacaoMatriz(models.Model):
    cacao_parent = models.ForeignKey(Cacao, on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Cacao'

# musaceas
@python_2_unicode_compatible
class TiposMusaceas(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Tipo de musaceas'
        ordering = ['nombre']

class Musaceas(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Musaceas'

class MusaceasMatriz(models.Model):
    musacea_parent = models.ForeignKey(Musaceas, on_delete=models.CASCADE)
    tipo_musacea = models.ForeignKey(TiposMusaceas, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Musaceas'

#Frutales
@python_2_unicode_compatible
class EspeciesFrutales(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies frutales'
        ordering = ['nombre']

class Frutales(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Frutales'

class FrutalesMatriz(models.Model):
    frutales_parent = models.ForeignKey(Frutales, on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Frutales'

#maderables
@python_2_unicode_compatible
class EspeciesMaderables(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies Maderables'
        ordering = ['nombre']

class Maderables(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Maderables'

class MaderablesMatriz(models.Model):
    maderable_parent = models.ForeignKey(Maderables, on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Maderables'

#sombra temporal
@python_2_unicode_compatible
class EspeciesArbolesServicios(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies árboles de servicios'
        ordering = ['nombre']

class SombraTemporal(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Árboles de servicios'

class SombraTemporalMatriz(models.Model):
    sombra_parent = models.ForeignKey(SombraTemporal, on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(EspeciesArbolesServicios, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Árboles de servicios'

#Hoyo o huaca para la siembra
class HuacaSiembra(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    tipo_planta = models.IntegerField(choices=CHOICE_TIPO_PLANTA)
    largo = models.FloatField('Largo en cm')
    ancho = models.FloatField('Ancho en cm')
    profundidad = models.FloatField('Profundidad en cm')
    uso_abono = models.IntegerField(choices=CHOICE_SI_NO)
    tipo_abono = MultiSelectField(choices=CHOICE_TIPO_ABONO,
                            verbose_name='Tipo de abono')

    class Meta:
        verbose_name_plural = 'Hoyo o Huaca para la siembra'

#Riego en la parcela
class RiegoParcela(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    planes_riego = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='Hay planes para utilizar riego')
    tipo_riego = models.IntegerField(choices=CHOICE_TIPO_RIEGO, null=True, blank=True, verbose_name='Qué tipo de riego')
    fuente_de_agua = models.IntegerField(choices=CHOICE_FUENTE_AGUA, null=True, blank=True)
    movimiento_de_agua = models.IntegerField(choices=CHOICE_MOVIMIENTO_AGUA, null=True, blank=True)
    plan_de_riego = models.FloatField(null=True, blank=True, verbose_name='Plan de riego (meses)')
    frecuencia_de_riego = models.FloatField(null=True, blank=True, verbose_name='Frecuencia de riego (veces por semana)')
    galones = models.FloatField('Galones por cada riego de la parcela', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Riego en la parcela'
