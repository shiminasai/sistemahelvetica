# -*- coding: utf-8 -*-

CHOICE_CUADERNOS_YEAR = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6'),
    (7, '7'),
    (8, '8'),
    (9, '9'),
    (10, '10'),
    )

CHOICE_CUADERNOS_MONTH = (
    (1, 'Mes 0'),
    (2, 'Mes 3'),
    (3, 'Mes 6'),
    (4, 'Mes 9'),
    )

CHOICE_TIPO_SISTEMA_AGRO = (
    ('A', 'Cacao + Plátano'),
    ('B', 'Cacao + Frutales'),
    ('C', 'Cacao + Servicio'),
    ('D', 'Cacao + Maderables'),
    ('E', 'Cacao + Plátano + Frutales'),
    ('F', 'Cacao + Plátano + Árboles de servicio'),
    ('G', 'Cacao + Plátano + Maderable'),
    ('H', 'Cacao + Frutales + Árboles de servicio'),
    ('I', 'Cacao + Frutales + Maderables'),
    ('J', 'Cacao + Árboles de servicio + Maderables'),
    ('K', 'Cacao + Plátano + Frutales + Árboles de servicio'),
    ('L', 'Cacao + Plátano + Frutales + Maderables'),
    ('M', 'Cacao + Árboles de servicio + Frutales + Maderables'),
    ('N', 'Cacao + Plátano + Frutales + Árboles de servicio + Maderables'),
    ('O', 'Cacao + Plátano + Maderables + Árboles de servicio'),
    )

CHOICES_UNIDADES = (
    (1, 'Mz'),
    (2, 'Ha'),
    (3, 'Tarea'),
    (4, 'Mt2'),
    (5, 'Cab'),
    )

CHOICE_ORIENTACION = (
    (1, 'N-S'),
    (2, 'NE-SW'),
    (3, 'NW-SE'),
    (4, 'E-W'),
    )

CHOICE_ARREGLO = (
    (1, 'Cuadrado '),
    (2, 'Tresbolillo'),
    )

CHOICE_TIPO_PLANTA = (
    (1, 'Cacao'),
    (2, 'Musaceas'),
    (3, 'Frutales'),
    (4, 'Maderables'),
    (5, 'Sombra temporal'),
    )

CHOICE_SI_NO = (
    (1, 'Si '),
    (2, 'No'),
    )

CHOICE_TIPO_ABONO = (
    (1, 'Compost'),
    (2, 'Bocachi'),
    (3, 'Químico'),
    (4, 'Cal'),
    (5, 'Ninguno'),
    )

CHOICE_TIPO_RIEGO = (
    (1, 'Por gravedad'),
    (2, 'Por Micro-aspersores'),
    (3, 'Por goteo'),
    )

CHOICE_FUENTE_AGUA = (
    (1, 'Reservorio'),
    (2, 'Rio o Quebrada'),
    (3, 'Pozo'),
    )

CHOICE_MOVIMIENTO_AGUA = (
    (1, 'Por gravedad'),
    (2, 'Por bombeo'),
    )

CHOICE_PLAN_RIEGO = (
    (1, '12 meses'),
    (2, '6 meses'),
    (3, '4 meses '),
    )

CHOICE_FRECUENCIA_RIEGO = (
    (1, '1 vez por semana'),
    (2, '2 veces por semana'),
    (3, '3 veces por semana'),
    )

#cuadenos 3 meses
CHOICE_ESTADO_PLANTA = (
    (1, 'Bueno'),
    (2, 'Regular'),
    (3, 'Malo'),
    )

CHOICE_MEDIDA_PLANTA = (
    (1, 'Plantas/mz'),
    (2, 'Plantas/ha'),
    (3, 'Plantas/tarea'),
    (4, 'Plantas/Cab'),
    )

CHOICE_UNIDAD_SUELO_DOSIS = (
    (1, 'qq/mz'),
    (2, 'qq/ha'),
    (3, 'qq/tarea'),
    (4, 'lb/planta'),
    (5, 'oz/planta'),
    (6, 'qq/cab'),
    )

CHOICE_UNIDAD_FOLIAR_DOSIS = (
    (1, 'Lt/mz'),
    (2, 'Lt/ha'),
    (3, 'Lt/tarea'),
    )

CHOICE_USO_PARCELA = (
    ('A', 'Bosque'),
    ('B', 'Potrero'),
    ('C', 'Granos básicos'),
    ('D', 'Tacotal'),
    ('E', 'Cacaotal viejo'),
    )

CHOICE_LIMITANTES_PRODUCTIVOS = (
    ('A', 'Acidez / pH del suelo'),
    ('B', 'Encharcamiento / Mal Drenaje'),
    ('C', 'Enfermedades de raíces'),
    ('D', 'Deficiencia de nutrientes'),
    ('E', 'Baja materia orgánica'),
    ('F', 'Baja actividad biológica y presencia de lombrices'),
    ('G', 'Erosión de suelo'),
    ('H', 'Compactación e baja infiltración de agua'),
    ('I', 'Ningún limitante'),
    )

CHOICE_COSTO_CACAO_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de cacao'),
    (3, 'Deschuponado'),
    (4, 'Poda de árboles'),
    (5, 'Aplicación de abono'),
    (6, 'Aplicación de insecticida'),
    (7, 'Aplicación de fungicida'),
    (8, 'Manejo de piso'),
    (9, 'Aplicación de riego'),
    (10, 'Obras de drenaje'),
    (11, 'Obras de conservación'),
    (12, 'Cosecha y Corte'),
    (13, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MUSACEAS_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Deshije y deshoja'),
    (3, 'Manejo de cabezas'),
    (4, 'Aplicación de abono'),
    (5, 'Aplicación de insecticida'),
    (6, 'Aplicación de fungicida'),
    (7, 'Cosecha y Corte'),
    (8, 'Transporte de cosecha'),
    )

CHOICE_COSTO_FRUTALES_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de frutales'),
    (3, 'Aplicación de abono'),
    (4, 'Aplicación de insecticida'),
    (5, 'Aplicación de fungicida'),
    (6, 'Cosecha y Corte'),
    (7, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MADERABLES_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de maderables'),
    (3, 'Aplicación de abono'),
    (4, 'Aplicación de insecticida'),
    (5, 'Aplicación de fungicida'),
    (6, 'Cosecha y Corte'),
    (7, 'Transporte de cosecha'),
    )

CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Cosecha y Corte'),
    (3, 'Transporte de cosecha'),
    )

CHOICE_TIPO_FERTILIZA = (
    (1, 'Orgánico'),
    (2, 'Químico'),
    (3, 'Químico-Orgánico'),
    (4, 'Ninguno'),
    )

CHOICE_MODO_FERTILIZA = (
    (1, 'Aplicación en Suelo'),
    (2, 'Aplicación foliar'),
    (3, 'Aplicación en Suelo y foliar'),
    (4, 'Ninguno'),
    )

CHOICE_INTENSIDAD_FERTILIZA = (
    (1, 'Intensivo'),
    (2, 'Semi-intensivo'),
    (3, 'Ninguno'),
    )

CHOICE_MONEDA_LOCAL = (
    (1, 'NIO'),
    (2, 'HNL'),
    (3, 'USD'),
    (4, 'DOP'),
    (5, 'GTQ'),
    )

CHOICE_PRODUCTOS = (
    (1, 'Cacao en baba'),
    (2, 'Cacao en grano seco'),
    (3, 'Cosecha de granos'),
    (4, 'Musaceas-Banano'),
    (5, 'Musaceas-Plátano'),
    (6, 'Frutales-Zapote'),
    (7, 'Frutales-Cítricos'),
    (8, 'Frutales-Aguacate'),
    (9, 'Frutales-Coco'),
    (10, 'Madera-Caoba'),
    (11, 'Madera-Cortez'),
    (12, 'Madera-Roble'),
    (13, 'Madera-Melina'),
    (14, 'Madera-Granadillo'),
    (15, 'Madera-Leña'),
    (16, 'Granos básicos-Maíz'),
    (17, 'Granos básicos-Frijol'),
    (18, 'Gandul'),
    (19, 'Guaba'),
    )

CHOICE_UNIDAD_DATOS = (
    (1, 'qq'),
    (2, 'Cabeza'),
    (3, 'Lb'),
    (4, 'Kg'),
    (5, 'Docena'),
    (6, 'Cien'),
    (7, 'Saco'),
    (8, 'M3'),
    (9, 'Pt'),
    (10, 'Unidad'),
    (11, 'Carga'),
    )

CHOICE_COMPRADOR_DATOS = (
    ('A', 'Intermediario'),
    ('B', 'Cooperativa'),
    ('C', 'Asociación'),
    ('D', 'Empresa'),
    )

#cuaderno 6 meses
CHOICE_PLAGA_ENFERMEDAD_CACA0 = (
    ('A', 'Monilia'),
    ('B', 'Mazorca negra'),
    ('C', 'Mal de machete'),
    ('D', 'Mal de talluelo'),
    ('E', 'Buba'),
    ('F', 'Ardillas'),
    ('G', 'Barrenadores '),
    ('H', 'Zompopos'),
    ('I', 'Chupadores Áfidos'),
    ('J', 'Gusanos'),
    ('K', 'Escarabajos'),
    ('L', 'Viento'),
    ('M', 'Falta de agua'),
    ('N', 'Ninguno'),
    )

CHOICE_PLAGA_ENFERMEDAD_MUSACEAS = (
    ('A', 'Sigatoka negra'),
    ('B', 'Fusarium'),
    ('C', 'Nematodos'),
    ('D', 'Zompopos'),
    ('E', 'Chupadores Áfidos'),
    ('F', 'Picudos '),
    ('G', 'Viento'),
    ('H', 'Falta de agua'),
     ('I', 'Ninguno'),
    )

CHOICE_PLAGA_ENFERMEDAD_FRUTALES = (
    ('A', 'Mal de machete'),
    ('B', 'Mal de talluelo '),
    ('C', 'Ardillas'),
    ('D', 'Barrenadores'),
    ('E', 'Zompopos'),
    ('F', 'Chupadores Áfidos '),
    ('G', 'Gusanos'),
    ('H', 'Escarabajos'),
    ('I', 'Viento'),
    ('J', 'Falta de agua'),
     ('K', 'Ninguno'),
    )

CHOICE_ARBOLES_MADERABLES = (
    ('A', 'Mal de machete'),
    ('B', 'Mal de talluelo '),
    ('C', 'Barrenadores'),
    ('D', 'Zompopos'),
    ('E', 'Chupadores Áfidos '),
    ('F', 'Gusanos'),
    ('G', 'Escarabajos'),
    ('H', 'Viento'),
    ('I', 'Falta de agua'),
     ('J', 'Ninguno'),
    )

CHOICE_MANEJO_PISO = (
    (1, 'Recuento de malezas'),
    (2, 'Chapoda tendida'),
    (3, 'Chapoda selectiva'),
    (4, 'Aplicar herbicidas total'),
    (5, 'Aplicar herbicidas en parches'),
    (6, 'Manejo de bejuco'),
    (7, 'Manejo de tanda'),
    (8, 'Regulación de sombra'),
    )

CHOICE_MESES_ANIO = (
    ('A', 'Enero'),
    ('B', 'Febrero'),
    ('C', 'Marzo'),
    ('D', 'Abril'),
    ('E', 'Mayo'),
    ('F', 'Junio'),
    ('G', 'Julio'),
    ('H', 'Agosto'),
    ('I', 'Septiembre'),
    ('J', 'Octubre'),
    ('K', 'Noviembre'),
    ('L', 'Diciembre'),
    )

CHOICE_PLAGA_ENFERMEDAD_MADERABLES = (
    ('A', 'Mal de machete'),
    ('B', 'Mal de talluelo '),
    ('C', 'Hypsipyla'),
    ('D', 'Barrenadores'),
    ('E', 'Zompopos'),
    ('F', 'Chupadores Áfidos '),
    ('G', 'Gusanos'),
    ('H', 'Escarabajos'),
     ('I', 'Ninguno'),
    )

CHOICE_UNIDAD_DATOS_POTENCIALMENTE = (
    (1, 'Zacate anual'),
    (2, 'Zacate perene'),
    (3, 'Hoja ancha anual'),
    (4, 'Hoja ancha perenne'),
    (5, 'Ciperácea o Coyolillo'),
    (6, 'Bejucos en suelo '),
    )

CHOICE_UNIDAD_DATOS_COBERTURA = (
    (1, 'Cobertura hoja ancha'),
    (2, 'Cobertura hoja angosta'),
    )

CHOICE_UNIDAD_DATOS_MULCH = (
    (1, 'Hojarasca'),
    (2, 'Mulch de maleza'),
    (3, 'Suelo desnudo'),
    )

#cuaderno 9 meses
CHOICE_PUNTO_TIPO_ARBOL = (
    ('A', 'Perennifolia'),
    ('B', 'Caducifolia'),
    )

CHOICE_PUNTO_TIPO_ARBOL2 = (
    (1, 'Perennifolia'),
    (2, 'Caducifolia'),
    )

CHOICE_PUNTO_USO_ARBOL2 = (
    (1, 'Leña'),
    (2, 'Fruta'),
    (3, 'Madera'),
    (4, 'Sombra'),
    (5, 'Nutrientes'),
    )

CHOICE_PUNTO_USO_ARBOL = (
    ('A', 'Leña'),
    ('B', 'Fruta'),
    ('C', 'Madera'),
    ('D', 'Sombra'),
    ('E', 'Nutrientes '),
    )

CHOICE_PUNTO_TIPO_COPA = (
    ('A', 'Ancha'),
    ('B', 'Media'),
     ('C', 'Angosta'),
    )
