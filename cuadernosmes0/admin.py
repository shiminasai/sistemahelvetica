# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
#from lineabase.models import Encuestadores, Entrevistados
from lineabase.forms import EntrevistadoForm
from .models import *
import nested_admin

# class EncuestadoresAdmin(admin.ModelAdmin):
#     search_fields = ['nombre']

# class EntrevistadosAdmin(admin.ModelAdmin):
#     form = EntrevistadosForm
#     search_fields = ['nombre']

class NestedInlineCacaoMatriz(nested_admin.NestedTabularInline):
    model = CacaoMatriz
    fk_name = 'cacao_parent'
    extra = 1

class InlineCacao(nested_admin.NestedTabularInline):
    model = Cacao
    inlines = [NestedInlineCacaoMatriz]
    extra = 1
    max_num = 1

class NestedInlineMusaceasMatriz(nested_admin.NestedTabularInline):
    model = MusaceasMatriz
    fk_name = 'musacea_parent'
    extra = 1

class InlineMusaceas(nested_admin.NestedTabularInline):
    model = Musaceas
    inlines = [NestedInlineMusaceasMatriz]
    extra = 1
    max_num = 1

class NestedInlineFrutalesMatriz(nested_admin.NestedTabularInline):
    model = FrutalesMatriz
    fk_name = 'frutales_parent'
    extra = 1

class InlineFrutales(nested_admin.NestedTabularInline):
    model = Frutales
    inlines = [NestedInlineFrutalesMatriz]
    extra = 1
    max_num = 1

class NestedInlineMaderablesMatriz(nested_admin.NestedTabularInline):
    model = MaderablesMatriz
    fk_name = 'maderable_parent'
    extra = 1

class InlineMaderables(nested_admin.NestedTabularInline):
    model = Maderables
    inlines = [NestedInlineMaderablesMatriz]
    extra = 1
    max_num = 1

class NestedInlineSombraTemporalMatriz(nested_admin.NestedTabularInline):
    model = SombraTemporalMatriz
    fk_name = 'sombra_parent'
    extra = 1

class InlineSombraTemporal(nested_admin.NestedTabularInline):
    model = SombraTemporal
    inlines = [NestedInlineSombraTemporalMatriz]
    extra = 1
    max_num = 1

class InlineHuacaSiembra(nested_admin.NestedTabularInline):
    model = HuacaSiembra
    extra = 1
    max_num = 5

class InlineRiegoParcela(nested_admin.NestedTabularInline):
    model = RiegoParcela
    extra = 1
    max_num = 1

class CuadenoMesCeroAdmin(nested_admin.NestedModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(CuadenoMesCeroAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CuadenoMesCeroAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('entrevistado', 'get_org_apoyo','ciclo','year')
    search_fields = ['entrevistado__nombre']
    list_filter = ['year']
    inlines = [InlineCacao,InlineMusaceas,InlineFrutales,
                    InlineMaderables,InlineSombraTemporal,
                    InlineHuacaSiembra,InlineRiegoParcela]

    def get_org_apoyo(self, obj):
        return obj.entrevistado.organizacion_apoyo.nombre
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'entrevistado__organizacion_apoyo__nombre'



# Register your models here.
#admin.site.register(Entrevistados, EntrevistadosAdmin)
#admin.site.register(Encuestadores, EncuestadoresAdmin)
admin.site.register(CuadernoMesCero, CuadenoMesCeroAdmin)
admin.site.register(ClonesVariedades)
admin.site.register(TiposMusaceas)
admin.site.register(EspeciesFrutales)
admin.site.register(EspeciesMaderables)
admin.site.register(Years)
admin.site.register(Ciclo)
admin.site.register(EspeciesArbolesServicios)
