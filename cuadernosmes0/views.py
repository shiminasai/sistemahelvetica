# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from django.db.models import Avg, Sum, Count

from cuadernosmes0.models import *
from cuadernosmes0.choices_static import *

from collections import OrderedDict, Counter
import numpy as np
from itertools import chain
# Create your views here.

class IndexView(TemplateView):
    template_name = "index.html"


def _queryset_filtrado_cuaderno0(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['entrevistado__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['entrevistado__organizacion_apoyo'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    sabrosote = CuadernoMesCero.objects.filter(**params)
    #print "pinta cuadernos 0"
    #print sabrosote
    #print params

    return CuadernoMesCero.objects.filter(**params)


def tamanio_parcelas(request, template='cuadernocero/parcelas.html'):
    filtro = _queryset_filtrado_cuaderno0(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        areas = []
        for obj in filtro.filter(year = anio[0]):
            areas.append(obj.area_ha)
             #cnt1 = obj.filter(id=obj).values_list('area', flat=True)

        # media arítmetica
        promedio = np.mean(areas)
        # mediana
        mediana = np.median(areas)
        # Desviación típica
        desviacion = np.std(areas)
        #minimo
        try:
            minimo = min(areas)
        except:
            minimo = 0
        #maximo
        try:
            maximo = max(areas)
        except:
            maximo = 0

        grafo_parcela = crear_rangos(request, areas, minimo, maximo, step=2)

        #salida de tipo de sistema agroforestal
        tabla_sistema_agro = OrderedDict()
        for obj in CHOICE_TIPO_SISTEMA_AGRO:
            total = filtro.filter(sistema_agro__icontains=obj[0],year = anio[0]).count()
            try:
                porcentaje = float(total) / float(numero_parcelas) * 100
            except:
                porcentaje = 0
            if total > 0:
                tabla_sistema_agro[obj[1]] = (total,porcentaje)

        years[anio[1]] = [numero_parcelas,promedio,mediana,maximo,minimo,
                        grafo_parcela,tabla_sistema_agro]

    return render(request, template, locals())


def distribucion_parcelas(request, template='cuadernocero/distribucion.html'):
    filtro = _queryset_filtrado_cuaderno0(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        surcos = filtro.filter(year = anio[0]).aggregate(cacao=Sum('surco_cacao'), platano=Sum('surco_platano'),
                                                    sombra=Sum('surco_sombra'), maderable=Sum('surco_maderable'))
        try:
            total_surcos = sum(surcos.values())
        except:
            total_surcos = 0

        grafo_orientacion = OrderedDict()
        for obj in CHOICE_ORIENTACION:
            conteo = filtro.filter(orientacion=obj[0],year = anio[0]).count()
            grafo_orientacion[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,surcos,total_surcos,grafo_orientacion

    return render(request, template, locals())

def composicion_parcelas(request, template='cuadernocero/composicion.html'):
    filtro = _queryset_filtrado_cuaderno0(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()
        grafo_parcela = []

        tabla_composicion_cacao = OrderedDict()
        for obj in ClonesVariedades.objects.all():
            contar = filtro.filter(cacao__cacaomatriz__clones_variedad=obj,year = anio[0]).count()
            try:
                porcentaje = float(contar) / float(numero_parcelas) * 100
            except:
                porcentaje = 0

            if contar > 0.1 and porcentaje > 0.1:
                tabla_composicion_cacao[obj.nombre] = (contar, porcentaje)

        tabla_composicion_musaseas = OrderedDict()
        for obj in TiposMusaceas.objects.all():
            contar = filtro.filter(musaceas__musaceasmatriz__tipo_musacea=obj,year = anio[0]).count()
            try:
                porcentaje = float(contar) / float(numero_parcelas) * 100
            except:
                porcentaje = 0

            if contar > 0.1 and porcentaje > 0.1:
                tabla_composicion_musaseas[obj.nombre] = (contar, porcentaje)

        tabla_composicion_frutales = OrderedDict()
        for obj in EspeciesFrutales.objects.all():
            contar = filtro.filter(frutales__frutalesmatriz__especies=obj,year = anio[0]).count()
            try:
                porcentaje = float(contar) / float(numero_parcelas) * 100
            except:
                porcentaje = 0

            if contar > 0.1 and porcentaje > 0.1:
                tabla_composicion_frutales[obj.nombre] = (contar, porcentaje)

        tabla_composicion_maderables = OrderedDict()
        for obj in EspeciesMaderables.objects.all():
            contar = filtro.filter(maderables__maderablesmatriz__especies=obj,year = anio[0]).count()
            try:
                porcentaje = float(contar) / float(numero_parcelas) * 100
            except:
                porcentaje = 0
            if contar > 0.1 and porcentaje > 0.1:
                tabla_composicion_maderables[obj.nombre] = (contar, porcentaje)

        tabla_composicion_sombra = OrderedDict()
        for obj in EspeciesArbolesServicios.objects.all():
            contar = filtro.filter(sombratemporal__sombratemporalmatriz__clones_variedad=obj,year = anio[0]).count()
            try:
                porcentaje = float(contar) / float(numero_parcelas) * 100
            except:
                porcentaje = 0
            if contar > 0.1 and porcentaje > 0.1:
                tabla_composicion_sombra[obj.nombre] = (contar, porcentaje)

        #DENSIDAD DE LAS PLANTAS
        area_total_parcela = filtro.filter(year = anio[0]).aggregate(total=Sum('area_ha'))['total']
        cacao = filtro.filter(year = anio[0]).aggregate(total=Sum('cacao__cacaomatriz__cantidad_sembrada'),
                                                    promedio=Avg('cacao__cacaomatriz__altura'))
        musaceas = filtro.filter(year = anio[0]).aggregate(total=Sum('musaceas__musaceasmatriz__cantidad_sembrada'),
                                                    promedio=Avg('musaceas__musaceasmatriz__altura'))
        frutales = filtro.filter(year = anio[0]).aggregate(total=Sum('frutales__frutalesmatriz__cantidad_sembrada'),
                                                    promedio=Avg('frutales__frutalesmatriz__altura'))
        madera = filtro.filter(year = anio[0]).aggregate(total=Sum('maderables__maderablesmatriz__cantidad_sembrada'),
                                                    promedio=Avg('maderables__maderablesmatriz__altura'))
        arboles = filtro.filter(year = anio[0]).aggregate(total=Sum('sombratemporal__sombratemporalmatriz__cantidad_sembrada'),
                                                    promedio=Avg('sombratemporal__sombratemporalmatriz__altura'))

        #DENSIDAD DE LAS PLANTAS SURCOS Y DISTANCIA
        cacao_surco = filtro.filter(year = anio[0]).aggregate(surco=Avg('cacao__cacaomatriz__distancia_surco'),
                                                    distancia=Avg('cacao__cacaomatriz__distancia_planta'))
        cacao_cuadrado = filtro.filter(cacao__cacaomatriz__arreglo=1,year = anio[0]).count()
        cacao_bolillo = filtro.filter(cacao__cacaomatriz__arreglo=2,year = anio[0]).count()

        musaceas_surco = filtro.filter(year = anio[0]).aggregate(surco=Avg('musaceas__musaceasmatriz__distancia_surco'),
                                                    distancia=Avg('musaceas__musaceasmatriz__distancia_planta'))
        musaceas_cuadrado = filtro.filter(musaceas__musaceasmatriz__arreglo=1,year = anio[0]).count()
        musaceas_bolillo = filtro.filter(musaceas__musaceasmatriz__arreglo=2,year = anio[0]).count()

        frutales_surco = filtro.filter(year = anio[0]).aggregate(surco=Avg('frutales__frutalesmatriz__distancia_surco'),
                                                    distancia=Avg('frutales__frutalesmatriz__distancia_planta'))
        frutales_cuadrado = filtro.filter(frutales__frutalesmatriz__arreglo=1,year = anio[0]).count()
        frutales_bolillo = filtro.filter(frutales__frutalesmatriz__arreglo=2,year = anio[0]).count()

        madera_surco = filtro.filter(year = anio[0]).aggregate(surco=Avg('maderables__maderablesmatriz__distancia_surco'),
                                                    distancia=Avg('maderables__maderablesmatriz__distancia_planta'))
        madera_cuadrado = filtro.filter(maderables__maderablesmatriz__arreglo=1,year = anio[0]).count()
        madera_bolillo = filtro.filter(maderables__maderablesmatriz__arreglo=2,year = anio[0]).count()

        arboles_surco = filtro.filter(year = anio[0]).aggregate(surco=Sum('sombratemporal__sombratemporalmatriz__distancia_surco'),
                                                    distancia=Avg('sombratemporal__sombratemporalmatriz__distancia_planta'))
        arboles_cuadrado = filtro.filter(sombratemporal__sombratemporalmatriz__arreglo=1,year = anio[0]).count()
        arboles_bolillo = filtro.filter(sombratemporal__sombratemporalmatriz__arreglo=2,year = anio[0]).count()

        years[anio[1]] = [numero_parcelas,grafo_parcela,tabla_composicion_cacao,tabla_composicion_musaseas,
                            tabla_composicion_frutales,tabla_composicion_maderables,tabla_composicion_sombra,
                            area_total_parcela,cacao,musaceas,frutales,madera,arboles,cacao_surco,cacao_bolillo,
                            cacao_cuadrado,musaceas_surco,musaceas_bolillo,musaceas_cuadrado,frutales_surco,
                            frutales_bolillo,frutales_cuadrado,madera_surco,madera_bolillo,madera_cuadrado,
                            arboles_surco,arboles_bolillo,arboles_cuadrado]

    return render(request, template, locals())

def hoyos_sistema(request, template='cuadernocero/hoyos.html'):
    filtro = _queryset_filtrado_cuaderno0(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        cacao_largo = filtro.filter(huacasiembra__tipo_planta=1,year = anio[0]).values_list('huacasiembra__largo', flat=True)
        cacao_ancho = filtro.filter(huacasiembra__tipo_planta=1,year = anio[0]).values_list('huacasiembra__ancho', flat=True)
        cacao_profundo = filtro.filter(huacasiembra__tipo_planta=1,year = anio[0]).values_list('huacasiembra__profundidad', flat=True)

        cacao_avg_largo = np.mean(cacao_largo)
        cacao_median_largo = np.median(cacao_largo)

        cacao_avg_ancho = np.mean(cacao_ancho)
        cacao_median_ancho = np.median(cacao_ancho)

        cacao_avg_profundo = np.mean(cacao_profundo)
        cacao_median_profundo = np.median(cacao_profundo)

        musaceas_largo = filtro.filter(huacasiembra__tipo_planta=2,year = anio[0]).values_list('huacasiembra__largo', flat=True)
        musaceas_ancho = filtro.filter(huacasiembra__tipo_planta=2,year = anio[0]).values_list('huacasiembra__ancho', flat=True)
        musaceas_profundo = filtro.filter(huacasiembra__tipo_planta=2,year = anio[0]).values_list('huacasiembra__profundidad', flat=True)

        musaceas_avg_largo = np.mean(musaceas_largo)
        musaceas_median_largo = np.median(musaceas_largo)

        musaceas_avg_ancho = np.mean(musaceas_ancho)
        musaceas_median_ancho = np.median(musaceas_ancho)

        musaceas_avg_profundo = np.mean(musaceas_profundo)
        musaceas_median_profundo = np.median(musaceas_profundo)

        frutales_largo = filtro.filter(huacasiembra__tipo_planta=3,year = anio[0]).values_list('huacasiembra__largo', flat=True)
        frutales_ancho = filtro.filter(huacasiembra__tipo_planta=3,year = anio[0]).values_list('huacasiembra__ancho', flat=True)
        frutales_profundo = filtro.filter(huacasiembra__tipo_planta=3,year = anio[0]).values_list('huacasiembra__profundidad', flat=True)

        frutales_avg_largo = np.mean(frutales_largo)
        frutales_median_largo = np.median(frutales_largo)

        frutales_avg_ancho = np.mean(frutales_ancho)
        frutales_median_ancho = np.median(frutales_ancho)

        frutales_avg_profundo = np.mean(frutales_profundo)
        frutales_median_profundo = np.median(frutales_profundo)

        madera_largo = filtro.filter(huacasiembra__tipo_planta=4,year = anio[0]).values_list('huacasiembra__largo', flat=True)
        madera_ancho = filtro.filter(huacasiembra__tipo_planta=4,year = anio[0]).values_list('huacasiembra__ancho', flat=True)
        madera_profundo = filtro.filter(huacasiembra__tipo_planta=4,year = anio[0]).values_list('huacasiembra__profundidad', flat=True)

        madera_avg_largo = np.mean(madera_largo)
        madera_median_largo = np.median(madera_largo)

        madera_avg_ancho = np.mean(madera_ancho)
        madera_median_ancho = np.median(madera_ancho)

        madera_avg_profundo = np.mean(madera_profundo)
        madera_median_profundo = np.median(madera_profundo)

        arbol_largo = filtro.filter(huacasiembra__tipo_planta=5,year = anio[0]).values_list('huacasiembra__largo', flat=True)
        arbol_ancho = filtro.filter(huacasiembra__tipo_planta=5,year = anio[0]).values_list('huacasiembra__ancho', flat=True)
        arbol_profundo = filtro.filter(huacasiembra__tipo_planta=5,year = anio[0]).values_list('huacasiembra__profundidad', flat=True)

        arbol_avg_largo = np.mean(arbol_largo)
        arbol_median_largo = np.median(arbol_largo)

        arbol_avg_ancho = np.mean(arbol_ancho)
        arbol_median_ancho = np.median(arbol_ancho)

        arbol_avg_profundo = np.mean(arbol_profundo)
        arbol_median_profundo = np.median(arbol_profundo)

        tabla_uso = OrderedDict()
        for obj in CHOICE_TIPO_PLANTA:
            conteo = filtro.filter(huacasiembra__tipo_planta=obj[0], huacasiembra__uso_abono=1,year = anio[0]).count()
            compost = filtro.filter(huacasiembra__tipo_planta=obj[0], huacasiembra__tipo_abono__icontains='1',year = anio[0]).count()
            bocachi = filtro.filter(huacasiembra__tipo_planta=obj[0], huacasiembra__tipo_abono__icontains='2',year = anio[0]).count()
            quimico = filtro.filter(huacasiembra__tipo_planta=obj[0], huacasiembra__tipo_abono__icontains='3',year = anio[0]).count()
            cal = filtro.filter(huacasiembra__tipo_planta=obj[0], huacasiembra__tipo_abono__icontains='4',year = anio[0]).count()
            tabla_uso[obj[1]] = (conteo, compost, bocachi, quimico, cal)

        years[anio[1]] = [numero_parcelas,cacao_avg_largo,cacao_median_largo,cacao_avg_ancho,cacao_median_ancho,
                            cacao_avg_profundo,cacao_median_profundo,musaceas_avg_largo,musaceas_median_largo,
                            musaceas_avg_ancho,musaceas_median_ancho,musaceas_avg_profundo,musaceas_median_profundo,
                            frutales_avg_largo,frutales_median_largo,frutales_avg_ancho,frutales_median_ancho,
                            frutales_avg_profundo,frutales_median_profundo,madera_avg_largo,madera_median_largo,
                            madera_avg_ancho,madera_median_ancho,madera_avg_profundo,madera_median_profundo,
                            arbol_avg_largo,arbol_median_largo,arbol_avg_ancho,arbol_median_ancho,arbol_avg_profundo,
                            arbol_median_profundo,tabla_uso
                            ]


    return render(request, template, locals())

def riego_sistema(request, template='cuadernocero/riego.html'):
    filtro = _queryset_filtrado_cuaderno0(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        grafo_planes = OrderedDict()
        for obj in CHOICE_SI_NO:
            conteo = filtro.filter(riegoparcela__planes_riego=obj[0],year = anio[0]).count()
            grafo_planes[obj[1]] = conteo

        grafo_tipos = OrderedDict()
        for obj in CHOICE_TIPO_RIEGO:
            conteo = filtro.filter(riegoparcela__tipo_riego=obj[0],year = anio[0]).count()
            grafo_tipos[obj[1]] = conteo

        grafo_agua = OrderedDict()
        for obj in CHOICE_FUENTE_AGUA:
            conteo = filtro.filter(riegoparcela__fuente_de_agua=obj[0],year = anio[0]).count()
            grafo_agua[obj[1]] = conteo

        grafo_movimiento = OrderedDict()
        for obj in CHOICE_MOVIMIENTO_AGUA:
            conteo = filtro.filter(riegoparcela__movimiento_de_agua=obj[0],year = anio[0]).count()
            grafo_movimiento[obj[1]] = conteo

        plan_riego = filtro.filter(year = anio[0]).aggregate(promedio=Avg('riegoparcela__plan_de_riego'))['promedio']
        frecuencia = filtro.filter(year = anio[0]).aggregate(promedio=Avg('riegoparcela__frecuencia_de_riego'))['promedio']
        galones = filtro.filter(year = anio[0]).aggregate(promedio=Avg('riegoparcela__galones'))['promedio']

        try:
            uso_anual = plan_riego * 4 * frecuencia * galones
        except:
            uso_anual = 0
        uso_ha = uso_anual * 0.00378

        years[anio[1]] = [numero_parcelas,grafo_planes,grafo_tipos,grafo_agua,grafo_movimiento,plan_riego,
                            frecuencia,galones,uso_anual,uso_ha]

    return render(request, template, locals())

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo
