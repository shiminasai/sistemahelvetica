# -*- coding: utf-8 -*-
from django import forms
from .models import Entrevistados, Encuesta, Organizacion, OrganizacionApoyo
from lugar.models import (Pais, Departamento,
                                                 Municipio, Comunidad)
from cuadernosmes0.models import CuadernoMesCero, Ciclo
from cuadernosmes3.models import CuadernoMesTres
from cuadernosmes6.models import CuadernoMesSeis
from cuadernosmes9.models import CuadernoMesNueve

from dal import autocomplete

class EntrevistadoForm(forms.ModelForm):
    class Meta:
        model = Encuesta
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class EntrevistadosForm(forms.ModelForm):

    class Meta:
        model = Entrevistados
        fields = '__all__'
        widgets = {
                'departamento': autocomplete.ModelSelect2(url='depart-autocomplete',
                                                           forward=['pais']),
                 'municipio': autocomplete.ModelSelect2(url='municipio-autocomplete',
                                                           forward=['departamento']),
                 'comunidad': autocomplete.ModelSelect2(url='comunidad-autocomplete',
                                                           forward=['municipio']),
            }


def fecha_choice():
    years = []
    for en in Encuesta.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))
    for en in CuadernoMesCero.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))
    for en in CuadernoMesTres.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))
    for en in CuadernoMesSeis.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))
    for en in CuadernoMesNueve.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))

    return list(sorted(set(years)))


def ciclo_choice():
    ciclos = []
    for en in Ciclo.objects.all():
        ciclos.append((en.nombre,en.nombre))
    return list(sorted(set(ciclos)))

CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Mujer'),
    (2, 'Hombre')
)

class ConsultaLineaBaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultaLineaBaseForm, self).__init__(*args, **kwargs)
          self.fields['fecha'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
                                                                                'data-placeholder':'Escoge....', 'tabindex':'-1','area-hidden': 'true'})
          self.fields['organizacion'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['pais'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['productor'].widget.attrs.update({'class': 'form-control'})
          self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})

          if 'pais' in self.data:
            try:
                pais_id = int(self.data.get('pais'))
                self.fields['departamento'].queryset = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

          if 'departamento' in self.data:
            try:
                pais_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

          if 'municipio' in self.data:
            try:
                pais_id = int(self.data.get('municipio'))
                self.fields['comunidad'].queryset = Comunidad.objects.filter(municipio_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

    fecha = forms.MultipleChoiceField(choices=fecha_choice(), label="Años", required=False)
    productor = forms.CharField(max_length=250, required=False)
    organizacion = forms.ModelChoiceField(queryset=OrganizacionApoyo.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.none(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.none(), required=False)
    comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.none(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)


class ConsultaEstablecimientoForm(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultaEstablecimientoForm, self).__init__(*args, **kwargs)
          self.fields['ciclo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above',
                                                                                'data-placeholder':'Escoge....', 'tabindex':'-1','area-hidden': 'true'})
          self.fields['organizacion_pertenece'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['organizacion_apoyo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['pais'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['productor'].widget.attrs.update({'class': 'form-control'})
          self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          #self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})

          if 'pais' in self.data:
            try:
                pais_id = int(self.data.get('pais'))
                self.fields['departamento'].queryset = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

          if 'departamento' in self.data:
            try:
                pais_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

          # if 'municipio' in self.data:
          #   try:
          #       pais_id = int(self.data.get('municipio'))
          #       self.fields['comunidad'].queryset = Comunidad.objects.filter(municipio_id=pais_id).order_by('nombre')
          #   except (ValueError, TypeError):
          #       pass  # invalid input from the client; ignore and fallback to empty City queryset
    ciclo = forms.ChoiceField(choices=ciclo_choice(), required=False)
    productor = forms.CharField(max_length=250, required=False)
    organizacion_pertenece = forms.ModelChoiceField(queryset=Organizacion.objects.all(), required=False)
    organizacion_apoyo = forms.ModelChoiceField(queryset=OrganizacionApoyo.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.none(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.none(), required=False)
    #comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)

class FormularioColabora(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FormularioColabora, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['correo'].widget.attrs.update({'class': 'form-control'})
        self.fields['asunto'].widget.attrs.update({'class': 'form-control'})
        self.fields['mensaje'].widget.attrs.update({'class': 'form-control'})
    nombre = forms.CharField(max_length=250,required=True)
    correo = forms.EmailField(required=True)
    asunto = forms.CharField(max_length=250,required=True)
    mensaje = forms.CharField(required=True,widget=forms.Textarea)
