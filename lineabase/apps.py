# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class LineabaseConfig(AppConfig):
    name = 'lineabase'
    verbose_name = "Datos Generales"
