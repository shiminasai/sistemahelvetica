# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User

from lugar.models import Pais, Departamento, Municipio, Comunidad
from .choices_static import *
from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money

# Create your models here.

@python_2_unicode_compatible
class OrganizacionApoyo(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Organización de apoyo'
        verbose_name_plural = 'Organizaciones de apoyos'
        unique_together = ('nombre',)

@python_2_unicode_compatible
class Proyectos(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Proyectos'


@python_2_unicode_compatible
class Entrevistados(models.Model):
    nombre = models.CharField('Nombre dueño/a de la finca', max_length=250)
    nombre_finca = models.CharField('Nombre de la finca', max_length=250)
    sexo = models.IntegerField(choices=CHOICE_SEXO)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE,
                                    verbose_name='Departamento/Provincia')
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    comunidad = models.ForeignKey(Comunidad, on_delete=models.CASCADE, null=True, blank=True)
    latitud = models.FloatField(null=True, blank=True)
    longitud = models.FloatField(null= True, blank=True)
    altitud = models.FloatField('Altitud de la finca', null=True,blank=True)
    maneja = models.IntegerField(choices=CHOICE_MANEJA_FINCA,
                                                          verbose_name='Quién maneja la finca')
    proyectos = models.ManyToManyField(Proyectos, verbose_name='Proyetos donde es beneficiario')
    organizacion_pertenece = models.ForeignKey('Organizacion', on_delete=models.CASCADE,
                                                                                verbose_name='Organización que pertence productor',
                                                                                null=True, blank=True)
    desde_cuando = models.IntegerField(choices=CHOICE_DESDE_CUANDO, null=True, blank=True)
    organizacion_apoyo = models.ForeignKey(OrganizacionApoyo, on_delete=models.CASCADE,
                                                                                verbose_name='Organización de apoyo',
                                                                                null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Productores/as'
        verbose_name_plural = 'Productores/as'
        ordering = ('nombre',)

@python_2_unicode_compatible
class Encuestadores(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Técnico'
        verbose_name_plural = 'Técnicos'
        unique_together = ('nombre',)

@python_2_unicode_compatible
class Organizacion(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Organización del productor'
        verbose_name_plural = 'Organizaciones donde pertenece el productor'
        unique_together = ('nombre',)

@python_2_unicode_compatible
class Encuesta(models.Model):
    fecha = models.DateField()
    encuestador = models.ForeignKey(Encuestadores, on_delete=models.CASCADE,
                                                                    verbose_name='Técnico')
    entrevistado = models.ForeignKey(Entrevistados, on_delete=models.CASCADE,
                                                                    verbose_name='Productor')
    socio = MultiSelectField('Es socio/a de una organizacion gremial',
                                                choices=CHOICE_SOCIOS, null=True, blank=True)
    #organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE,
    #                                                               verbose_name='Nombre de la organización')
    #desde_cuando = models.IntegerField(choices=CHOICE_DESDE_CUANDO)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(Encuesta, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s' % (self.entrevistado.nombre)

    class Meta:
        verbose_name_plural = 'ENCUESTAS'

class ComposicionFamilia(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    #Número de personas que depende de la finca (Familia)
    varones_adultos = models.IntegerField('Número de adultos varones')
    mujeres_adultas = models.IntegerField('Número de adultas mujeres')
    varones_jovenes = models.IntegerField('Número de Jóvenes varones (11-16 años)')
    mujeres_jovenes = models.IntegerField('Número de Jóvenes mujeres (11-16 años)')
    ninos = models.IntegerField('Números niños (1-10 años)')
    ninas = models.IntegerField('Números niñas (1-10 años)')
    #Relación finca-vivienda (relacionado al dueño(s) de la finca)
    viven_finca = models.IntegerField('Viven en la finca', choices=CHOICE_SI_NO)
    viven_pueblo = models.IntegerField('Viven en pueblo cerca de finca', choices=CHOICE_SI_NO)
    viven_ciudad = models.IntegerField('Vive en ciudad lejos de la finca', choices=CHOICE_SI_NO)
    viven_finca_casa = models.IntegerField('Pasa tiempo en la finca y tiempo en casa', choices=CHOICE_SI_NO)
    #Número de trabajadores que laboran en la finca
    permanentes_hombre = models.IntegerField('Trabajadores permanentes hombres')
    permanentes_mujeres = models.IntegerField('Trabajadores permanentes mujeres')
    temporal_hombre = models.IntegerField('Trabajadores temporales hombres')
    temporal_mujeres = models.IntegerField('Trabajadores temporales mujeres')
    tecnicos_hombre = models.IntegerField('Trabajadores tecnicos hombres')
    tecnicos_mujeres = models.IntegerField('Trabajadores tecnicos mujeres')
    #nivel de educacion del duano de la finca
    nivel_educacion = models.IntegerField(choices=CHOICE_EDUCACION,
                                    verbose_name='Nivel de educación de dueño de la finca?')

    class Meta:
        verbose_name_plural = '1.2 Composicion del grupo familia'

class ServiciosFinca(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    electricidad = MultiSelectField('Disponibilidad de energía',
                                                choices=CHOICE_ELECTRICIDAD, null=True, blank=True)
    combustible = MultiSelectField('Combustible en la cocina',
                                                choices=CHOICE_COMBUSTIBLE, null=True, blank=True)
    agua_trabajo = MultiSelectField('Fuente de agua para trabajo de la finca',
                                                choices=CHOICE_FUENTE_AGUA, null=True, blank=True)
    agua_consumo = MultiSelectField('Fuente de agua para consumo humano en la finca',
                                                choices=CHOICE_FUENTE_AGUA2, null=True, blank=True)


    class Meta:
        verbose_name_plural = '1.3 Servicios  básicos en la finca'

class Tenencia(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    tenencia_parcela = models.IntegerField(choices=CHOICE_TENENCIA)
    documento_legal = models.IntegerField(choices=CHOICE_DOCUMENTO_LEGAL)

    class Meta:
        verbose_name_plural = '1.4 Tenencia de la tierra'

class SeguridadAlimentario(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    alimentos_basico = models.IntegerField('¿Qué parte de alimentos básicos que consume la familia se compra?',
                                    choices=CHOICE_ALIMENTOS_COMPRA)
    cubrir_necesidades_basicas = models.IntegerField('¿Siente que en algunos años no ha podido cubrir las necesidades básicas de alimentación de la familia o la finca?',
                                                        choices=CHOICE_SI_NO)
    meses_dificiles = MultiSelectField('¿Cuáles son los meses más difíciles para la alimentación de la familia o la finca?',
                                                choices=CHOICE_MESES, null=True, blank=True)

    class Meta:
        verbose_name_plural = '1.5 Seguridad alimentaria de la familia'

class UsoTierra(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    tierra = models.IntegerField(choices=CHOICE_TIERRA, verbose_name='Uso de la tierra')
    manzanas = models.FloatField('Área')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)

    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.manzanas / 1.4
        if self.unidad == 2:
            self.area_ha = self.manzanas / 1.0
        if self.unidad == 3:
            self.area_ha = self.manzanas / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.manzanas) / 10000
        if self.unidad == 5:
            self.area_ha = self.manzanas / 24.0

        super(UsoTierra, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = '2.1 Uso de la tierra en la finca'

class Rubros(models.Model):
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Rubros'
        unique_together = ('nombre',)

class RubrosFinca(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    rubros = models.ForeignKey(Rubros, on_delete=models.CASCADE)
    producen_finca = models.IntegerField(choices=CHOICE_SI_NO)
    fin_produccion = models.IntegerField(choices=CHOICES_FIN_PRODUCCION)

    class Meta:
        verbose_name_plural = '2.3 Rubros en la finca'

# 3 sistema produccion cacao en la finca

class AreasCacao(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=CHOICES_ANIOS)
    area_total = models.FloatField('Área total')
    area_desarrollo = models.FloatField('Área desarrollo')
    area_produccion = models.FloatField('Área producción')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA2, verbose_name='Unidad de medida')

    area_total_ha = models.FloatField(editable=False, null=True, blank=True)
    area_desarrollo_ha = models.FloatField(editable=False, null=True, blank=True)
    area_produccion_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_total_ha = self.area_total / 1.4
            self.area_desarrollo_ha = self.area_desarrollo / 1.4
            self.area_produccion_ha = self.area_produccion / 1.4
        if self.unidad == 2:
            self.area_total_ha = self.area_total / 1.0
            self.area_desarrollo_ha = self.area_desarrollo / 1.0
            self.area_produccion_ha = self.area_produccion / 1.0
        if self.unidad == 3:
            self.area_total_ha = self.area_total / 16.0
            self.area_desarrollo_ha = self.area_desarrollo / 16.0
            self.area_produccion_ha = self.area_produccion / 16.0
        if self.unidad == 4:
            self.area_total_ha = float(self.area_total) / 10000
            self.area_desarrollo_ha = float(self.area_desarrollo) / 10000
            self.area_produccion_ha = float(self.area_produccion) / 10000
        if self.unidad == 5:
            self.area_total_ha = self.area_total / 24.0
            self.area_desarrollo_ha = self.area_desarrollo / 24.0
            self.area_produccion_ha = self.area_produccion / 24.0

        super(AreasCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = '3.1A Áreas de cacao'

class ProduccionCacao(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=CHOICES_ANIOS)
    produccion_total = models.FloatField('Producción total')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_CACAO, verbose_name='Unidad de medida')

    #TODO: aca es produccion en kg mal nombre de campo deberia ser produccion_total_kg :XD
    produccion_total_qq = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.produccion_total_qq = (self.produccion_total * 0.363) * 45.36
        if self.unidad == 2:
            self.produccion_total_qq = self.produccion_total * 45.36
        if self.unidad == 3:
            self.produccion_total_qq = self.produccion_total

        super(ProduccionCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = '3.1b Producción de cacao'

class Factores(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    factor_socioeconomico = MultiSelectField('Factores socioeconómicos',
                                                choices=CHOICE_LIMITANTE_PRODUCCION,
                                                null=True, blank=True)
    factor_biofisico = MultiSelectField('Factores limitantes biofísicos',
                                                choices=CHOICES_LIMITANTE_BIOFISICO,
                                                null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.2 Factores Limitantes la Producción de Cacao'

#3.4 Manejo de los cacaotales en el ultimo year

class LaProduccion(models.Model):
     encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
     tiene_vivero = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Actualmente tiene vivero de cacao en la finca?')
     cuantas = models.FloatField('¿Cuántas plantas hay en el vivero?', null=True, blank=True)
     variedad = MultiSelectField('¿Qué variedades hay en el vivero?', choices=CHOICES_VARIEDADES,null=True, blank=True)
     consiguen = MultiSelectField('¿Actualmente donde consiguen las semillas?', choices=CHOICES_CONSIGUEN_SEMILLA, null=True, blank=True)

     class Meta:
        verbose_name_plural = '3.3 La producción de vivero y su disponibilidad a los productores'

class CalidadManejo(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_1)
    poda = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.1- Calidad de manejo'

class NivelManejo(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_2)
    poda = models.IntegerField(choices=CHOICES_USO_MANEJO, verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.2- Nivel de uso de manejo'


class MesLabores(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_3)
    poda = MultiSelectField(choices=CHOICE_MESES,verbose_name='Poda de cacao', null=True, blank=True)
    sombra = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de sombra', null=True, blank=True)
    maleza = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de malezas', null=True, blank=True)
    fertilidad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Fertilidad de Suelo', null=True, blank=True)
    enfermedad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Enfermeda des (Hongos)', null=True, blank=True)
    plagas_insectos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Insectos)', null=True, blank=True)
    plagas_nematodos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Nematodos)', null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.4.3- Mes que realizaron los labores'

class RealizanLabores(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_4)
    poda = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Poda de cacao', null=True, blank=True)
    sombra = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de sombra', null=True, blank=True)
    maleza = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de malezas', null=True, blank=True)
    fertilidad = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Fertilidad de Suelo', null=True, blank=True)
    enfermedad = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Enfermeda des (Hongos)', null=True, blank=True)
    plagas_insectos = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Plagas (Insectos)', null=True, blank=True)
    plagas_nematodos = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Plagas (Nematodos)', null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.4.4- Quiénes realizan las labores?'

class OpcionesAgroecologicas(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opciones = models.IntegerField(choices=CHOICES_OPCIONES_AGRO)
    nivel = models.IntegerField(choices=CHOICES_ESCALAS)

    class Meta:
        verbose_name_plural = '3.6 Uso de opciones agroecológicas en los cacaotales'

class Cosecha(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    cortes = models.IntegerField(choices=CHOICES_CORTES, verbose_name='¿Cómo realizaron los cortes?')
    separan_mazorca = models.IntegerField(choices=CHOICES_SEPARAN_MAZORCA, verbose_name='¿Separan diferentes tipos de mazorcas?')
    quiebran_mazorca = models.IntegerField(choices=CHOICES_QUIEBRAN_MAZORCA, verbose_name='¿Cómo quiebran las mazorcas?')
    calidad  = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Conoce la calidad de su cacao?')
    determina_calidad = models.CharField('¿Quién determino la calidad de su cacao?', max_length=250, null=True, blank=True)
    precio  = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Reciben un sobre precio para su cacao?')
    cuanto = models.FloatField('¿Cuánto por qq seco?', null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.7 Cosecha'

class OpcionesCadena(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = MultiSelectField(choices=CHOICES_OPCION_CADENA,
                                                verbose_name='Opciones de tipo de cadena ',
                                                null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.8.1 Opciones de tipo de cadena '

class Comercializacion(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_COMERCIALIZACION)
    cantidad_total = models.FloatField('Cantidad en qq Total')
    intermediario = models.FloatField('Intermediarios Cantidad en qq')
    intermediario_precio = models.FloatField('Intermediarios Precio pagado por qq')
    coop_cantidad = models.FloatField('Cooperativa/Empresa Cantidad en qq')
    coop_precio = models.FloatField('Cooperativa/Empresa Precio pagado en qq')
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, null=True, blank=True)

    intermediario_precio_usd = models.FloatField(editable=False, null=True, blank=True)
    coop_precio_usd = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.intermediario_precio_usd = convert_money(Money(self.intermediario_precio, str(self.get_moneda_display())), 'USD')
        self.coop_precio_usd = convert_money(Money(self.coop_precio, str(self.get_moneda_display())), 'USD')
        super(Comercializacion, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = '3.8.2 Volumen y precio'

class Credito(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_CREDITO)
    monto_credito_corto = models.FloatField('Monto de crédito corto plazo')
    monto_credito_mediano = models.FloatField('Monto de crédito de mediano plazo')
    monto_credito_largo = models.FloatField('Monto de crédito de largo plazo')
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, null=True, blank=True)
    facilidad = models.IntegerField(choices=CHOICES_FACILIDAD, verbose_name='Facilidad de pago')

    monto_credito_corto_usd = models.FloatField(editable=False, null=True, blank=True)
    monto_credito_mediano_usd = models.FloatField(editable=False, null=True, blank=True)
    monto_credito_largo_usd = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.monto_credito_corto_usd = convert_money(Money(self.monto_credito_corto, str(self.get_moneda_display())), 'USD')
        self.monto_credito_mediano_usd = convert_money(Money(self.monto_credito_mediano, str(self.get_moneda_display())), 'USD')
        self.monto_credito_largo_usd = convert_money(Money(self.monto_credito_largo, str(self.get_moneda_display())), 'USD')
        super(Credito, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = '3.9.1 Crédito para producción de cacao'

class ObtieneCredito(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    obtiene_credito = MultiSelectField(choices=CHOICES_OBTIENE_CREDITO,
                                    verbose_name='¿De quién obtiene el crédito?')

    class Meta:
        verbose_name_plural = '3.9.2 Obtiene crédito para producción de cacao'

class MitigacionRiesgo(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    monitoreo_plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Realiza monitoreo de plagas y enfermedades?')
    cada_cuanto = models.IntegerField(choices=CHOICES_CADA_CUANTO,
                                verbose_name='¿Cada cuánto realiza monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    como_realiza = models.IntegerField(choices=CHOICES_COMO_REALIZA,
                                verbose_name='¿Cómo realiza monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    lleva_registro = models.IntegerField(choices=CHOICES_LLEVA_REGISTROS,
                                verbose_name='¿Si lleva registro de monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    cuenta_recursos = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con suficiente recursos para manejo de finca?')
    falta_recurso = MultiSelectField(choices=CHOICES_FALTA_RECURSO,
                                verbose_name='¿Para qué cosas hace falta los recursos?',
                                null=True, blank=True)
    obras_almacenamiento = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con obras para almacenamiento de agua?')
    venta_organizada = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Vende su Cacao en forma organizada?')
    contrato_venta = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un contrato para la venta de cacao?')
    certificado = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Está certificado los cacaotales?')
    tipo_certificado = MultiSelectField(choices=CHOICES_TIPO_CERTIFICADO,
                                verbose_name='¿Qué tipo de certificación?')
    calidad_cacao = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿La calidad de su cacao en reconocida y monitoreada?')
    plan_manejo = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con plan de manejo de la finca?')
    plan_negocio = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un plan de negocio para el cultivo de cacao?')
    plan_inversion = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un plan de inversión para el cultivo de cacao?')
    apoya_planes = MultiSelectField(choices=CHOICES_APOYA_PLANES,
                                verbose_name='¿Quién apoyo para elaborar estos planes?')

    class Meta:
        verbose_name_plural = '3.10 Mitigación de los riesgos'

class CapacitacionTecnicas(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_CAPACITACION_TECNICA)
    cuantas_veces = models.IntegerField('¿Cuántas veces?')
    capacito = models.CharField('¿Quién le capacitó?', max_length=250)
    nivel_consideracion = models.IntegerField(choices=CHOICES_NIVEL_CONOCIMIENTO,
                                            verbose_name='¿Cómo considera su nivel de conocimiento?')

    class Meta:
        verbose_name_plural = '3.11 Capacitaciones técnica recibida en el último año'

class CapacitacionSocial(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    tema = models.IntegerField(choices=CHOICES_TEMAS_SOCIAL)
    cuantas_veces = models.IntegerField('¿Cuántas veces?')
    capacito = models.CharField('¿Quién le capacitó?', max_length=250)
    nivel_consideracion = models.IntegerField(choices=CHOICES_NIVEL_CONOCIMIENTO,
                                            verbose_name='¿Cómo considera su nivel de conocimiento')

    class Meta:
        verbose_name_plural = '3.12 Capacitaciones en tema social recibidas en el último año'

class DetallePlantios(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    nombre_plantio = models.CharField('Nombre de la parcela', max_length=150)
    area = models.FloatField('Área')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    tipo_planta = models.IntegerField(choices=CHOICE_TIPO_PLANTA)
    edad = models.FloatField('Edad en años')
    variedades = MultiSelectField(choices=CHOICES_VARIEDAD_PREDOMINANTE,
                                verbose_name='Variedades Predominante' )
    produccion = models.FloatField('Producción en último año')
    unidad_pro = models.IntegerField(choices=CHOICE_UNIDAD_CACAO, verbose_name='Unidad')
    nivel = models.FloatField('Nivel de Monilia año pasado basado en % mazorcas afectadas')
    #final de la encuesta
    tipo_sombra = models.IntegerField(choices=CHOICE_TIPO_SOMBRA)
    nivel_sombra = models.FloatField('Nivel de sombra %')
    tipo_fertilizacion = models.IntegerField(choices=CHOICE_TIPO_FERTILIZACION)
    tipo_fungicida = models.IntegerField(choices=CHOICE_TIPO_FUNGICIDA)
    rendimientos = models.FloatField(editable=False)

    area_ha = models.FloatField(editable=False, null=True, blank=True)
    produccion_kg = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1.0
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = self.area / float(10000)
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        if self.unidad_pro == 1:
            self.produccion_kg = (self.produccion * 0.363) * 45.36
        if self.unidad_pro == 2:
            self.produccion_kg = self.produccion * 45.36
        if self.unidad_pro == 3:
            self.produccion_kg = self.produccion

        self.rendimientos = float(self.produccion_kg) / float(self.area_ha)

        super(DetallePlantios, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Detalles de algunos plantíos de cacao (en base de visita de campo)'

class DetalleSeleccion(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    parcela_seleccionada = models.CharField('Parcela Seleccionado como parcela de aprendizaje?', max_length=250)
    porque = models.TextField()

    class Meta:
        verbose_name_plural = 'Parcela seleccionada de aprendizaje'


#aca el modelo para customizar el user
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    organizacion = models.ForeignKey(OrganizacionApoyo,
                                                                    null=True, blank=True,
                                                                    verbose_name="Organización que pertenece")
    foto_perfil = models.ImageField(upload_to='avatare/', null=True, blank=True)

    class Meta:
        verbose_name = 'Perfil del usuario'
        verbose_name_plural = "Perfiles de los usuarios"
