# -*- coding: utf-8 -*-
CHOICE_SEXO = (
                (1, 'Mujer'),
                (2, 'Hombre'),
              )

CHOICE_MANEJA_FINCA = (
                (1, 'Hombre'),
                (2, 'Mujer'),
                (3, 'Ambos'),
                (4, 'Entre varios')
              )

CHOICE_DESDE_CUANDO = (
                (1, 'Menos de 5 años'),
                (2, 'Más de 5 años'),
                (3, 'Ninguno')
              )

CHOICE_SOCIOS = (
                ('A', 'Cooperativa'),
                ('B', 'Asociación'),
                ('C', 'Empresa'),
                ('D', 'Grupo'),
                ('E', 'Ninguno')
              )

CHOICE_EDUCACION = (
                (1, 'No Sabe Leer/Escribir'),
                (2, 'Primaria Incompleta'),
                (3, 'Primaria completa'),
                (4, 'Secundaria Incompleta'),
                (5, 'Bachiller'),
                (6, 'Universidad')
              )

CHOICE_ELECTRICIDAD = (
                ('A', 'No hay'),
                ('B', 'La red'),
                ('C', 'Planta eléctrica'),
                ('D', 'Panel Solar'),
                ('E', 'Molino de viento'),
                ('F', 'Candil')
              )

CHOICE_COMBUSTIBLE = (
                ('A', 'Gas'),
                ('B', 'Leña de la finca'),
                ('C', 'Leña comprada'),
                ('D', 'Carbón'),
                ('E', 'Candela'),
                ('F', 'Foco')
              )

CHOICE_FUENTE_AGUA = (
                ('A', 'Río'),
                ('B', 'Ojo de agua'),
                ('C', 'Quebrada'),
                ('D', 'Pozo comunitario'),
                ('E', 'Pozo propio'),
                ('F', 'Agua entubada')
              )

CHOICE_FUENTE_AGUA2 = (
                ('A', 'Río'),
                ('B', 'Ojo de agua'),
                ('C', 'Quebrada'),
                ('D', 'Pozo comunitario'),
                ('E', 'Pozo propio'),
                ('F', 'Agua entubada'),
                ('G', 'Botellón'),
              )

CHOICE_TENENCIA = (
                (1, 'Propia con escritura pública'),
                (2, 'Propias con Promesa de venta'),
                (3, 'Propias con Escritura posesoria'),
                (4, 'Propia por herencia'),
                (5, 'Propias con título de reforma agraria'),
                (6, 'Propia Sin documento'),
                (7, 'Parcela en tierra comunitaria'),
                (8, 'Arrendada')
              )

CHOICE_DOCUMENTO_LEGAL = (
                (1, 'Hombre'),
                (2, 'Mujer'),
                (3, 'Mancomunado'),
                (4, 'Parientes'),
                (5, 'Colectivo'),
                (6, 'No hay'),
              )

CHOICE_ALIMENTOS_COMPRA = (
                (1, 'Todo'),
                (2, 'Más de la Mitad'),
                (3, 'Menos de la Mitad'),
                (4, 'Nada'),
              )

CHOICE_SI_NO = (
                (1, 'Si'),
                (2, 'No'),
              )

CHOICE_MESES = (
                ('A', 'Enero'),
                ('B', 'Febrero'),
                ('C', 'Marzo'),
                ('D', 'Abril'),
                ('E', 'Mayo'),
                ('F', 'Junio'),
                ('G', 'Julio'),
                ('H', 'Agosto'),
                ('I', 'Septiembre'),
                ('J', 'Octubre'),
                ('K', 'Noviembre'),
                ('L', 'Diciembre'),
              )

CHOICE_UNIDAD_TIERRA = (
                (1, 'Mz'),
                (2, 'Ha'),
                (3, 'Tarea DOM'),
                (4, 'Mt2'),
                (5, 'Tarea GUATE'),
              )

CHOICE_UNIDAD_TIERRA2 = (
                (1, 'Mz'),
                (2, 'Ha'),
                (3, 'Tarea DOM'),
                (4, 'Mt2'),
                (5, 'Tarea GUATE'),
              )

CHOICE_UNIDAD_CACAO = (
                (1, 'qq baba/verde'),
                (2, 'qq seco'),
                (3, 'Kg'),
              )

CHOICE_TIERRA = (
        (1, 'Área total'),
        (2, 'Bosque primario'),
        (3, 'Bosque secundario'),
        (4, 'Cultivos anuales'),
        (5, 'Plantación forestal'),
        (6, 'Áreas de pastos abiertos'),
        (7, 'Áreas de pastos con árboles'),
        (8, 'Cultivos perennes'),
    )

CHOICES_FIN_PRODUCCION = (
                    (1, 'Autoconsumo'),
                    (2, 'Venta'),
                    (3, 'Ambos'),
    )

CHOICES_ESTADO_ACTUAL = (
                    (1, 'Área total de cacao'),
                    (2, 'Área de cacao en desarrollo'),
                    (3, 'Área de cacao en producción'),
                    (4, 'Producción total de cacao'),
    )

CHOICES_ANIOS = (
                (3, '2016'),
                (4, '2017'),
                (5, '2018'),
                # (6, '2019'),
                # (7, '2020'),
                # (8, '2021'),
                # (9, '2022'),
                # (10, '2023'),
        )

CHOICE_LIMITANTE_PRODUCCION = (
                ('A', 'Falta de crédito'),
                ('B', 'Comercialización'),
                ('C', 'Falta de mano de obra'),
                ('D', 'Falta de capacitación / asistencia técnica'),
              )

CHOICES_LIMITANTE_BIOFISICO = (
                ('A', 'Fertilidad de suelo'),
                ('B', 'Variedades no productivas'),
                ('C', 'Monilia'),
                ('D', 'Ardillas'),
                ('E', 'Sombra/Poda'),
                ('F', 'Exceso de lluvia'),
                ('G', 'Falta de lluvia'),
    )

CHOICES_VARIEDADES = (
                ('A', 'Clone'),
                ('B', 'Híbrido'),
                ('C', 'Cacao criollo'),
                ('D', 'Planta élite'),
                ('E', 'Mezcla'),
    )

CHOICES_CONSIGUEN_SEMILLA = (
                ('A', 'La misma finca'),
                ('B', 'Fincas vecinas'),
                ('C', 'Vivero Cooperativa'),
                ('D', 'Vivero Empresa'),
    )

CHOICES_MANEJO_1 = (
                (1, 'Calidad de manejo'),
        )

CHOICES_MANEJO_2 = (
                (1, 'Nivel de uso de manejo'),
        )

CHOICES_MANEJO_3 = (
                (1, 'Mes que realizaron los labores'),
        )

CHOICES_MANEJO_4 = (
                (1, 'Quiénes realizan las labores?'),
        )

CHOICES_CALIDAD = (
                (1, 'Bueno'),
                (2, 'Regular'),
                (3, 'Mal'),
                (4, 'No realiza'),
        )

CHOICES_USO_MANEJO = (
                (1, 'En todos los plantios'),
                (2, 'En varios platios'),
                (3, 'Solamente en algunos plantíos'),
                (4, 'En ninguno plantío'),
        )

CHOICES_REALIZAN_LABOR = (
                ('A', 'Hombre'),
                ('B', 'Mujeres'),
                ('C', 'Niños o Niñas'),
                ('D', 'Trabajadores'),
    )

CHOICES_OPCIONES_AGRO = (
                (1, 'Biofertilzantes'),
                (2, 'Compost'),
                (3, 'Roca Minerales'),
                (4, 'Insecticida natural'),
                (5, 'Fungicida natural'),
                (6, 'Cerca viva'),
                (7, 'Cortina rompe viento'),
                (8, 'Abonos verdes'),
                (9, 'Siembra en Curva a nivel'),
                (10, 'Acequia'),
                (11, 'Barrera viva'),
                (12, 'Barrera muerta'),
                (13, 'Cosecha de agua'),
                (14, 'Incorporación de rastrojo'),
                (15, 'Manejo selectivo de malas hierbas'),
                (16, 'Siembra de coberturas'),
                (17, 'Siembra de plantas injertadas'),
                (18, 'Diversificación de los cacaotales'),
                (19, 'Eliminación de mazorcas enfermas'),
        )

CHOICES_ESCALAS = (
                (1, 'No utiliza'),
                (2, 'En pequeño escala'),
                (3, 'En mayor escala'),
                (4, 'En toda la finca'),
            )

CHOICES_CORTES = (
                (1, 'Con machete'),
                (2, 'Con tijera'),
                (3, 'Con media luna'),
                (4, 'Con dejarretadora'),
            )

CHOICES_SEPARAN_MAZORCA = (
                (1, 'Si'),
                (2, 'No'),
                (3, 'Algunas veces'),
            )

CHOICES_QUIEBRAN_MAZORCA = (
                (1, 'Con Cutacha'),
                (2, 'Con mazo'),
                (3, 'Con piedra'),
                (4, 'Con machete'),
            )

CHOICES_COMERCIALIZACION = (
                (1, 'Cacao baba/ verde'),
                (2, 'Cacao seco fermentado'),
                (3, 'Cacao seco no fermentado'),
            )

CHOICES_CREDITO = (
                (1, 'Ultimo año'),
            )

CHOICES_FACILIDAD = (
                (1, 'Facil'),
                (2, 'Regular'),
                (3, 'Dificil'),
            )

CHOICES_OBTIENE_CREDITO = (
                (1, 'Empresa Ex'),
                (2, 'ONG'),
                (3, 'Micro Finanzas'),
                (4, 'Banco'),
                (5, 'Coop'),
                (6, 'Proyecto'),
            )

CHOICES_CADA_CUANTO = (
            (1,'Mensual'),
            (2,'Cada 3 meses'),
            (3,'Cada 6 meses'),
            (4,'Anual'),
    )

CHOICES_COMO_REALIZA = (
            (1,'Observaciones'),
            (2,'Recuentos'),
    )

CHOICES_LLEVA_REGISTROS = (
            (1,'Si'),
            (2,'No'),
            (3, 'Si y Procesa y usa los datos')
    )

CHOICES_FALTA_RECURSO = (
            (1,'Insumos'),
            (2,'Pago de Mano de obra'),
            (3, 'Gastos operativos'),
            (4, 'Inversiones'),
    )

CHOICES_TIPO_CERTIFICADO = (
            ('A','CJ'),
            ('B','ORG'),
            ('C', 'RFA'),
            ('D', 'UTZ'),
            ('E', 'SPP'),
            ('F', 'No Certificado'),
    )

CHOICES_APOYA_PLANES = (
            ('A','Propio'),
            ('B','Contrate AT'),
            ('C', 'ONG'),
            ('D', 'Empresas'),
            ('E', 'Cooperativa'),
            ('F', 'Asociación'),
            ('G', 'Banco o Micro-finanza'),
            ('H', 'Nadie apoya'),
    )

CHOICES_CAPACITACION_TECNICA = (
            (1,'Variedades de cacao'),
            (2,'Manejo de semillero y vivero'),
            (3,'Establecimiento y manejo de cacao'),
            (4,'Manejo de suelo y fertilidad de suelo'),
            (5,'Manejo de cosecha de cacao'),
            (6,'Manejo de plagas y enfermedades'),
            (7,'Poda/ Manejo de sombra'),
            (8,'Enjertación'),
            (9,'Planificación de la finca'),
            (10,'Diversificación de la finca'),
    )

CHOICES_NIVEL_CONOCIMIENTO = (
            (1,'Pobre'),
            (2,'Algo'),
            (3,'Regular'),
            (4,'Bueno'),
            (5,'Excelente'),
    )

CHOICES_TEMAS_SOCIAL = (
            (1,'Formación y fortalecimiento organizacional'),
            (2,'Contabilidad básica y administración'),
            (3,'Manejo de créditos'),
            (4,'Administración de pequeños negocios'),
            (5,'Gestión empresarial'),
            (6,'Registro de datos de la finca'),
            (7,'Certificación de cacao'),

    )

CHOICE_TIPO_PLANTA = (
            (1,'En desarrollo'),
            (2,'Productivo'),
            (3,'Mezcla'),
    )

CHOICES_VARIEDAD_PREDOMINANTE = (
            ('A','Clone'),
            ('B','Hibrido'),
            ('C', 'Mezcla'),
           ('D', 'Criolla'),
           ('E', 'Planta élite'),
    )

CHOICE_TIPO_SOMBRA = (
            (1,'Montaña'),
            (2,'Guineo'),
            (3,'Mezclado'),
            (4,'Musaceas'),
            (5,'Árboles nativos'),
            (6,'Amapola'),
    )

CHOICE_TIPO_FERTILIZACION = (
            (1,'Orgánico'),
            (2,'Químico'),
            (3,'Quimico + Orgánico'),
            (4,'Ninguno'),
    )

CHOICE_TIPO_FUNGICIDA = (
            (1,'Quimico Prev'),
            (2,'Quimico Sist'),
            (3,'Orgánico'),
            (4,'Ninguno'),
    )

CHOICE_MONEDA_LOCAL = (
    (1, 'NIO'),
    (2, 'HNL'),
    (3, 'USD'),
    (4, 'DOP'),
    (5, 'GTQ'),
    )


CHOICES_OPCION_CADENA = (
        ('A','Cosecha – Seca en finca- Entrega cacao seco no fermentado a Empresa/Cooperativa'),
        ('B','Cosecha- Seca en finca – Entrega cacao seco no fermentado a Intermediarios'),
        ('C', 'Cosecha- Entrega cacao  baba/verde a Empresa/Cooperativa'),
        ('D', 'Cosecha – Entrega cacao baba/verde a Intermediarios'),
        ('E', 'Cosecha -  Fermenta en finca – Entrega cacao seco fermentado a Empresa/Cooperativa'),
        ('F', 'Cosecha -  Fermenta en finca – Entrega cacao seco fermentado a Intermediario'),
    )
