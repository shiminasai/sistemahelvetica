# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView
from django.db.models import Avg, Sum, Count,  Value as V
from django.db.models.functions import Coalesce
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from lugar.models import Comunidad, Departamento, Municipio, Pais
from .models import *
from .choices_static import *
from .forms import ConsultaLineaBaseForm, ConsultaEstablecimientoForm,FormularioColabora
from dal import autocomplete
import json as simplejson
from collections import OrderedDict
import numpy as np

# Create your views here.

class EntrevistadosAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Entrevistados.objects.none()

        qs = Entrevistados.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

def _queryset_filtrado(request):
    params = {}

    if 'fecha' in request.session:
       params['year__in'] = request.session['fecha']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion' in request.session:
        params['entrevistado__organizacion_apoyo'] = request.session['organizacion']

    if 'pais' in request.session:
       params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print "paramettros"
    print params
    return Encuesta.objects.filter(**params).select_related()

def consulta_herramienta(request, template='lineaBase/consultaLinea.html'):
    if request.method == 'POST':
        form = ConsultaLineaBaseForm(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            #request.session['ciclo'] = form.cleaned_data['ciclo']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['organizacion'] = form.cleaned_data['organizacion']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            #request.session['tipologia'] = form.cleaned_data['tipologia']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaLineaBaseForm()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['ciclo']
                del request.session['productor']
                del request.session['organizacion_pertenece']
                del request.session['organizacion_apoyo']
                del request.session['pais']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def consulta_establecimiento(request, template='lineaBase/consultaEstablecimiento.html'):
    if request.method == 'POST':
        form = ConsultaEstablecimientoForm(request.POST)
        if form.is_valid():
            request.session['ciclo'] = form.cleaned_data['ciclo']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['organizacion_pertenece'] = form.cleaned_data['organizacion_pertenece']
            request.session['organizacion_apoyo'] = form.cleaned_data['organizacion_apoyo']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['sexo'] = form.cleaned_data['sexo']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaEstablecimientoForm()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['ciclo']
                del request.session['productor']
                del request.session['organizacion_pertenece']
                del request.session['organizacion_apoyo']
                del request.session['pais']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
                #del request.session['tipologia']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def get_view(request, vista):
    if vista in VALID_VIEWS:
        return VALID_VIEWS[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'lineabase.views'))

def homePageViewLineaBase(request, template='lineaBase/indexlineaBase.html'):
    familias = Encuesta.objects.count()
    mujeres = Encuesta.objects.filter(entrevistado__sexo=1).count()
    hombres = Encuesta.objects.filter(entrevistado__sexo=2).count()
    organizacion = Organizacion.objects.count()
    return render(request, template, locals())

def organizacion_pertenece(request, template='herramientaDesicion/educacion.html'):
    filtro = _queryset_filtrado(request)
    num_familias = filtro.count()
    years = request.session['fecha']

    tabla_socio = {}
    tabla_organizaciones = {}
    tabla_desde = {}
    num_familias2 = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        tabla_socio[year] = {}
        tabla_desde[year] = {}
        tabla_organizaciones[year] = {}
        tabla_org = []

        for obj in CHOICE_SOCIOS:
            contar = filtro.filter(year=year,socio__contains=obj[0]).count()
            if contar > 0:
                tabla_socio[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        for obj in Organizacion.objects.all():
            conteo = filtro.filter(year=year,entrevistado__organizacion_pertenece=obj).count()
            if conteo > 0:
                tabla_org.append([obj.nombre,conteo])
        tabla_organizaciones[year]  = tabla_org

        for obj in CHOICE_DESDE_CUANDO:
            contar = filtro.filter(year=year,entrevistado__desde_cuando=obj[0]).count()
            if contar > 0:
                tabla_desde[year][obj[1]] = [contar,filtro.filter(year=year).count()]

    return render(request, template, locals())

def composicion(request, template='herramientaDesicion/composicion.html'):
    filtro = _queryset_filtrado(request)
    num_familias = filtro.count()
    years = request.session['fecha']

    composicion = {}
    grafo_educacion={}
    num_familias2 = {}
    for year in years:
        grafo_educacion[year]={}
        num_familias2[year] = filtro.filter(year=year).count()
        #composcion de la familia
        conteo_adultos_varones = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__varones_adultos'))['valor']
        conteo_adultos_mujeres = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__mujeres_adultas'))['valor']
        conteo_varones_jovenes = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__varones_jovenes'))['valor']
        conteo_mujeres_jovenes = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__mujeres_jovenes'))['valor']
        conteo_ninos = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__ninos'))['valor']
        conteo_ninas = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__ninas'))['valor']
        #relacion finca-vivienda
        conteo_viven_finca = filtro.filter(year=year).filter(composicionfamilia__viven_finca=1).count()
        conteo_viven_pueblo = filtro.filter(year=year).filter(composicionfamilia__viven_pueblo=1).count()
        conteo_viven_ciudad = filtro.filter(year=year).filter(composicionfamilia__viven_ciudad=1).count()
        conteo_viven_finca_casa = filtro.filter(year=year).filter(composicionfamilia__viven_finca_casa=1).count()
        #trabajadres laboran finca
        conteo_permanentes_hombre = filtro.filter(year=year).aggregate(valor=Sum('composicionfamilia__permanentes_hombre'))['valor']
        conteo_permanentes_mujeres = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__permanentes_mujeres'))['valor']
        conteo_temporal_hombre = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__temporal_hombre'))['valor']
        conteo_temporal_mujeres = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__temporal_mujeres'))['valor']
        conteo_tecnicos_hombre = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__tecnicos_hombre'))['valor']
        conteo_tecnicos_mujeres = filtro.filter(year=year).aggregate(valor=Avg('composicionfamilia__tecnicos_mujeres'))['valor']

        #trabajadores laboran finca
        permanentes_hombre_lista = len(filtro.filter(year=year,composicionfamilia__permanentes_hombre__gte=1).values_list('composicionfamilia__permanentes_hombre', flat=True))
        permanentes_mujeres_lista = len(filtro.filter(year=year,composicionfamilia__permanentes_mujeres__gte=1).values_list('composicionfamilia__permanentes_mujeres', flat=True))
        temporal_hombre_lista = len(filtro.filter(year=year,composicionfamilia__temporal_hombre__gte=1).values_list('composicionfamilia__temporal_hombre', flat=True))
        temporal_mujeres_lista = len(filtro.filter(year=year,composicionfamilia__temporal_mujeres__gte=1).values_list('composicionfamilia__temporal_mujeres', flat=True))
        tecnicos_hombre_lista = len(filtro.filter(year=year,composicionfamilia__tecnicos_hombre__gte=1).values_list('composicionfamilia__tecnicos_hombre', flat=True))
        tecnicos_mujeres_lista = len(filtro.filter(year=year,composicionfamilia__tecnicos_mujeres__gte=1).values_list('composicionfamilia__tecnicos_mujeres', flat=True))

        composicion[year] = [conteo_adultos_varones,conteo_adultos_mujeres,conteo_varones_jovenes,
                                                conteo_mujeres_jovenes,conteo_ninos,conteo_ninas,conteo_viven_finca,
                                                conteo_viven_pueblo,conteo_viven_ciudad,conteo_viven_finca_casa,
                                                conteo_permanentes_hombre,conteo_permanentes_mujeres,
                                                conteo_temporal_hombre,conteo_temporal_mujeres,conteo_tecnicos_hombre,
                                                conteo_tecnicos_mujeres,permanentes_hombre_lista,permanentes_mujeres_lista,
                                                temporal_hombre_lista,temporal_mujeres_lista,tecnicos_hombre_lista,
                                                tecnicos_mujeres_lista,filtro.filter(year=year).count()]
        #grafico de educacion
        for obj in CHOICE_EDUCACION:
            conteo = filtro.filter(year=year,composicionfamilia__nivel_educacion=obj[0]).count()
            grafo_educacion[year][obj[1]] = [conteo,filtro.filter(year=year).count()]

    return render(request, template, locals())

def servicios_basico(request, template='herramientaDesicion/servicios.html'):
    filtro = _queryset_filtrado(request)
    #num_familias = filtro.count()
    years = request.session['fecha']

    num_familias2 = {}
    tabla_electricidad = {}
    tabla_combustible = {}
    tabla_agua_trabajo = {}
    tabla_agua_consumo = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        tabla_electricidad[year] = {}
        for obj in CHOICE_ELECTRICIDAD:
            contar = filtro.filter(year=year,serviciosfinca__electricidad__contains=obj[0]).count()
            tabla_electricidad[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        tabla_combustible[year] = {}
        for obj in CHOICE_COMBUSTIBLE:
            contar = filtro.filter(year=year,serviciosfinca__combustible__contains=obj[0]).count()
            tabla_combustible[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        tabla_agua_trabajo[year] = {}
        for obj in CHOICE_FUENTE_AGUA:
            contar = filtro.filter(year=year,serviciosfinca__agua_trabajo__contains=obj[0]).count()
            tabla_agua_trabajo[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        tabla_agua_consumo[year] = {}
        for obj in CHOICE_FUENTE_AGUA:
            contar = filtro.filter(year=year,serviciosfinca__agua_consumo__contains=obj[0]).count()
            tabla_agua_consumo[year][obj[1]] = [contar,filtro.filter(year=year).count()]


    return render(request, template, {'tabla_electricidad':tabla_electricidad,
                                                                'tabla_combustible':tabla_combustible,
                                                                'tabla_agua_trabajo':tabla_agua_trabajo,
                                                                'tabla_agua_consumo':tabla_agua_consumo,
                                                                 'num_familias': filtro.count(),
                                                                 'num_familias2':num_familias2,
                                                                 'years':years})

def tenencia(request, template='herramientaDesicion/tenencia.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    tabla_tenencia= {}
    tabla_documento_legal = {}
    num_familias2 = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_tenencia[year]= {}
        for obj in CHOICE_TENENCIA:
            contar = filtro.filter(year=year,tenencia__tenencia_parcela=obj[0]).count()
            tabla_tenencia[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        tabla_documento_legal[year] = {}
        for obj in CHOICE_DOCUMENTO_LEGAL:
            contar = filtro.filter(year=year,tenencia__documento_legal=obj[0]).count()
            tabla_documento_legal[year][obj[1]] = [contar,filtro.filter(year=year).count()]


    return render(request, template, {'tabla_tenencia':tabla_tenencia,
                                                                'tabla_documento_legal':tabla_documento_legal,
                                                                 'num_familias': filtro.count(),
                                                                 'num_familias2':num_familias2,
                                                                 'years':years})

def seguridad_alimentaria(request, template='herramientaDesicion/seguridad_alimentaria.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    tabla_alimento_compra= {}
    tabla_cubrir_necesidad = {}
    tabla_meses_dificiles = OrderedDict()
    num_familias2 = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        tabla_alimento_compra[year]= {}
        for obj in CHOICE_ALIMENTOS_COMPRA:
            contar = filtro.filter(year=year,seguridadalimentario__alimentos_basico=obj[0]).count()
            tabla_alimento_compra[year][obj[1]] = [contar,filtro.filter(year=year).count()]

        tabla_cubrir_necesidad[year] = {}
        for obj in CHOICE_SI_NO:
            contar = filtro.filter(year=year,seguridadalimentario__cubrir_necesidades_basicas=obj[0]).count()
            tabla_cubrir_necesidad[year][obj[1]] = contar

        tabla_meses_dificiles[year] = OrderedDict()
        for obj in CHOICE_MESES:
            contar = filtro.filter(year=year,seguridadalimentario__meses_dificiles__contains=obj[0]).count()
            porcentaje = (float(contar) / float(filtro.filter(year=year).count())) * 100
            tabla_meses_dificiles[year][obj[1]] = (contar,porcentaje)


    return render(request, template, {'tabla_alimento_compra':tabla_alimento_compra,
                                                                'tabla_cubrir_necesidad':tabla_cubrir_necesidad,
                                                                'tabla_meses_dificiles':tabla_meses_dificiles,
                                                                 'num_familias': filtro.count(),
                                                                 'years':years,
                                                                 'num_familias2':num_familias2})

def tierra(request, template='herramientaDesicion/tierra.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']


    tabla_uso_tierra= OrderedDict()
    promedio_ha = {}
    num_familias2 = {}
    totales = {}
    rangos_conteos = OrderedDict()
    rangos_totales = {}
    cobertura_arboles = {}
    biodiversidad = {}
    carbono = {}
    indice_suelo = {}
    total= 0
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        total_familiar = filtro.filter(year=year).count()
        total_manzanas_familias = UsoTierra.objects.filter(encuesta__in=filtro.filter(year=year)).exclude(tierra=1).aggregate(t=Sum('area_ha'))['t']
        promedio_ha[year] =  filtro.filter(year=year,usotierra__tierra=1).aggregate(t=Avg('usotierra__area_ha'))['t']

        tabla_uso_tierra[year]= OrderedDict()
        porcentaje = 0
        for obj in CHOICE_TIERRA[1:]:
            query = filtro.filter(year=year,usotierra__tierra = obj[0])
            conteo = query.count()
            porcentaje = saca_porcentajes(conteo, total_familiar)
            manzana = query.aggregate(t=Sum('usotierra__area_ha'))['t']
            porcentaje_mz = saca_porcentajes(manzana,total_manzanas_familias)
            tabla_uso_tierra[year][obj[1]] = (conteo, porcentaje,manzana,porcentaje_mz)

        total= 0
        for key, values in tabla_uso_tierra.items():
            totales[key] = []
            for k,v in values.items():
                if v[2] is not None:
                    total += v[2]
                totales[key] = total

        #calculando los promedios
        rangos = {'0 Ha':(0,0), '0.1 -- 10 Ha':(0.1,10), '11 -- 25 Ha':(11,25),
                        '26 -- 35 Ha':(26,35), '36 -- 45 Ha':(36,45), '46 Ha o Mas':(46,10000)}
        rangos_conteos[year] = OrderedDict()
        for k,v in rangos.items():
            query = filtro.filter(year=year,usotierra__tierra=1,usotierra__area_ha__range=v).count()
            rangos_conteos[year][k] = (query)
        rangos_totales[year] = sum([x for k,v in rangos_conteos.items() for x in v.values()])


        bosque_p = filtro.filter(year=year,usotierra__tierra=2).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        bosque_s = filtro.filter(year=year,usotierra__tierra=3).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        c_anuales = filtro.filter(year=year,usotierra__tierra=4).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        p_forestal = filtro.filter(year=year,usotierra__tierra=5).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        a_abiertos = filtro.filter(year=year,usotierra__tierra=6).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        a_arboles = filtro.filter(year=year,usotierra__tierra=7).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']
        perennes = filtro.filter(year=year,usotierra__tierra=8).aggregate(t=Coalesce(Sum('usotierra__area_ha'), V(0)))['t']

        cobertura_arboles[year] = ((bosque_p * 1) + (bosque_s * 0.5) + (c_anuales * 0) + \
                                              (p_forestal * 0.8) + (a_abiertos * 0) + (a_arboles * 0.3) + \
                                              (perennes * 0.4)) /  total_manzanas_familias

        biodiversidad[year] = ((bosque_p * 0.9) + (bosque_s * 0.6) + (c_anuales * 0) + \
                                              (p_forestal * 0.6) + (a_abiertos * 0.2) + (a_arboles * 0.4) + \
                                              (perennes * 0.6)) /  total_manzanas_familias

        carbono[year] = ((bosque_p * 1) + (bosque_s * 0.8) + (c_anuales * 0) + \
                                              (p_forestal * 0.7) + (a_abiertos * 0.4) + (a_arboles * 0.7) + \
                                              (perennes * 0.7)) /  total_manzanas_familias

        indice_suelo[year] = ((bosque_p * 1) + (bosque_s * 1) + (c_anuales * 0.2) + \
                                              (p_forestal * 0.9) + (a_abiertos * 0.4) + (a_arboles * 0.6) + \
                                              (perennes * 0.9)) /  total_manzanas_familias



    return render(request, template, {'tabla_uso_tierra':tabla_uso_tierra,
                                                                'total_manzanas_familias':total,
                                                                'total_porcentaje': porcentaje,
                                                                'promedio_ha': promedio_ha,
                                                                'rangos_conteos':rangos_conteos,
                                                                'rangos_totales':rangos_totales,
                                                                'cobertura_arboles':cobertura_arboles,
                                                                'biodiversidad':biodiversidad,
                                                                'carbono':carbono,
                                                                'indice_suelo':indice_suelo,
                                                                 'num_familias': filtro.count(),
                                                                 'years':years,
                                                                 'num_familias2':num_familias2,
                                                                 'totales':totales})

def rubros(request, template='herramientaDesicion/rubros.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    metrics_0 = {
            'producen': Count('id'),
        }

    metrics_1 = {
            'autoconsumo': Count('id'),
        }

    metrics_2 = {
            'venta': Count('id'),
        }

    metrics_3 = {
            'ambos': Count('id'),
        }

    num_familias2 = {}
    otro_dict = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        summary_0 = list(
            filtro.filter(year=year,rubrosfinca__producen_finca=1)
            .values('rubrosfinca__rubros__nombre')
            .annotate(**metrics_0)
            .order_by('-producen')
        )
        summary_1 = list(
            filtro.filter(year=year,rubrosfinca__fin_produccion=1)
            .values('rubrosfinca__rubros__nombre')
            .annotate(**metrics_1)
            .order_by('-autoconsumo')
        )
        summary_2 = list(
            filtro.filter(year=year,rubrosfinca__fin_produccion=2)
            .values('rubrosfinca__rubros__nombre')
            .annotate(**metrics_2)
            .order_by('-venta')
        )
        summary_3 = list(
            filtro.filter(year=year,rubrosfinca__fin_produccion=3)
            .values('rubrosfinca__rubros__nombre')
            .annotate(**metrics_3)
            .order_by('-ambos')
        )

    #magia = [{**a, **b, **c} for a, b, c in zip(listone, listtwo,listthree)]
        otro_dict[year] = {}
        for x in range(len(summary_0)):
            #if summary_0[x]['rubrosfinca__rubros__nombre'] == summary_1[x]['rubrosfinca__rubros__nombre'] and summary_2[x]['rubrosfinca__rubros__nombre']  == summary_3[x]['rubrosfinca__rubros__nombre']:
            try:
                producen = summary_0[x]['producen']
            except:
                producen = 0
            try:
                autoconsumo = summary_1[x]['autoconsumo']
            except:
                autoconsumo = 0
            try:
                venta = summary_2[x]['venta']
            except:
                venta = 0
            try:
                ambos = summary_3[x]['ambos']
            except:
                ambos = 0
            otro_dict[year][summary_0[x]['rubrosfinca__rubros__nombre']] = [
                                                                                                        producen,
                                                                                                       filtro.filter(year=year).count() - producen,
                                                                                                        autoconsumo,
                                                                                                        venta,
                                                                                                        ambos,
                                                                                                        ]
    #print otro_dict

    #tabla_rubros= OrderedDict()
    # for obj in Rubros.objects.all():
    #     producen_finca_si = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__producen_finca=1).count()
    #     producen_finca_no = filtro.count() - producen_finca_si
    #     produccion_autoconsumo = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=1).count()
    #     produccion_venta = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=2).count()
    #     produccion_ambos = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=3).count()

    #     tabla_rubros[obj] = (producen_finca_si,producen_finca_no,produccion_autoconsumo,
    #                                         produccion_venta,produccion_ambos)

    return render(request, template, {'tabla_rubros':otro_dict,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def areas_factores(request, template='herramientaDesicion/areas.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    tabla_areas = OrderedDict()
    tabla_factor_socioeconomico = OrderedDict()
    tabla_factor_biofisico = OrderedDict()
    num_familias2 = {}
    for year2 in years:
        num_familias2[year2] = filtro.filter(year=year2).count()

        tabla_areas[year2] = OrderedDict()
        for year in CHOICES_ANIOS:
            area_total = AreasCacao.objects.filter(encuesta__in=filtro.filter(year=year2),
                                                                              anio=year[0]).aggregate(t=Sum('area_total_ha'))['t']
            if area_total > 0:
                desarrollo = AreasCacao.objects.filter(encuesta__in=filtro.filter(year=year2),
                                                                                  anio=year[0]).aggregate(t=Sum('area_desarrollo_ha'))['t']
                produccion = AreasCacao.objects.filter(encuesta__in=filtro.filter(year=year2),
                                                                                  anio=year[0]).aggregate(t=Sum('area_produccion_ha'))['t']
                produccion_total = ProduccionCacao.objects.filter(encuesta__in=filtro.filter(year=year2),
                                                                                  anio=year[0]).aggregate(t=Sum('produccion_total_qq'))['t']
                try:
                    productividad = produccion_total / float(produccion)
                except:
                    productividad = 0
                tabla_areas[year2][year[1]] = (area_total,desarrollo,produccion,produccion_total, productividad)

        tabla_factor_socioeconomico[year2] = OrderedDict()
        for obj in CHOICE_LIMITANTE_PRODUCCION:
            contar = filtro.filter(year=year2,factores__factor_socioeconomico__contains=obj[0]).count()
            try:
                porcentaje = (float(contar) / float(filtro.filter(year=year2).count())) * 100
            except:
                porcentaje = 0
            tabla_factor_socioeconomico[year2][obj[1]] = (contar,porcentaje)

        tabla_factor_biofisico[year2] = OrderedDict()
        for obj in CHOICES_LIMITANTE_BIOFISICO:
            contar = filtro.filter(year=year2,factores__factor_biofisico__contains=obj[0]).count()
            try:
                porcentaje = (float(contar) / float(filtro.filter(year=year2).count())) * 100
            except:
                porcentaje = 0
            tabla_factor_biofisico[year2][obj[1]] = (contar,porcentaje)

    return render(request, template, {'tabla_areas':tabla_areas,
                                                                'tabla_factor_socioeconomico':tabla_factor_socioeconomico,
                                                                'tabla_factor_biofisico':tabla_factor_biofisico,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def produccion(request, template='herramientaDesicion/produccion.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    promedio_plantas = filtro.filter(laproduccion__cuantas__gt=0.1).aggregate(p=Avg('laproduccion__cuantas'))['p']
    num_familias2 = {}
    tabla_tiene_vivero = OrderedDict()
    promedio_plantas2 = OrderedDict()
    tabla_variedad = OrderedDict()
    tabla_semilla = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_tiene_vivero[year] = OrderedDict()
        for obj in CHOICE_SI_NO:
            contar = filtro.filter(year=year,laproduccion__tiene_vivero=obj[0]).count()
            #porcentaje = (float(contar) / float(filtro.count())) * 100
            tabla_tiene_vivero[year][obj[1]] = contar

        promedio_plantas2[year] = filtro.filter(year=year,laproduccion__cuantas__gt=0.1).aggregate(p=Avg('laproduccion__cuantas'))['p']

        tabla_variedad[year] = OrderedDict()
        for obj in CHOICES_VARIEDADES:
            contar = filtro.filter(year=year,laproduccion__variedad__contains=obj[0]).count()
            #porcentaje = (float(contar) / float(filtro.count())) * 100
            tabla_variedad[year][obj[1]] = contar

        tabla_semilla[year] = OrderedDict()
        for obj in CHOICES_CONSIGUEN_SEMILLA:
            contar = filtro.filter(year=year,laproduccion__consiguen__contains=obj[0]).count()
            #porcentaje = (float(contar) / float(filtro.count())) * 100
            tabla_semilla[year][obj[1]] = contar

    return render(request, template, {'tabla_tiene_vivero':tabla_tiene_vivero,
                                                                'promedio_plantas':promedio_plantas,
                                                                'tabla_variedad':tabla_variedad,
                                                                'tabla_semilla':tabla_semilla,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2,
                                                                'promedio_plantas2':promedio_plantas2})

def manejo_poda(request, template='herramientaDesicion/manejoPoda.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year] = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__poda=obj[0]).count()
            calidad_poda[year][obj[1]] = conteo

        calidad_nivel[year] = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__poda=obj[0]).count()
            calidad_nivel[year][obj[1]] = conteo

        calidad_mes[year] = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__poda__contains=obj[0]).count()
            calidad_mes[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year] = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__poda__contains=obj[0]).count()
            calidad_realiza[year][obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2': num_familias2})

def manejo_sombra(request, template='herramientaDesicion/manejoSombra.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year] = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__sombra=obj[0]).count()
            calidad_poda[year][obj[1]] = conteo

        calidad_nivel[year] = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__sombra=obj[0]).count()
            calidad_nivel[year][obj[1]] = conteo

        calidad_mes[year] = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__sombra__contains=obj[0]).count()
            calidad_mes[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year] = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__sombra__contains=obj[0]).count()
            calidad_realiza[year][obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2': num_familias2})

def manejo_maleza(request, template='herramientaDesicion/manejoMaleza.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year] = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__maleza=obj[0]).count()
            calidad_poda[year][obj[1]] = conteo

        calidad_nivel[year] = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__maleza=obj[0]).count()
            calidad_nivel[year][obj[1]] = conteo

        calidad_mes[year] = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__maleza__contains=obj[0]).count()
            calidad_mes[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year] = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__maleza__contains=obj[0]).count()
            calidad_realiza[year][obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def manejo_fertilidad(request, template='herramientaDesicion/manejoFertilidad.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year] = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__fertilidad=obj[0]).count()
            calidad_poda[year][obj[1]] = conteo

        calidad_nivel[year] = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__fertilidad=obj[0]).count()
            calidad_nivel[year][obj[1]] = conteo

        calidad_mes[year] = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__fertilidad__contains=obj[0]).count()
            calidad_mes[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year] = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__fertilidad__contains=obj[0]).count()
            calidad_realiza[year][obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def manejo_hongos(request, template='herramientaDesicion/manejoHongos.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda  = OrderedDict()
    calidad_nivel  = OrderedDict()
    calidad_mes  = OrderedDict()
    calidad_realiza  = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year]  = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__enfermedad=obj[0]).count()
            calidad_poda[year] [obj[1]] = conteo

        calidad_nivel[year]  = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__enfermedad=obj[0]).count()
            calidad_nivel[year] [obj[1]] = conteo

        calidad_mes[year]  = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__enfermedad__contains=obj[0]).count()
            calidad_mes[year] [obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year]  = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__enfermedad__contains=obj[0]).count()
            calidad_realiza[year] [obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def manejo_insectos(request, template='herramientaDesicion/manejoInsectos.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year]  = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__plagas_insectos=obj[0]).count()
            calidad_poda[year] [obj[1]] = conteo

        calidad_nivel[year]  = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__plagas_insectos=obj[0]).count()
            calidad_nivel[year] [obj[1]] = conteo

        calidad_mes[year]  = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__plagas_insectos__contains=obj[0]).count()
            calidad_mes[year] [obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year]  = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__plagas_insectos__contains=obj[0]).count()
            calidad_realiza[year] [obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def manejo_nematodos(request, template='herramientaDesicion/manejoNematodos.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    #TODO: falta programar este cuadro el 3.4
    num_familias2 = {}
    calidad_poda = OrderedDict()
    calidad_nivel = OrderedDict()
    calidad_mes = OrderedDict()
    calidad_realiza = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        calidad_poda[year] = OrderedDict()
        for obj in CHOICES_CALIDAD:
            conteo = filtro.filter(year=year,calidadmanejo__plagas_nematodos=obj[0]).count()
            calidad_poda[year][obj[1]] = conteo

        calidad_nivel[year] = OrderedDict()
        for obj in CHOICES_USO_MANEJO:
            conteo = filtro.filter(year=year,nivelmanejo__plagas_nematodos=obj[0]).count()
            calidad_nivel[year][obj[1]] = conteo

        calidad_mes[year] = OrderedDict()
        for obj in CHOICE_MESES:
            conteo = filtro.filter(year=year,meslabores__plagas_nematodos__contains=obj[0]).count()
            calidad_mes[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        calidad_realiza[year] = OrderedDict()
        for obj in CHOICES_REALIZAN_LABOR:
            conteo = filtro.filter(year=year,realizanlabores__plagas_nematodos__contains=obj[0]).count()
            calidad_realiza[year][obj[1]] = (conteo,filtro.filter(year=year).count())

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})


def opcion_agroecologica(request, template='herramientaDesicion/opcion_agro.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_opcion_agro = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_opcion_agro[year] = OrderedDict()
        for obj in CHOICES_OPCIONES_AGRO:
            query = filtro.filter(year=year,opcionesagroecologicas__opciones=obj[0])
            no_utiliza = query.filter(year=year,opcionesagroecologicas__opciones=obj[0],
                                opcionesagroecologicas__nivel=1).count()
            pequena_escala = query.filter(year=year,opcionesagroecologicas__opciones=obj[0],
                                opcionesagroecologicas__nivel=2).count()
            mayor_escala = query.filter(year=year,opcionesagroecologicas__opciones=obj[0],
                                opcionesagroecologicas__nivel=3).count()
            toda_finca = query.filter(year=year,opcionesagroecologicas__opciones=obj[0],
                                opcionesagroecologicas__nivel=4).count()
            try:
                por_mayor_escala = mayor_escala / float(filtro.filter(year=year).count()) * 100
            except:
                por_mayor_escala = 0
            try:
                por_toda_finca = toda_finca / float(filtro.filter(year=year).count()) * 100
            except:
                por_toda_finca = 0
            suma_total_porcentaje = por_mayor_escala + por_toda_finca
            tabla_opcion_agro[year][obj[1]] = (no_utiliza,pequena_escala,mayor_escala,toda_finca, suma_total_porcentaje)

    return render(request, template, {'tabla_opcion_agro':tabla_opcion_agro,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def cosecha(request, template='herramientaDesicion/cosecha.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_cortes = OrderedDict()
    tabla_mazorca = OrderedDict()
    tabla_quiebran = OrderedDict()
    tabla_calidad = OrderedDict()
    tabla_precio = OrderedDict()
    promedio_precio = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_cortes[year]  = OrderedDict()
        for obj in CHOICES_CORTES:
            valor = filtro.filter(year=year,cosecha__cortes=obj[0]).count()
            tabla_cortes[year] [obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_mazorca[year]  = OrderedDict()
        for obj in CHOICES_SEPARAN_MAZORCA:
            valor = filtro.filter(year=year,cosecha__separan_mazorca=obj[0]).count()
            tabla_mazorca[year] [obj[1]] = valor

        tabla_quiebran[year]  = OrderedDict()
        for obj in CHOICES_QUIEBRAN_MAZORCA:
            valor = filtro.filter(year=year,cosecha__quiebran_mazorca=obj[0]).count()
            tabla_quiebran[year] [obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_calidad[year]  = OrderedDict()
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,cosecha__calidad=obj[0]).count()
            tabla_calidad[year] [obj[1]] = valor

        tabla_precio[year]  = OrderedDict()
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,cosecha__precio=obj[0]).count()
            tabla_precio[year] [obj[1]] = valor

        promedio_precio[year] = Cosecha.objects.filter(encuesta__in=filtro.filter(year=year)).aggregate(p=Avg('cuanto'))['p']


    return render(request, template, {'tabla_cortes':tabla_cortes,
                                                                'tabla_mazorca':tabla_mazorca,
                                                                'tabla_quiebran':tabla_quiebran,
                                                                'tabla_calidad':tabla_calidad,
                                                                'tabla_precio':tabla_precio,
                                                                'promedio_precio':promedio_precio,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def comercializacion(request, template='herramientaDesicion/comercializacion.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    familias_producen = {}
    data = {}
    tabla_cadena = OrderedDict()
    tabla_comercializacion = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        familias_producen[year] = filtro.filter(year=year,comercializacion__opcion=1, comercializacion__cantidad_total__gt=0).count()

        tabla_cadena[year] = OrderedDict()
        for obj in CHOICES_OPCION_CADENA:
            conteo = filtro.filter(year=year,opcionescadena__opcion__icontains=obj[0]).count()
            tabla_cadena[year][obj[1]] = (conteo,filtro.filter(year=year).count())

        tabla_comercializacion[year] = {}
        for obj in CHOICES_COMERCIALIZACION:
            familias =  filtro.filter(year=year,comercializacion__opcion=obj[0], comercializacion__cantidad_total__gt=0).count()
            cantidad_total = filtro.filter(year=year,comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__cantidad_total'))['t']
            intermediario = filtro.filter(year=year,comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__intermediario'))['t']
            inter_precio = filtro.filter(year=year,comercializacion__opcion=obj[0]).aggregate(t=Avg('comercializacion__intermediario_precio_usd'))['t']
            coop_cantidad = filtro.filter(year=year,comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__coop_cantidad'))['t']
            coop_precio = filtro.filter(year=year,comercializacion__opcion=obj[0]).aggregate(t=Avg('comercializacion__coop_precio_usd'))['t']
            if cantidad_total > 0:
                formula1 = (intermediario * inter_precio) + (coop_cantidad * coop_precio)
                tabla_comercializacion[year][obj[1]] = (familias,cantidad_total,intermediario,
                                                                        inter_precio,coop_cantidad,coop_precio,
                                                                        formula1)

        ingreso_total = sum([v[6] for a,b in tabla_comercializacion.items() for k,v in b.items()]) or 0

        try:
            promedio_ingreso_bruto = ingreso_total / float(filtro.filter(year=year).count())
        except:
            promedio_ingreso_bruto = 0

        area_total_cacao_produccion = AreasCacao.objects.filter(encuesta__in=filtro.filter(year=year), anio=5).aggregate(t=Sum('area_produccion_ha'))['t']

        try:
            promedio_ingreso_anual = ingreso_total / area_total_cacao_produccion
        except:
            promedio_ingreso_anual = 0

        promedio_ingreso_neto = promedio_ingreso_anual * 0.4

        data[year] = [ingreso_total,area_total_cacao_produccion,promedio_ingreso_bruto,
                               promedio_ingreso_anual,promedio_ingreso_neto]


    return render(request, template, {'tabla_comercializacion':tabla_comercializacion,
                                                                'tabla_cadena':tabla_cadena,
                                                                'data':data,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'familias_producen':familias_producen,
                                                                'num_familias2':num_familias2})

def credito(request, template='herramientaDesicion/credito.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_credito = {}
    tabla_facilidad = {}
    tabla_obtiene_credito = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_credito[year]  = {}
        for obj in CHOICES_CREDITO:
            monto_credito_corto = filtro.filter(year=year,credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_corto_usd'))['t']
            monto_credito_mediano = filtro.filter(year=year,credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_mediano_usd'))['t']
            monto_credito_largo = filtro.filter(year=year,credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_largo_usd'))['t']
            tabla_credito[year] [obj[1]] = (monto_credito_corto,monto_credito_mediano,monto_credito_largo)

        tabla_facilidad[year]  = {}
        for obj in CHOICES_FACILIDAD:
            valor = filtro.filter(year=year,credito__facilidad=obj[0]).count()
            tabla_facilidad[year] [obj[1]] = valor

        tabla_obtiene_credito[year]  = {}
        for obj in CHOICES_OBTIENE_CREDITO:
            valor = filtro.filter(year=year,obtienecredito__obtiene_credito__contains=obj[0]).count()
            tabla_obtiene_credito[year] [obj[1]] = (valor,filtro.filter(year=year).count())


    return render(request, template, {'tabla_credito':tabla_credito,
                                                                'tabla_facilidad':tabla_facilidad,
                                                                'tabla_obtiene_credito':tabla_obtiene_credito,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def mitigacion(request, template='herramientaDesicion/mitigacion.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 ={}
    tabla_monitorio_plaga = {}
    tabla_monitorio_cada_cuanto = {}
    tabla_monitorio_realiza = {}
    tabla_lleva_registro = {}
    tabla_suficiente_recurso = {}
    tabla_falta_recurso = {}
    tabla_obra_almacenamiento = {}
    tabla_venta_organizada = {}
    tabla_contrato_venta = {}
    tabla_certificado = {}
    tabla_tipo_certificado = {}
    tabla_calidad_cacao = {}
    tabla_plan_manejo = {}
    tabla_plan_negocio = {}
    tabla_plan_inversion = {}
    tabla_apoya_planes = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_monitorio_plaga[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__monitoreo_plaga=obj[0]).count()
            tabla_monitorio_plaga[year] [obj[1]] = valor

        tabla_monitorio_cada_cuanto[year]  = {}
        for obj in CHOICES_CADA_CUANTO:
            valor = filtro.filter(year=year,mitigacionriesgo__cada_cuanto=obj[0]).count()
            tabla_monitorio_cada_cuanto[year] [obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_monitorio_realiza[year]  = {}
        for obj in CHOICES_COMO_REALIZA:
            valor = filtro.filter(year=year,mitigacionriesgo__como_realiza=obj[0]).count()
            tabla_monitorio_realiza[year] [obj[1]] = valor

        tabla_lleva_registro[year]  = {}
        for obj in CHOICES_LLEVA_REGISTROS:
            valor = filtro.filter(year=year,mitigacionriesgo__lleva_registro=obj[0]).count()
            tabla_lleva_registro[year] [obj[1]] = valor

        tabla_suficiente_recurso[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__cuenta_recursos=obj[0]).count()
            tabla_suficiente_recurso[year] [obj[1]] = valor

        tabla_falta_recurso[year]  = {}
        for obj in CHOICES_FALTA_RECURSO:
            valor = filtro.filter(year=year,mitigacionriesgo__falta_recurso__contains=obj[0]).count()
            tabla_falta_recurso[year] [obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_obra_almacenamiento[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__obras_almacenamiento=obj[0]).count()
            tabla_obra_almacenamiento[year] [obj[1]] = valor

        tabla_venta_organizada[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__venta_organizada=obj[0]).count()
            tabla_venta_organizada[year] [obj[1]] = valor

        tabla_contrato_venta[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__contrato_venta=obj[0]).count()
            tabla_contrato_venta[year] [obj[1]] = valor

        tabla_certificado[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__certificado=obj[0]).count()
            tabla_certificado[year] [obj[1]] = valor

        tabla_tipo_certificado[year]  = {}
        for obj in CHOICES_TIPO_CERTIFICADO:
            valor = filtro.filter(year=year,mitigacionriesgo__tipo_certificado__contains=obj[0]).count()
            tabla_tipo_certificado[year] [obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_calidad_cacao[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__calidad_cacao=obj[0]).count()
            tabla_calidad_cacao[year] [obj[1]] = valor

        tabla_plan_manejo[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__plan_manejo=obj[0]).count()
            tabla_plan_manejo[year] [obj[1]] = valor

        tabla_plan_negocio[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__plan_negocio=obj[0]).count()
            tabla_plan_negocio[year] [obj[1]] = valor

        tabla_plan_inversion[year]  = {}
        for obj in CHOICE_SI_NO:
            valor = filtro.filter(year=year,mitigacionriesgo__plan_inversion=obj[0]).count()
            tabla_plan_inversion[year] [obj[1]] = valor

        tabla_apoya_planes[year]  = {}
        for obj in CHOICES_APOYA_PLANES:
            valor = filtro.filter(year=year,mitigacionriesgo__apoya_planes__contains=obj[0]).count()
            tabla_apoya_planes[year] [obj[1]] = (valor,filtro.filter(year=year).count())


    return render(request, template, {'tabla_monitorio_plaga':tabla_monitorio_plaga,
                                                                'tabla_monitorio_cada_cuanto':tabla_monitorio_cada_cuanto,
                                                                'tabla_monitorio_realiza':tabla_monitorio_realiza,
                                                                'tabla_lleva_registro':tabla_lleva_registro,
                                                                'tabla_suficiente_recurso':tabla_suficiente_recurso,
                                                                'tabla_falta_recurso':tabla_falta_recurso,
                                                                'tabla_obra_almacenamiento':tabla_obra_almacenamiento,
                                                                'tabla_venta_organizada':tabla_venta_organizada,
                                                                'tabla_contrato_venta':tabla_contrato_venta,
                                                                'tabla_certificado':tabla_certificado,
                                                                'tabla_tipo_certificado':tabla_tipo_certificado,
                                                                'tabla_calidad_cacao':tabla_calidad_cacao,
                                                                'tabla_plan_manejo':tabla_plan_manejo,
                                                                'tabla_plan_negocio':tabla_plan_negocio,
                                                                'tabla_plan_inversion':tabla_plan_inversion,
                                                                'tabla_apoya_planes':tabla_apoya_planes,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def capasitacion_tecnica(request, template='herramientaDesicion/capasitaciones.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_tecnica = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_tecnica[year] = OrderedDict()
        for obj in CHOICES_CAPACITACION_TECNICA:
            total_veces =  filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0]).aggregate(t=Sum('capacitaciontecnicas__cuantas_veces'))['t']
            pobre = filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=1).count()
            algo = filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=2).count()
            regular = filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=3).count()
            bueno = filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=4).count()
            excelente = filtro.filter(year=year,capacitaciontecnicas__opcion=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=5).count()

            tabla_tecnica[year][obj[1]] = (total_veces,pobre,algo,regular,bueno,excelente)

    return render(request, template, {'tabla_tecnica':tabla_tecnica,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def capasitacion_social(request, template='herramientaDesicion/capasitacion_social.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_social = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_social[year] = OrderedDict()
        for obj in CHOICES_TEMAS_SOCIAL:
            total_veces =  filtro.filter(year=year,capacitacionsocial__tema=obj[0]).aggregate(t=Sum('capacitacionsocial__cuantas_veces'))['t']
            pobre = filtro.filter(year=year,capacitacionsocial__tema=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=1).count()
            algo = filtro.filter(year=year,capacitacionsocial__tema=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=2).count()
            regular = filtro.filter(year=year,capacitacionsocial__tema=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=3).count()
            bueno = filtro.filter(year=year,capacitacionsocial__tema=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=4).count()
            excelente = filtro.filter(year=year,capacitacionsocial__tema=obj[0],
                                             capacitaciontecnicas__nivel_consideracion=5).count()

            tabla_social[year][obj[1]] = (total_veces,pobre,algo,regular,bueno,excelente)

    return render(request, template, {'tabla_social':tabla_social,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})


def detalles_conteo_parcela(request, template='herramientaDesicion/detallesConteoParcela.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']


    num_familias2 = {}
    total_plantios = {}
    #conteos_plantios = {}
    promedio = {}
    mediana = {}
    minimo = {}
    maximo = {}
    grafo_parcelas = OrderedDict()
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()
        #numero de plantios
        total_plantios[year] = filtro.filter(year=year).aggregate(t=Count('detalleplantios__nombre_plantio'))['t']

        conteos_plantios = []
        for obj in filtro.filter(year=year):
            conteo = obj.detalleplantios_set.all().count()
            conteos_plantios.append(conteo)
        # media arítmetica
        promedio[year] = np.mean(conteos_plantios)
        # mediana
        mediana[year] = np.median(conteos_plantios)
        #minimo y maximo
        minimo[year] = min(conteos_plantios)
        maximo[year] = max(conteos_plantios)

        lista_reducida = list(set(conteos_plantios))
        grafo_parcelas[year] = OrderedDict()
        for obj in lista_reducida:
            conteo = contar_veces(obj, conteos_plantios)
            grafo_parcelas[year][obj] = conteo

    return render(request, template, {'grafo_parcelas':grafo_parcelas,
                                                                'numero_plantio':total_plantios,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2': num_familias2})


def detalles_area_parcela(request, template='herramientaDesicion/detallesAreaParcela.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    grafo_area_plantios = {}
    data = {}
    total_area_finca = 0
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        total_area_finca= UsoTierra.objects.filter(encuesta__in=filtro.filter(year=year)).exclude(tierra=1).aggregate(t=Sum('area_ha'))['t']
        lista_area_finca = UsoTierra.objects.filter(encuesta__in=filtro.filter(year=year), tierra=1).values_list('area_ha', flat=True)

        # media arítmetica
        promedio = np.mean(lista_area_finca)
        # mediana
        mediana = np.median(lista_area_finca)
        #minimo y maximo
        try:
            minimo = min(lista_area_finca)
            maximo = max(lista_area_finca)
        except:
            minimo = 0
            maximo = 0

        data[year] = [total_area_finca,promedio,mediana,minimo,maximo]

        grafo_area_plantios[year] = crear_rangos(request, lista_area_finca, minimo, maximo, step=30)

    return render(request, template, {'numero_plantio':total_area_finca,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'grafo_area_plantios':grafo_area_plantios,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2,
                                                                'data':data})

def detalles_area_cacao(request, template='herramientaDesicion/detallesAreaCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    grafo_area_plantios = {}
    grafo_area_plantios_D = {}
    grafo_area_plantios_P = {}
    grafo_area_plantios_M = {}
    data_total = {}
    data_desarrollo = {}
    data_productiva = {}
    data_mezcla = {}
    total_areas_plantios = 0
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        total_areas_plantios = filtro.filter(year=year).aggregate(t=Sum('detalleplantios__area_ha'))['t']
        conteos_areas_plantios = []
        for obj in filtro.filter(year=year):
            suma = 0
            for i in obj.detalleplantios_set.all():
                suma += i.area_ha
            conteos_areas_plantios.append(suma)
        # media arítmetica
        promedio = np.mean(conteos_areas_plantios)
        # mediana
        mediana = np.median(conteos_areas_plantios)
        #minimo y maximo
        minimo = min(conteos_areas_plantios)
        maximo = max(conteos_areas_plantios)

        data_total[year] = [total_areas_plantios,promedio,mediana,minimo,maximo]
        grafo_area_plantios[year] = crear_rangos(request, conteos_areas_plantios, minimo, maximo, step=2)

        #   lo mismo pero para desarrollo de platas en cacao
        total_areas_plantios_D = filtro.filter(year=year,detalleplantios__tipo_planta=1).aggregate(t=Sum('detalleplantios__area'))['t']
        conteos_areas_plantios_D = []
        for obj in filtro.filter(year=year,detalleplantios__tipo_planta=1):
            suma_d = 0
            for i in obj.detalleplantios_set.filter(tipo_planta=1):
                #if i.tipo_planta == 1:
                suma_d += i.area_ha
            conteos_areas_plantios_D.append(suma_d)

        # media arítmetica
        promedio_D = np.mean(conteos_areas_plantios_D)
        # mediana
        mediana_D = np.median(conteos_areas_plantios_D)
        #minimo y maximo
        try:
            minimo_D = min(conteos_areas_plantios_D)
        except:
            minimo_D = 0
        try:
            maximo_D = max(conteos_areas_plantios_D)
        except:
            maximo_D = 0

        data_desarrollo[year] = [total_areas_plantios_D,promedio_D,mediana_D,minimo_D,maximo_D]
        grafo_area_plantios_D[year] = crear_rangos(request, conteos_areas_plantios_D, minimo_D, maximo_D, step=3)

        # para productiva
        total_areas_plantios_P = filtro.filter(year=year,detalleplantios__tipo_planta=2).aggregate(t=Sum('detalleplantios__area'))['t']
        conteos_areas_plantios_P = []
        for obj in filtro.filter(year=year,detalleplantios__tipo_planta=2):
            suma_p = 0
            for i in obj.detalleplantios_set.filter(tipo_planta=2):
                #if i.tipo_planta == 2:
                suma_p += i.area_ha
            conteos_areas_plantios_P.append(suma_p)
        # media arítmetica
        promedio_P = np.mean(conteos_areas_plantios_P)
        # mediana
        mediana_P = np.median(conteos_areas_plantios_P)
        #minimo y maximo
        try:
            minimo_P = min(conteos_areas_plantios_P)
            maximo_P = max(conteos_areas_plantios_P)
        except:
            minimo_P = 0
            maximo_P = 0

        data_productiva[year] = [total_areas_plantios_P,promedio_P,mediana_P,minimo_P,maximo_P]
        grafo_area_plantios_P[year] = crear_rangos(request, conteos_areas_plantios_P, minimo_P, maximo_P, step=3)

        #para mezcla
        total_areas_plantios_M = filtro.filter(year=year,detalleplantios__tipo_planta=3).aggregate(t=Sum('detalleplantios__area'))['t']

        conteos_areas_plantios_M = []
        for obj in filtro.filter(year=year,detalleplantios__tipo_planta=3):
            suma_m = 0
            for i in obj.detalleplantios_set.filter(tipo_planta=3):
                #if i.tipo_planta == 3:
                suma_m += i.area_ha
            conteos_areas_plantios_M.append(suma_m)
        # media arítmetica
        promedio_M = np.mean(conteos_areas_plantios_M)
        # mediana
        mediana_M = np.median(conteos_areas_plantios_M)
        #minimo y maximo
        try:
            minimo_M = min(conteos_areas_plantios_M)
            maximo_M = max(conteos_areas_plantios_M)
        except:
            minimo_M = 0
            maximo_M = 0

        data_mezcla[year] = [total_areas_plantios_M,promedio_M,mediana_M,minimo_M,maximo_M]
        grafo_area_plantios_M[year] = crear_rangos(request, conteos_areas_plantios_M, minimo_M, maximo_M, step=3)

    return render(request, template, {'numero_plantio':total_areas_plantios,
                                                               'data_total':data_total,
                                                                'grafo_area_plantios':grafo_area_plantios,
                                                                #desarrollo
                                                                'numero_plantio_d':total_areas_plantios_D,
                                                                'data_desarrollo':data_desarrollo,
                                                                'grafo_area_plantios_d':grafo_area_plantios_D,
                                                                #productivo
                                                                'numero_plantio_p':total_areas_plantios_P,
                                                                'data_productiva':data_productiva,
                                                                'grafo_area_plantios_p':grafo_area_plantios_P,
                                                                #mezcla
                                                                 'numero_plantio_m':total_areas_plantios_M,
                                                                'data_mezcla':data_mezcla,
                                                                'grafo_area_plantios_m':grafo_area_plantios_M,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_produccion_cacao(request, template='herramientaDesicion/detallesProduccionCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    data = {}
    grafo_produccion_plantios = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        total_produccion_plantios = filtro.filter(year=year).aggregate(t=Sum('detalleplantios__produccion_kg'))['t']
        conteos_produccion_plantios = []
        for obj in filtro.filter(year=year,detalleplantios__tipo_planta__in=[2,3]):
            suma = 0
            for i in obj.detalleplantios_set.filter(tipo_planta__in=[2,3]):
                try:
                    suma += i.produccion_kg
                except:
                    suma = 0
            conteos_produccion_plantios.append(suma)
        # media arítmetica
        promedio = np.mean(conteos_produccion_plantios)
        # mediana
        mediana = np.median(conteos_produccion_plantios)
        #minimo y maximo
        try:
            minimo = min(conteos_produccion_plantios)
            maximo = max(conteos_produccion_plantios)
        except:
            minimo = 0
            maximo = 0

        data[year] = [total_produccion_plantios,promedio,mediana,minimo,maximo]
        grafo_produccion_plantios[year] = crear_rangos(request, conteos_produccion_plantios, minimo, maximo, step=500)


    return render(request, template, {'numero_plantio':total_produccion_plantios,
                                                                'data':data,
                                                                'grafo_produccion_plantios':grafo_produccion_plantios,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_productividad_cacao(request, template='herramientaDesicion/detallesProductividadCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    data = {}
    grafo_produccion_plantios = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        total_produccion_plantios = filtro.filter(year=year).aggregate(t=Sum('detalleplantios__rendimientos'))['t']
        conteos_produccion_plantios = []
        for obj in filtro.filter(year=year,detalleplantios__tipo_planta__in=[2,3]):
            suma = 0
            for i in obj.detalleplantios_set.filter(tipo_planta__in=[2,3]):
                try:
                    suma += i.rendimientos
                except:
                    suma = 0
            conteos_produccion_plantios.append(suma)
        # media arítmetica
        promedio = np.mean(conteos_produccion_plantios)
        # mediana
        mediana = np.median(conteos_produccion_plantios)
        #minimo y maximo
        minimo = min(conteos_produccion_plantios)
        maximo = max(conteos_produccion_plantios)

        data[year] = [total_produccion_plantios,promedio,mediana,minimo,maximo]
        grafo_produccion_plantios[year] = crear_rangos(request, conteos_produccion_plantios, minimo, maximo, step=500)


    return render(request, template, {'numero_plantio':total_produccion_plantios,
                                                                'data': data,
                                                                'grafo_produccion_plantios':grafo_produccion_plantios,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_edad_cacao(request, template='herramientaDesicion/detallesEdadCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    grafo_edad_plantios = {}
    grafo_edad_plantios_M = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        conteos_edad_plantios = []
        for obj in filtro.filter(year=year):
            for i in obj.detalleplantios_set.all():
                if i.tipo_planta == 2:
                    conteos_edad_plantios.append(i.edad)

        #minimo y maximo
        try:
            minimo = min(conteos_edad_plantios)
        except:
            minimo = 0
        try:
            maximo = max(conteos_edad_plantios)
        except:
            maximo = 0

        grafo_edad_plantios[year] = crear_rangos(request, conteos_edad_plantios, minimo, maximo, step=5)

        conteos_edad_plantios_M = []
        for obj in filtro.filter(year=year):
            for i in obj.detalleplantios_set.all():
                if i.tipo_planta == 3:
                    conteos_edad_plantios_M.append(i.edad)

        #minimo y maximo
        try:
            minimo_M = min(conteos_edad_plantios_M)
        except:
            minimo_M = 0
        try:
            maximo_M = max(conteos_edad_plantios_M)
        except:
            maximo_M = 0

        grafo_edad_plantios_M[year] = crear_rangos(request, conteos_edad_plantios_M, minimo_M, maximo_M, step=5)

    return render(request, template, {'grafo_edad_plantios':grafo_edad_plantios,
                                                                'grafo_edad_plantios_m': grafo_edad_plantios_M,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_variedad_cacao(request, template='herramientaDesicion/detallesVariedadCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    tabla_variedad = {}
    tabla_sombra = {}
    tabla_fertilizacion = {}
    tabla_fungicida = {}
    grafo_monilia = {}
    grafo_sombra = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        tabla_variedad[year] = {}
        for obj in CHOICES_VARIEDAD_PREDOMINANTE:
            valor = filtro.filter(year=year,detalleplantios__variedades__contains=obj[0]).count()
            tabla_variedad[year][obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_sombra[year]  = {}
        for obj in CHOICE_TIPO_SOMBRA:
            valor = filtro.filter(year=year,detalleplantios__tipo_sombra=obj[0]).count()
            tabla_sombra[year][obj[1]] = (valor,filtro.filter(year=year).count())

        tabla_fertilizacion[year] = {}
        for obj in CHOICE_TIPO_FERTILIZACION:
            valor = filtro.filter(year=year,detalleplantios__tipo_fertilizacion=obj[0]).count()
            tabla_fertilizacion[year][obj[1]] = valor

        tabla_fungicida[year] = {}
        for obj in CHOICE_TIPO_FUNGICIDA:
            valor = filtro.filter(year=year,detalleplantios__tipo_fungicida=obj[0]).count()
            tabla_fungicida[year][obj[1]] = valor

        try:
            lista_monilia = []
            for obj in filtro.filter(year=year):
                for i in obj.detalleplantios_set.all():
                    lista_monilia.append(i.nivel)
            grafo_monilia[year] = crear_rangos(request, lista_monilia, min(lista_monilia), max(lista_monilia), step=10)
        except:
            grafo_monilia[year] = []

        try:
            lista_nivel_sombra = []
            for obj in filtro.filter(year=year):
                for i in obj.detalleplantios_set.all():
                    lista_nivel_sombra.append(i.nivel_sombra)
            grafo_sombra[year] = crear_rangos(request, lista_nivel_sombra, min(lista_nivel_sombra), max(lista_nivel_sombra), step=10)
        except:
            grafo_sombra[year] = []

    return render(request, template, {'tabla_variedad':tabla_variedad,
                                                                'tabla_sombra':tabla_sombra,
                                                                'tabla_fertilizacion':tabla_fertilizacion,
                                                                'tabla_fungicida':tabla_fungicida,
                                                                'grafo_monilia': grafo_monilia,
                                                                'grafo_sombra':grafo_sombra,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_dispercion_cacao(request, template='herramientaDesicion/detallesDispercionCacao.html'):
    filtro = _queryset_filtrado(request)
    years = request.session['fecha']

    num_familias2 = {}
    dispercion_fertilidad = OrderedDict()
    dispercion_fertilidad_lista = OrderedDict()
    dispercion_fungicida = OrderedDict()
    dispercion_fungicida_lista = OrderedDict()
    data = {}
    for year in years:
        num_familias2[year] = filtro.filter(year=year).count()

        dispersion_prod_area = []
        dispersion_prod_monilia = []
        dispersion_prod_sombra = []
        dispersion_prod_edad = []
        for obj in filtro.filter(year=year):
            for i in obj.detalleplantios_set.filter(tipo_planta__in=[2,3]):
                dispersion_prod_area.append([i.area_ha,i.rendimientos])
                dispersion_prod_monilia.append([i.nivel,i.rendimientos])
                dispersion_prod_sombra.append([i.nivel_sombra,i.rendimientos])
                dispersion_prod_edad.append([i.edad,i.rendimientos])

        data[year] = [dispersion_prod_area,dispersion_prod_monilia,dispersion_prod_sombra,dispersion_prod_edad]
        try:
            dispercion_fertilidad[year] = OrderedDict()
            for obj in CHOICE_TIPO_FERTILIZACION:
                rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro.filter(year=year), tipo_fertilizacion=obj[0]).aggregate(t=Avg('rendimientos'))['t']
                if rendimiento > 0:
                    dispercion_fertilidad[year][obj[1]] = rendimiento
        except:
            pass

        try:
            dispercion_fertilidad_lista[year] = OrderedDict()
            for obj in CHOICE_TIPO_FERTILIZACION:
                rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro.filter(year=year), tipo_planta__in=[2,3], tipo_fertilizacion=obj[0]).values_list('area_ha','rendimientos')
                if len(rendimiento) > 0:
                    a = [ [x[0],x[1]] for x in rendimiento]
                    dispercion_fertilidad_lista[year][obj[1]] = a
        except:
            pass

        try:
            dispercion_fungicida[year] = OrderedDict()
            for obj in CHOICE_TIPO_FUNGICIDA:
                rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro.filter(year=year), tipo_fungicida=obj[0]).aggregate(t=Avg('rendimientos'))['t']
                if rendimiento > 0:
                    dispercion_fungicida[year][obj[1]] = rendimiento
        except:
            pass

        try:
            dispercion_fungicida_lista[year] = OrderedDict()
            for obj in CHOICE_TIPO_FUNGICIDA:
                rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro.filter(year=year),tipo_planta__in=[2,3], tipo_fungicida=obj[0]).values_list('area_ha','rendimientos')
                if len(rendimiento) > 0:
                    a = [ [x[0],x[1]] for x in rendimiento]
                    dispercion_fungicida_lista[year][obj[1]] = a
        except:
            pass

    return render(request, template, {'data':data,
                                                                'dispercion_fertilidad':dispercion_fertilidad,
                                                                'dispercion_fungicida':dispercion_fungicida,
                                                                'dispercion_fertilidad_lista':dispercion_fertilidad_lista,
                                                                'dispercion_fungicida_lista':dispercion_fungicida_lista,
                                                                'num_familias': filtro.count(),
                                                                'years':years,
                                                                'num_familias2':num_familias2})

def detalles_tabla(request, template='herramientaDesicion/detallesTabla.html'):
    filtro = _queryset_filtrado(request)

    tabla_detalles = []
    lista_variedad = []
    for obj in filtro:
        if request.user.is_superuser:
            todo = obj.detalleplantios_set.all()
        else:
            todo = obj.detalleplantios_set.filter(encuesta__user=request.user)
        for i in todo:
            tabla_detalles.append([i.encuesta.entrevistado,
                                                     i.nombre_plantio,
                                                     i.area_ha,
                                                     i.get_tipo_planta_display(),
                                                     i.edad,
                                                     #[ unicode(x[1], "utf-8")  for x in CHOICES_VARIEDAD_PREDOMINANTE if x[0] in i.variedades],
                                                     i.produccion_kg,
                                                     i.rendimientos,
                                                     i.nivel,
                                                     i.get_tipo_sombra_display(),
                                                     i.nivel_sombra,
                                                     i.get_tipo_fertilizacion_display(),
                                                     i.get_tipo_fungicida_display(),
                                                     i.rendimientos,
                                                                         ])

    return render(request, template, {'tabla_detalles':tabla_detalles,
                                                                'num_familias': filtro.count()})

def contactenos(request):
    form_class = FormularioColabora
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            subject = request.POST.get('nombre', '')
            from_email = request.POST.get('correo', '')
            asunto = request.POST.get('asunto', '')
            mensaje = request.POST.get('mensaje', '')

            html_content = render_to_string('lineaBase/mail_template.html', {'nombre':subject,
                                                                                                             'correo': from_email,
                                                                                                              'mensaje':mensaje})
            text_content = strip_tags(html_content)

            #msg = EmailMultiAlternatives(asunto, text_content, from_email, ['fguharay@gmail.com','ninoska.hurtado@rikolto.org'])
            msg = EmailMultiAlternatives(asunto,
                                                                text_content,
                                                                from_email,
                                                                to=['fguharay@gmail.com','ninoska.hurtado@rikolto.org','falguni.guharay@worldcocoa.org'],
                                                                reply_to=[from_email])
            msg.attach_alternative(html_content, "text/html")
            try:
                msg.send()
            except BadHeaderError:
                messages.add_message(request, messages.ERROR, 'Error al enviar comentarios')
            messages.add_message(request, messages.SUCCESS, 'El mensaje fue enviado, gracias, nos pondremos en contacto con usted!')
            return HttpResponseRedirect('/linea-base/contactenos')

    return render(request, 'lineaBase/colabora.html', {'form': form_class,})

def to_unicode_or_bust(obj, encoding="latin1"):
    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj=unicode(obj, encoding)
    return obj

def contar_veces(elemento, lista):
    veces = 0
    for i in lista:
        if elemento == i:
            veces += 1
    return veces

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo


class DepartamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        #if not self.request.user.is_authenticated():
        #    return Departamento.objects.none()

        qs = Departamento.objects.all()

        pais = self.forwarded.get('pais', None)

        if pais:
            qs = qs.filter(pais=pais)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        #if not self.request.user.is_authenticated():
        #    return Departamento.objects.none()

        qs = Municipio.objects.all()

        departamento = self.forwarded.get('departamento', None)

        if departamento:
            qs = qs.filter(departamento=departamento)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class ComunidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        #if not self.request.user.is_authenticated():
        #    return Departamento.objects.none()

        qs = Comunidad.objects.all()

        municipio = self.forwarded.get('municipio', None)

        if municipio:
            qs = qs.filter(municipio=municipio)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


def obtener_lista_mapa_cacao(request):
    if request.is_ajax():
        lista = []
        for objeto in Encuesta.objects.all():
            try:
                apoyo = objeto.entrevistado.organizacion_apoyo.nombre
            except:
                apoyo='Sin Organizacion de apoyo'
            if objeto.entrevistado.longitud and objeto.entrevistado.latitud:
                dicc = dict(nombre=objeto.entrevistado.nombre, id=objeto.id,
                            lon=float(objeto.entrevistado.longitud) ,
                            lat=float(objeto.entrevistado.latitud),
                            altitud=objeto.entrevistado.altitud,
                            proyecto = str(", ".join([p.nombre for p in objeto.entrevistado.proyectos.all()])),
                            org_apoyo= apoyo,
                            )
                lista.append(dicc)

        serializado = simplejson.dumps(lista)
        return HttpResponse(serializado, content_type='application/json')

def saca_porcentajes(dato, total, formato=True):
    '''Si formato es true devuelve float caso contrario es cadena'''
    if dato != None:
        try:
            porcentaje = (dato/float(total)) * 100 if total != None or total != 0 else 0
        except:
            return 0
        if formato:
            return porcentaje
        else:
            return '%.2f' % porcentaje
    else:
        return 0

def get_productor(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        if request.user.is_superuser:
            personas = Entrevistados.objects.filter(nombre__icontains = q )[:10]
        else:
            personas = Entrevistados.objects.filter(nombre__icontains = q, user=request.user)[:10]
        #print personas
        results = []
        for person in personas:
            personas_json = {}
            personas_json['id'] = person.id
            personas_json['label'] = person.nombre
            personas_json['value'] = person.nombre
            results.append(personas_json)
    else:
        results = 'fail'
    return HttpResponse(simplejson.dumps(results), content_type='application/json')

def load_departamentos(request):
    pais_id = request.GET.get('pais')
    lugares = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
    return render(request, 'lineaBase/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_municipios(request):
    departamento_id = request.GET.get('departamento')
    lugares = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')
    return render(request, 'lineaBase/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_comunidades(request):
    municipio_id = request.GET.get('municipio')
    lugares = Comunidad.objects.filter(municipio_id=municipio_id).order_by('nombre')
    return render(request, 'lineaBase/lugares_dropdown_list_options.html', {'lugares': lugares})


VALID_VIEWS = {
        'organizacion': organizacion_pertenece,
        'composicion': composicion,
        'servicios': servicios_basico,
        'tenencia': tenencia,
        'seguridad': seguridad_alimentaria,
        'tierra': tierra,
        'rubros': rubros,
        'areas': areas_factores,
        'produccion': produccion,
        'manejo_poda':manejo_poda,
        'manejo_sombra':manejo_sombra,
        'manejo_maleza':manejo_maleza,
        'manejo_fertilidad':manejo_fertilidad,
        'manejo_hongos':manejo_hongos,
        'manejo_insectos':manejo_insectos,
        'manejo_nematodos':manejo_nematodos,
        'agroecologicas':opcion_agroecologica,
        'cosecha': cosecha,
        'comercializacion':comercializacion,
        'credito': credito,
        'mitigacion':mitigacion,
        'tecnicas':capasitacion_tecnica,
        'sociales':capasitacion_social,
        'detalles_conteo_parcela': detalles_conteo_parcela,
        'detalles_area_parcela': detalles_area_parcela,
        'detalles_area_cacao': detalles_area_cacao,
        'detalles_produccion_cacao':detalles_produccion_cacao,
        'detalles_productividad_cacao':detalles_productividad_cacao,
        'detalles_edad_cacao': detalles_edad_cacao,
        'detalles_variedad_cacao':detalles_variedad_cacao,
        'detalles_dispercion_cacao':detalles_dispercion_cacao,
        'detalles_tabla':detalles_tabla,
        'contactenos':contactenos,
    }
