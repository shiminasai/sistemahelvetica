from django import forms

from .models import *
from ckeditor.widgets import CKEditorWidget

class ConfigurationSingleForm(forms.ModelForm):
    texto_sistema = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_datos_generales = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_establecimiento = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_tranformacion = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_rentabilidad = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_mapa = forms.CharField(widget=CKEditorWidget(), required=False)
    texto_practicas_cci = forms.CharField(widget=CKEditorWidget(), required=False)
    class Meta:
        model = SiteConfiguration
        fields = "__all__"

class ConfiguraionForm(forms.ModelForm):
    departamento = forms.ModelMultipleChoiceField(queryset=Departamento.objects.all(),
                                                                                widget=forms.CheckboxSelectMultiple(),
                                                                                required=False)
    class Meta:
        model = LineaTiempoCostos
        fields = "__all__"
