# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from solo.admin import SingletonModelAdmin
from configuracion.models import *
from .forms import ConfiguraionForm, ConfigurationSingleForm

class LogosApoyoInlines(admin.TabularInline):
    model = LogosApoyo
    extra = 1

class UsuariosPlataformaInlines(admin.TabularInline):
    model = UsuariosPlataforma
    extra = 1

class DocumentosInlines(admin.TabularInline):
    model = Documentos
    extra = 1

class LogosUsuariosInlines(admin.TabularInline):
    model = LogosUsuarios
    extra = 1

class SiteAdmin(SingletonModelAdmin):
    form = ConfigurationSingleForm
    inlines = [LogosApoyoInlines,UsuariosPlataformaInlines,
                    LogosUsuariosInlines,DocumentosInlines]

admin.site.register(SiteConfiguration, SiteAdmin)
# Register your models here.

class DatosCostoInlines(admin.TabularInline):
    model = DatosCosto
    extra = 1

class LineaTiempoAdmin(admin.ModelAdmin):
    form = ConfiguraionForm
    inlines = [DatosCostoInlines]

admin.site.register(LineaTiempoCostos, LineaTiempoAdmin)
