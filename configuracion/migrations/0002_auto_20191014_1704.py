# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-10-14 23:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuracion', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuariosplataforma',
            name='logo',
            field=models.ImageField(null=True, upload_to='logos/usuarios/'),
        ),
        migrations.AddField(
            model_name='usuariosplataforma',
            name='web',
            field=models.URLField(blank=True, null=True),
        ),
    ]
