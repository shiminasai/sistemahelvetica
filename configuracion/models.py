# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from solo.models import SingletonModel
from lugar.models import Pais, Departamento

class SiteConfiguration(SingletonModel):
    site_name = models.CharField(max_length=255, default='Site Name')
    maintenance_mode = models.BooleanField(default=False)
    texto_sistema = models.TextField(null=True, blank=True)
    generales_resumen = models.CharField(max_length=75, default='resumen')
    texto_datos_generales = models.TextField(null=True, blank=True)
    establecimiento_resumen = models.CharField(max_length=75, default='resumen')
    texto_establecimiento = models.TextField(null=True, blank=True)
    transformacion_resumen = models.CharField(max_length=75, default='resumen')
    texto_tranformacion = models.TextField(null=True, blank=True)
    rentabilidad_resumen = models.CharField(max_length=75, default='resumen')
    texto_rentabilidad = models.TextField(null=True, blank=True)
    mapa_resumen = models.CharField(max_length=75, default='resumen')
    texto_mapa = models.TextField(null=True, blank=True)
    practicas_resumen = models.CharField(max_length=75, default='resumen')
    otro_resumen = models.CharField(max_length=75, default='resumen')
    texto_practicas_cci = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u"Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"

class LogosApoyo(models.Model):
    sitio = models.ForeignKey(SiteConfiguration)
    nombre = models.CharField(max_length=250, null=True, blank=True)
    web = models.URLField(null=True, blank=True)
    logo = models.ImageField(upload_to="logos/apoyo/")

    class Meta:
        verbose_name = 'Logo apoyo'
        verbose_name_plural = 'Logos de apoyo'
        ordering = ('id',)


class UsuariosPlataforma(models.Model):
    sitio = models.ForeignKey(SiteConfiguration)
    nombre = models.CharField(max_length=250)
    web = models.URLField(null=True, blank=True)
    logo = models.ImageField(upload_to="logos/usuarios/", null=True)

    class Meta:
        verbose_name = 'Logos Financiadores'
        verbose_name_plural = 'Logos Financiadores'
        ordering = ('id',)


class LogosUsuarios(models.Model):
    sitio = models.ForeignKey(SiteConfiguration)
    nombre = models.CharField(max_length=250)
    web = models.URLField(null=True, blank=True)
    logo = models.ImageField(upload_to="logos/logosusuarios/", null=True)

    class Meta:
        verbose_name = 'Logos Usuarios'
        verbose_name_plural = 'Logos Usuarios'
        ordering = ('id',)

CHOICE_PRIVADO = ( (1,'Privado'),(2,'Público') )

class Documentos(models.Model):
    sitio = models.ForeignKey(SiteConfiguration)
    nombre = models.CharField(max_length=250)
    fecha = models.DateTimeField(auto_now_add=True)
    adjuntar = models.FileField(upload_to="archivosCuadernos/")
    tipo = models.IntegerField(choices=CHOICE_PRIVADO, null=True, blank=True)

    class Meta:
        verbose_name = 'Documento'
        verbose_name_plural = 'Documentos'

CHOICES_ANIO = (
        (1, 'Año 1'),
        (2, 'Año 2'),
        (3, 'Año 3'),
        (4, 'Año 4'),
        (5, 'Año 5'),
        (6, 'Año 6'),
        (7, 'Año 7'),
        (8, 'Año 8'),
        (9, 'Año 9'),
        (10, 'Año 10'),
        (11, 'Año 11'),
        (12, 'Año 12'),
        (13, 'Año 13'),
        (14, 'Año 14'),
        (15, 'Año 15'),
        (16, 'Año 16'),
        (17, 'Año 17'),
        (18, 'Año 18'),
        (19, 'Año 19'),
        (20, 'Año 20'),
        (21, 'Año 21'),
        (22, 'Año 22'),
        (23, 'Año 23'),
        (24, 'Año 24'),
        (25, 'Año 25'),
    )

CHOICES_CULTIVOS_ESTADO = (
            (1, 'Establecimiento'),
            (2, 'Desarrollo'),
            (3, 'Producción'),
            (4, 'No tiene'),
    )
class LineaTiempoCostos(models.Model):
    fecha = models.DateField(null=True)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    departamento = models.ManyToManyField(Departamento)

    def __unicode__(self):
        return '%s - %s' % (self.fecha, self.pais)

    class Meta:
        verbose_name_plural = 'Línea de tiempo de cultivos para cálculo de rentabilidad'

class DatosCosto(models.Model):
    costo = models.ForeignKey(LineaTiempoCostos, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=CHOICES_ANIO, verbose_name='Año')
    cacao = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    cafe = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    musaceas = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    frutales = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    maderables = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    arboles = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)
    granos = models.IntegerField(choices=CHOICES_CULTIVOS_ESTADO)

    class Meta:
        verbose_name_plural = 'Calculos de rentabilidad'
