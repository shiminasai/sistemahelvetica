# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-11-03 07:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('transformacion', '0005_auto_20190808_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fichacierre',
            name='ciclo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
        ),
        migrations.AlterField(
            model_name='fichacosecha',
            name='ciclo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='fichacosechasecundaria',
            name='ciclo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='fichapisoplaga',
            name='ciclo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='fichapodasombra',
            name='ciclo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='fichasuelofertilidad',
            name='ciclo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Ciclo', verbose_name='Ciclo de inicio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='plagasenfermedadvisto',
            name='plagas',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, choices=[(b'A', b'Monilia'), (b'B', b'Mazorca negra'), (b'C', b'Mal de machete'), (b'D', b'Mal de talluelo en el vivero'), (b'E', b'Buba'), (b'F', b'Barrenadores de tallo'), (b'G', b'Zompopos'), (b'H', b'Chupadores o \xc3\xa1fidos'), (b'I', b'Escarabajos'), (b'J', b'Comej\xc3\xa9n'), (b'K', b'Ardillas'), (b'L', b'Otros')], max_length=23, null=True, verbose_name='Plagas y enfermedades'),
        ),
    ]
