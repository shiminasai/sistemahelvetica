# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

from lineabase.models import Entrevistados, Encuestadores
from cuadernosmes0.models import Ciclo, Years
from cuadernosmes3.models import DatosAnalisis
from cuadernosmes9.models import Especies
from .transformacion_static import *

from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money

# Create your models here.
class FichaPodaSombra(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='poda_sombra_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='poda_sombra_tecnico')

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self):
        self.year = self.fecha_visita.year
        super(FichaPodaSombra, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha poda y sombra"
        verbose_name_plural = "Ficha poda y sombra"


#--------------------- Sombra -------------------------------

class Cobertura1(models.Model):
    cobertura = models.FloatField('% de cobertura de sombra')
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name = 'Punto 1 cobertura de sombra'
        verbose_name_plural = 'Punto 1 cobertura de sombra'

class Punto1(models.Model):
    especie = models.ForeignKey(Especies, related_name='poda_sombra_punto1')
    pequena = models.FloatField(verbose_name='# árbol Pequeña')
    mediana = models.FloatField(verbose_name='# árbol Mediana')
    grande = models.FloatField(verbose_name='# árbol Grande')
    tipo = models.IntegerField(choices=CHOICE_TIPO_PUNTO, verbose_name= 'Tipo de árboles')
    uso = models.IntegerField(choices=CHOICE_TIPO_USO_PUNTO, verbose_name='Uso de árboles')
    tipo_de_copa = models.IntegerField(choices=CHOICE_TIPO_COPA_PUNTO)

    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Punto 1"

class Cobertura2(models.Model):
    cobertura = models.FloatField('% de cobertura de sombra')
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name = 'Punto 2 cobertura de sombra'
        verbose_name_plural = 'Punto 2 cobertura de sombra'

class Punto2(models.Model):
    especie = models.ForeignKey(Especies, related_name='poda_sombra_punto2')
    pequena = models.FloatField(verbose_name='# árbol Pequeña')
    mediana = models.FloatField(verbose_name='# árbol Mediana')
    grande = models.FloatField(verbose_name='# árbol Grande')
    tipo = models.IntegerField(choices=CHOICE_TIPO_PUNTO, verbose_name= 'Tipo de árboles')
    uso = models.IntegerField(choices=CHOICE_TIPO_USO_PUNTO, verbose_name='Uso de árboles')
    tipo_de_copa = models.IntegerField(choices=CHOICE_TIPO_COPA_PUNTO)

    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Punto 2"

class Cobertura3(models.Model):
    cobertura = models.FloatField('% de cobertura de sombra')
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name = 'Punto 3 cobertura de sombra'
        verbose_name_plural = 'Punto 3 cobertura de sombra'

class Punto3(models.Model):
    especie = models.ForeignKey(Especies, related_name='poda_sombra_punto3')
    pequena = models.FloatField(verbose_name='# árbol Pequeña')
    mediana = models.FloatField(verbose_name='# árbol Mediana')
    grande = models.FloatField(verbose_name='# árbol Grande')
    tipo = models.IntegerField(choices=CHOICE_TIPO_PUNTO, verbose_name= 'Tipo de árboles')
    uso = models.IntegerField(choices=CHOICE_TIPO_USO_PUNTO, verbose_name='Uso de árboles')
    tipo_de_copa = models.IntegerField(choices=CHOICE_TIPO_COPA_PUNTO)

    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Punto 3"

#------------------------ poda --------------------------------

class PodaPunto1(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS1)
    uno = models.FloatField(verbose_name='1')
    dos = models.FloatField(verbose_name='2')
    tres = models.FloatField(verbose_name='3')
    cuatro = models.FloatField(verbose_name='4')
    cinco = models.FloatField(verbose_name='5')
    seis = models.FloatField(verbose_name='6')
    siete = models.FloatField(verbose_name='7')
    ocho = models.FloatField(verbose_name='8')
    nueve = models.FloatField(verbose_name='9')
    diez = models.FloatField(verbose_name='10')

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 1 Poda'

class PodaPunto1B(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS2)
    uno = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 1 Poda'

class PodaPunto1C(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 1 Poda'

class PodaPunto2(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS1)
    uno = models.FloatField(verbose_name='1')
    dos = models.FloatField(verbose_name='2')
    tres = models.FloatField(verbose_name='3')
    cuatro = models.FloatField(verbose_name='4')
    cinco = models.FloatField(verbose_name='5')
    seis = models.FloatField(verbose_name='6')
    siete = models.FloatField(verbose_name='7')
    ocho = models.FloatField(verbose_name='8')
    nueve = models.FloatField(verbose_name='9')
    diez = models.FloatField(verbose_name='10')

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 2 Poda'

class PodaPunto2B(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS2)
    uno = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 2 Poda'

class PodaPunto2C(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Punto 2 Poda'

class PodaPunto3(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS1)
    uno = models.FloatField(verbose_name='1')
    dos = models.FloatField(verbose_name='2')
    tres = models.FloatField(verbose_name='3')
    cuatro = models.FloatField(verbose_name='4')
    cinco = models.FloatField(verbose_name='5')
    seis = models.FloatField(verbose_name='6')
    siete = models.FloatField(verbose_name='7')
    ocho = models.FloatField(verbose_name='8')
    nueve = models.FloatField(verbose_name='9')
    diez = models.FloatField(null=True, blank=True, verbose_name='10')

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name = 'Poda punto 3'
        verbose_name_plural = 'Poda punto 3'


class PodaPunto3B(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS2)
    uno = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name_plural = 'Poda punto 3 plantas'


class PodaPunto3C(models.Model):
    plantas = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='1')
    dos = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='2')
    tres = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='3')
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='4')
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='5')
    seis = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='6')
    siete = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='7')
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='8')
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='9')
    diez = models.IntegerField(choices=CHOICE_PRODUCCION, verbose_name='10', null=True, blank=True)

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return self.get_plantas_display()

    class Meta:
        verbose_name = 'Poda punto 3 nivel'
        verbose_name_plural = 'Poda punto 3 nivel'

#----------- analisis de sombra ------------------------

class AnalisisSombra(models.Model):
    densidad = models.IntegerField(
        choices=(
            (1,
             'Alta'),
            (2,
             'Adecuada'),
            (3,
             'Baja'),
        ),
        verbose_name='Densidad de árboles de sombra')
    forma_copa = models.IntegerField(
        choices=(
            (1,
             'Ancha'),
            (2,
             'Adecuada'),
            (3,
             'Angosta'),
        ),
        verbose_name='Forma de copa de árboles de sombra')
    arreglo = models.IntegerField(choices=((1, 'Uniforme'), (2, 'Desuniforme'),),
                                  verbose_name='Arreglo de árboles')
    hojarasca = models.IntegerField(
        choices=(
            (1,
             'Suficiente'),
            (2,
             'No Suficiente'),
        ),
        verbose_name='Cantidad de hojarasca ')
    calidad_hojarasca = models.IntegerField(
        choices=(
            (1,
             'Rico en nutrientes'),
            (2,
             'Pobre en nutriente'),
        ),
        verbose_name='Calidad de hojarasca ')
    competencia = models.IntegerField(
        choices=(
            (1,
             'Fuerte'),
            (2,
             'Mediana'),
            (3,
             'Leve'),
        ),
        verbose_name='Competencia de árboles con cacao')
    Problema = models.IntegerField(
        choices=(
            (1,
             'Cobertura'),
            (2,
             'Mal arreglo'),
            (3,
             'Competencia'),
            (4,
             'Densidad Tipo de árboles'),
            (5,
             'Ninguno')),
        verbose_name='Problema de sombra')
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Análisis sobre sombra y árboles de sombra"

class AccionesSombra(models.Model):
    accion = models.IntegerField(
        choices=CHOICE_ACCIONES_SOMBRA,
        verbose_name="Que acciones hay que realizar ")
    ficha = models.ForeignKey(FichaPodaSombra)


class ReducirSombra(models.Model):
    poda = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Podando árboles")
    poda_cuales = models.CharField(max_length=350)
    eliminando = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Eliminando árboles")
    eliminando_cuales = models.CharField(max_length=350)
    todo = models.IntegerField(
        choices=CHOICE_TODO,
        verbose_name="En todo la parcela o Solo en una parte de la parcela")
    que_parte = models.CharField(max_length=250, null=True, blank=True)
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Si marca reducir la sombra"


class AumentarSombra(models.Model):
    sembrando = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Sembrando árboles")
    sembrando_cuales = models.CharField(max_length=350)
    cambiando = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Cambiando tipo árboles")
    cambiando_cuales = models.CharField(max_length=350)
    todo = models.IntegerField(
        choices=CHOICE_TODO,
        verbose_name="En todo la parcela o Solo en una parte de la parcela")
    que_parte = models.CharField(max_length=250, null=True, blank=True)
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name_plural = "Si marca aumentar la sombra"


class ManejoSombra(models.Model):
    herramientas = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Tiene herramienta para manejo de sombra? ")
    formacion = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="Tiene formación para manejo de sombra? ")
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name = "Herramienta y formación de sombras"

class AnalisisPoda(models.Model):
    campo1 = MultiSelectField(choices=CHOICES_PROBLEMA_PLANTA, verbose_name='¿Cuáles son los problemas principales en cuanto a las estructuras de las plantas?')
    campo2 = MultiSelectField(choices=CHOICES_TIPO_PODA, verbose_name='¿Qué tipo de poda podemos aplicar para mejorar la estructura de las plantas?')
    campo3 = models.IntegerField(choices=CHOICE_REALIZA_PODA, verbose_name='¿Dónde se va a realizar la poda para mejorar la estructura de las plantas?')
    campo4 = models.IntegerField(choices=CHOICE_VIGOR, verbose_name='Las plantas tienen suficiente vigor, hojas y ramas para ser podadas?')
    campo5 = models.IntegerField(choices=CHOICE_ENTRADA_LUZ, verbose_name='¿Cómo podemos mejorar la entrada de luz en las plantas con la poda?')
    campo6 = MultiSelectField(choices=CHOICES_FECHA_PODA, verbose_name='¿Cuándo se van a realizar las podas?')

    ficha = models.ForeignKey(FichaPodaSombra)

    def __unicode__(self):
        return 'Analisis'

    class Meta:
        verbose_name_plural = 'Análisis de poda y acciones'

class ManejoPoda(models.Model):
    herramientas = MultiSelectField(
        choices=CHOICES_EQUIPO_PODA,
        verbose_name="¿Tenemos los equipos necesarios para realizar la poda?")
    formacion = models.IntegerField(
        choices=CHOICE_PODA,
        verbose_name="¿Tenemos la formación para realizar la poda?")
    ficha = models.ForeignKey(FichaPodaSombra)

    class Meta:
        verbose_name = "Herramienta y formación de poda"

#----------------------fin poda y sombra ---------------

#-------------------- inicio de suelo y fertilidad -----

class FichaSueloFertilidad(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='persona_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='persona_tecnico')

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self):
        self.year = self.fecha_visita.year
        super(FichaSueloFertilidad, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha suelo y fertilidad"
        verbose_name_plural = "Ficha suelo y fertilidad"

class Punto1Suelo(models.Model):
    uso_parcela = models.IntegerField(choices=CHOICE_SUELO_USO_PARCELA,
                  verbose_name="Cuál era el uso de la parcela antes de establecer el cacao?")
    limitante = MultiSelectField(choices=CHOICE_SUELO_LIMITANTES,
          verbose_name='Cuáles son los limitantes productivos del suelo de la parcela?')
    orientacion = MultiSelectField(choices=CHOICE_SUELO_ORIENTACION,
        verbose_name='Quien su orientación de manejo de fertilidad de suelo?')
    abonos = MultiSelectField(choices=CHOICE_SUELO_ABONOS,
        verbose_name='4. De donde consigue los abonos, fertilizantes y enmiendas de suelo?')

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Punto 1"

    class Meta:
        verbose_name = '1.Conversación con productor'
        verbose_name_plural = '1.Conversación con productor'

class PuntoASuelo(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_EROSION_OPCION,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_EROSION_RESPUESTA,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Indicadores de erosión"

    class Meta:
        verbose_name = 'Indicadores de erosión'
        verbose_name_plural = 'Indicadores de erosión'

class PuntoBSuelo(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_CONSERVACION_OPCION,
                  verbose_name="Obras")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_CONSERVACION_RESPUESTA,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Obras de conservación de suelo"

    class Meta:
        verbose_name = 'Obras de conservación de suelo'
        verbose_name_plural = 'Obras de conservación de suelo'

class Punto2ASuelo(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_DRENAJE_OPCION,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_EROSION_RESPUESTA,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Indicadores de drenaje"

    class Meta:
        verbose_name = 'Indicadores de drenaje'
        verbose_name_plural = 'Indicadores de drenaje'

class Punto2BSuelo(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_DRENAJE_OPCION2,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_CONSERVACION_RESPUESTA,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Obras de drenaje"

    class Meta:
        verbose_name = 'Obras de drenaje'
        verbose_name_plural = 'Obras de drenaje'

class Punto3SueloPunto1(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_OPCION_PUNTOS,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_RESPUESTA_PUNTOS,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Punto 1"

    class Meta:
        verbose_name = 'Salud de Raíces punto 1'
        verbose_name_plural = 'Salud de Raíces punto 1'

class Punto3SueloPunto2(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_OPCION_PUNTOS,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_RESPUESTA_PUNTOS,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Punto 2"

    class Meta:
        verbose_name = 'Salud de Raíces punto 2'
        verbose_name_plural = 'Salud de Raíces punto 2'

class Punto3SueloPunto3(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SUELO_OPCION_PUNTOS,
                  verbose_name="Indicadores")
    respuesta = models.IntegerField(choices=CHOICE_SUELO_RESPUESTA_PUNTOS,
                verbose_name="respuesta")

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Punto 3"

    class Meta:
        verbose_name = 'Salud de Raíces punto 3'
        verbose_name_plural = 'Salud de Raíces punto 3'

class Punto4Suelo(models.Model):
    area = models.FloatField(verbose_name='Tamaño de Área de Cacao SAF')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    densidad = models.FloatField(verbose_name='Cantidad Arboles')

    ficha = models.ForeignKey(FichaSueloFertilidad)
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1.0
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        super(Punto4Suelo, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Balance de nutrientes de parcela Cacao SAF"

    class Meta:
        verbose_name = 'Balance de nutrientes de parcela Cacao SAF'
        verbose_name_plural = 'Balance de nutrientes de parcela Cacao SAF'

class Punto4SueloCosecha(models.Model):
    producto = models.IntegerField(choices=CHOICE_SUELO_PRODUCTO_COSECHA)
    cantidad = models.FloatField()

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Cosechas del Productos SAF"

    class Meta:
        verbose_name = 'Cosechas del Productos SAF'
        verbose_name_plural = 'Cosechas del Productos SAF'

class Punto4SueloSI(models.Model):
    opcion = models.IntegerField(choices=CHOICE_SI_NO)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Se regresa la cascara a la parcela como abono?"

    class Meta:
        verbose_name = 'Se regresa la cascara a la parcela como abono?'
        verbose_name_plural = 'Se regresa la cascara a la parcela como abono?'


class TipoFertilizantes(models.Model):
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return u'%s' % (self.nombre)

CHOICE_UNIDAD_MEDIDA_ABONO = ((1,'lb/mz'),(2,'lb/planta '),(3,'oz/planta'),(4,'L/mz'),(5, 'qq/mz'))

class Punto5SueloAbonos(models.Model):
    tipo = models.ForeignKey(TipoFertilizantes)
    cantidad = models.FloatField('Cantidad(Valor)')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_MEDIDA_ABONO)
    humedad = models.FloatField('Humedad (%)')
    frecuencia = models.FloatField('Frecuencia (por año)')
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA, verbose_name='Meses de aplicación')

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Abonos, fertilizantes y Enmiendas aplicadas en la parcela cacao SAF"

    class Meta:
        verbose_name = 'Abonos, fertilizantes y Enmiendas aplicadas en la parcela cacao SAF'
        verbose_name_plural = 'Abonos, fertilizantes y Enmiendas aplicadas en la parcela cacao SAF'

class Punto6AnalisisSuelo(models.Model):
    variable = models.ForeignKey(DatosAnalisis)
    valor = models.FloatField()

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Datos de análisis de suelo"

    class Meta:
        verbose_name = 'Datos de análisis de suelo'
        verbose_name_plural = 'Datos de análisis de suelo'


class Punto7TipoSuelo(models.Model):
    opcion = models.IntegerField(choices=(
                                        (1,'Ultisol (rojo)'),
                                        (2, 'Andisol (volcánico)'),
                                        (3, 'Vertisol'),))

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Tipo de suelo"

    class Meta:
        verbose_name = 'Tipo de suelo'
        verbose_name_plural = 'Tipo de suelo'


class Punto8SueloPropuesta(models.Model):
    tipo = models.ForeignKey(TipoFertilizantes)
    cantidad = models.FloatField('Cantidad(Valor)')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_MEDIDA_ABONO)
    frecuencia = models.FloatField('Frecuencia (por año)')
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA, verbose_name='Meses de aplicación')

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Nueva Propuesta de Fertilización Generada"

    class Meta:
        verbose_name = 'Nueva Propuesta de Fertilización Generada'
        verbose_name_plural = 'Nueva Propuesta de Fertilización Generada'

class Punto9Erosion(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_1)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_1_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de eroción"

    class Meta:
        verbose_name = 'Erosión de Suelo'
        verbose_name_plural = 'Erosión de Suelo'

class Punto9Drenaje(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_2)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_2_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de mal drenaje"

    class Meta:
        verbose_name = 'Mal drenaje y encharamientos'
        verbose_name_plural = 'Mal drenaje y encharamientos'

class Punto9Nutrientes(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_3)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_3_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de deficiencia nutrientes"

    class Meta:
        verbose_name = 'Deficiencia de Nutrientes'
        verbose_name_plural = 'Deficiencia de Nutrientes'

class Punto9Exceso(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_4)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_4_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de exceso de nutrientes"

    class Meta:
        verbose_name = 'Exceso de nutrientes'
        verbose_name_plural = 'Exceso de nutrientes'

class Punto9Desbalance(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_5)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_5_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de desbalance de nutrientes"

    class Meta:
        verbose_name = 'Desbalance de nutrientes'
        verbose_name_plural = 'Desbalance de nutrientes'

class Punto9Enfermedades(models.Model):
    limitaciones = models.IntegerField(choices=CHOICE_PUNTO9_LIMITACION_6)
    acciones = MultiSelectField(choices=CHOICE_PUNTO9_LIMITACION_6_ACCION, verbose_name='Acciones potenciales')
    donde = models.IntegerField(choices=CHOICE_PUNTO9_DONDE)

    ficha = models.ForeignKey(FichaSueloFertilidad)

    def __unicode__(self):
        return u"Toma de decisión con base en las observaciones de enfermedades y plagas"

    class Meta:
        verbose_name = 'Enfermedades y plagas de raíces'
        verbose_name_plural = 'Enfermedades y plagas de raíces'

# ------------ fin suelo y fertilidad ---------------------

class FichaCosechaSecundaria(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='cosecha_secundaria_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='cosecha_secudaria_tecnico')
    area_parcela = models.FloatField(null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self):
        self.year = self.fecha_visita.year
        if self.unidad == 1:
            self.area_ha = self.area_parcela / 1.4
        if self.unidad == 2:
            self.area_ha = self.area_parcela / 1.0
        if self.unidad == 3:
            self.area_ha = self.area_parcela / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area_parcela) / 10000
        if self.unidad == 5:
            self.area_ha = self.area_parcela / 24.0
        super(FichaCosechaSecundaria, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha cosecha secundaria"
        verbose_name_plural = "Ficha cosecha secundaria"

class EspeciesCosechaSecundaria(models.Model):
    tipo = models.IntegerField(choices=CHOICE_TIPO_ESPECIES_SECUNDARIAS)
    nombre = models.CharField(max_length=250)
    unidad = models.IntegerField(choices=UNIDAD_ESPECIES_COSECHA, null=True)

    def __unicode__(self):
        return '%s - %s' % (self.nombre, self.get_unidad_display())

class InventarioParcela(models.Model):
    especie = models.ForeignKey(EspeciesCosechaSecundaria)
    numero = models.FloatField('Número de plantas/árboles')

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    class Meta:
        verbose_name_plural = 'Inventario resumido de la parcela'

class CosechaSecundariaPunto1(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaSecundariaPunto1, self).save(*args, **kwargs)


    def __unicode__(self):
        return u"2.1 Punto 1"

    class Meta:
        verbose_name='Cacao punto 1'
        verbose_name_plural='Cacao punto 1'

class FrutalesMusaceasPunto1(models.Model):
    especie = models.ForeignKey(EspeciesCosechaSecundaria,
                                                limit_choices_to={'tipo__in': [2,3]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(FrutalesMusaceasPunto1, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.2 Frutales y musaceas punto 1"

    class Meta:
        verbose_name = '1.2 Frutales y musaceas punto 1'
        verbose_name_plural = '1.2 Frutales y musaceas punto 1'

class MaderablesServicioPunto1(models.Model):
    #tipo = models.IntegerField(choices=CHOICE_MADERABLES_SERVICIOS_COSECHA_SECUNDARIAS)
    especie = models.ForeignKey(EspeciesCosechaSecundaria, null=True,
                                            limit_choices_to={'tipo__in': [4,5]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(MaderablesServicioPunto1, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.3 Maderable y servicio punto 1"

    class Meta:
        verbose_name = '1.3 Maderable y servicio punto 1'
        verbose_name_plural = '1.3 Maderable y servicio punto 1'

class CosechaSecundariaPunto2(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaSecundariaPunto2, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"2.2 Punto 2"

    class Meta:
        verbose_name = 'Cacao punto 2'
        verbose_name_plural = 'Cacao punto 2'

class FrutalesMusaceasPunto2(models.Model):
    especie = models.ForeignKey(EspeciesCosechaSecundaria,
                                                    limit_choices_to={'tipo__in': [2,3]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(FrutalesMusaceasPunto2, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.2 Frutales y musaceas punto 2"

    class Meta:
        verbose_name = '1.2 Frutales y musaceas punto 2'
        verbose_name_plural = '1.2 Frutales y musaceas punto 2'

class MaderablesServicioPunto2(models.Model):
    #tipo = models.IntegerField(choices=CHOICE_MADERABLES_SERVICIOS_COSECHA_SECUNDARIAS)
    especie = models.ForeignKey(EspeciesCosechaSecundaria, null=True,
                                                    limit_choices_to={'tipo__in': [4,5]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(MaderablesServicioPunto2, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.3 Maderable y servicio punto 2"

    class Meta:
        verbose_name = '1.3 Maderable y servicio punto 2'
        verbose_name_plural = '1.3 Maderable y servicio punto 2'

class CosechaSecundariaPunto3(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaSecundariaPunto3, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"2.3 Punto 3"

    class Meta:
        verbose_name = 'Cacao punto 3'
        verbose_name_plural = 'Cacao punto 3'

class FrutalesMusaceasPunto3(models.Model):
    especie = models.ForeignKey(EspeciesCosechaSecundaria,
                                                limit_choices_to={'tipo__in': [2,3]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(FrutalesMusaceasPunto3, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.2 Frutales y musaceas punto 3"

    class Meta:
        verbose_name = '1.2 Frutales y musaceas punto 3'
        verbose_name_plural = '1.2 Frutales y musaceas punto 3'

class MaderablesServicioPunto3(models.Model):
    #tipo = models.IntegerField(choices=CHOICE_MADERABLES_SERVICIOS_COSECHA_SECUNDARIAS)
    especie = models.ForeignKey(EspeciesCosechaSecundaria, null=True,
                                                    limit_choices_to={'tipo__in': [4,5]})
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        self.contador = contar

        super(MaderablesServicioPunto3, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"1.3 Maderable y servicio punto 3"

    class Meta:
        verbose_name = '1.3 Maderable y servicio punto 3'
        verbose_name_plural = '1.3 Maderable y servicio punto 3'

class CosechaSecundariaAreaPlantas(models.Model):
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    plantas = models.FloatField('Número de plantas')

    ficha = models.ForeignKey(FichaCosechaSecundaria)
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1.0
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        super(CosechaSecundariaAreaPlantas, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Area y número de platas"

class CosechaSecundariaAnalisis(models.Model):
    analisis1 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_1,
                                verbose_name='3.1-¿Cuál es el problema principal que afecta el rendimiento productivo de la parcela de cacao?')
    analisis2 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_2,
                                verbose_name='3.2-¿Cuál es la causa de la pérdida de producción en la parcela de cacao?  ')
    analisis3 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_3,
                                verbose_name='3.3-¿Qué prácticas se pueden realizar en la parcela de cacao para mejorar la cosecha?  ')

    ficha = models.ForeignKey(FichaCosechaSecundaria)

    def __unicode__(self):
        return u"Análisis sobre la cosecha y acciones"

    class Meta:
        verbose_name='Análisis sobre la cosecha y acciones'
        verbose_name_plural='Análisis sobre la cosecha y acciones'

#----------fin Cosecha Secundaria -------------------

#------- inicio de piso y plaga -------------------------

class FichaPisoPlaga(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='piso_plaga_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='piso_plaga_tecnico')

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self):
        self.year = self.fecha_visita.year
        super(FichaPisoPlaga, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha Piso plagas y enfermedades"
        verbose_name_plural = "Ficha Piso plagas y enfermedades"

class PlagasEnfermedadVisto(models.Model):
    plagas = MultiSelectField(choices=CHOICE_ENFERMEDADES_CACAOTALES_MULTI,
                blank=True, null=True, verbose_name="Plagas y enfermedades")

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"PlagasEnfermedad"

    class Meta:
        verbose_name_plural = '¿Cuáles son las plagas y enfermedades que han visto en su cacaotal?'

class PlagasEnfermedadAfectan(models.Model):
    plagas = models.IntegerField(choices=CHOICE_ENFERMEDADES_CACAOTALES,
                blank=True, null=True, verbose_name="Plagas y enfermedades")
    promedio = models.FloatField("¿Promedio nivel de daño en %?")

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"PlagasEnfermedad"

    class Meta:
        verbose_name_plural = '¿Cuáles son las plagas y enfermedades que afectan  su cacaotal año con año?'

class AccionesEnfermedad(models.Model):
    plagas_acciones = models.IntegerField(choices=CHOICE_ACCIONES_ENFERMEDADES,
                    blank=True, null=True, verbose_name="Manejo de Plagas y enfermedadess")
    cuantas_veces = models.IntegerField(blank=True, null=True,
                verbose_name="Cuantas veces realizan el manejo")
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA,
            verbose_name='En qué meses realizan el manejo')
    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"AccionesEnfermedad"

    class Meta:
        verbose_name = "¿Qué hacen para manejar las plagas y enfermedades actualmente?"

class PisoPunto1(models.Model):
    punto1 = MultiSelectField(choices=CHOICE_PISO1,
            verbose_name='4.¿Cuáles son las hierbas qué cubren el piso y sube sobre las planta de cacao? ')
    punto2 = MultiSelectField(choices=CHOICE_PISO1,
            verbose_name='5.¿Cuáles son las hierbas qué usted considera dañino? ')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"piso 1 y 2"

    class Meta:
        verbose_name_plural = 'Preguntas piso'

class PisoPunto3(models.Model):
    manejo = models.IntegerField(choices=CHOICE_PISO3,
            verbose_name="Manejo de piso",
            blank=True, null=True)
    veces = models.FloatField("Cuántas veces realizan el manejo?")
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA,
            verbose_name='En qué meses vamos a realiza las acciones')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 3"

    class Meta:
        verbose_name_plural = '6.¿Qué hacen para manejar las malas hierbas actualmente?'

class PisoPunto4(models.Model):
    manejo = MultiSelectField(choices=CHOICE_PISO4)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 4"

    class Meta:
        verbose_name_plural = '7.¿De dónde viene su orientación de manejo plasgas, enfermedades y malas hierbas?'

class ObservacionPunto1(models.Model):
    planta = models.IntegerField(choices=CHOICE_OBSERVACION_PUNTO1,
                                blank=True, null=True)
    uno = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True, verbose_name='Diez')

    contador = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar = 0
        if self.uno == 1:
            contar += 1
        if self.dos == 1:
            contar += 1
        if self.tres == 1:
            contar += 1
        if self.cuatro == 1:
            contar += 1
        if self.cinco == 1:
            contar += 1
        if self.seis == 1:
            contar += 1
        if self.siete == 1:
            contar += 1
        if self.ocho == 1:
            contar += 1
        if self.nueve == 1:
            contar += 1
        if self.dies == 1:
            contar += 1

        self.contador = contar

        super(ObservacionPunto1, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Punto1"

    class Meta:
        verbose_name_plural = 'Punto 1'

class ObservacionPunto1Nivel(models.Model):
    planta = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)

    alta = models.IntegerField(editable=False, null=True, blank=True)
    media = models.IntegerField(editable=False, null=True, blank=True)
    baja = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar_alta = 0
        if self.uno == 1:
            contar_alta += 1
        if self.dos == 1:
            contar_alta += 1
        if self.tres == 1:
            contar_alta += 1
        if self.cuatro == 1:
            contar_alta += 1
        if self.cinco == 1:
            contar_alta += 1
        if self.seis == 1:
            contar_alta += 1
        if self.siete == 1:
            contar_alta += 1
        if self.ocho == 1:
            contar_alta += 1
        if self.nueve == 1:
            contar_alta += 1
        if self.dies == 1:
            contar_alta += 1
        self.alta = contar_alta

        contar_media = 0
        if self.uno == 2:
            contar_media += 1
        if self.dos == 2:
            contar_media += 1
        if self.tres == 2:
            contar_media += 1
        if self.cuatro == 2:
            contar_media += 1
        if self.cinco == 2:
            contar_media += 1
        if self.seis == 2:
            contar_media += 1
        if self.siete == 2:
            contar_media += 1
        if self.ocho == 2:
            contar_media += 1
        if self.nueve == 2:
            contar_media += 1
        if self.dies == 2:
            contar_media += 1
        self.media = contar_media

        contar_baja = 0
        if self.uno == 3:
            contar_baja += 1
        if self.dos == 3:
            contar_baja += 1
        if self.tres == 3:
            contar_baja += 1
        if self.cuatro == 3:
            contar_baja += 1
        if self.cinco == 3:
            contar_baja += 1
        if self.seis == 3:
            contar_baja += 1
        if self.siete == 3:
            contar_baja += 1
        if self.ocho == 3:
            contar_baja += 1
        if self.nueve == 3:
            contar_baja += 1
        if self.dies == 3:
            contar_baja += 1
        self.baja = contar_baja

        super(ObservacionPunto1Nivel, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Punto1 nivel produccion"

    class Meta:
        verbose_name_plural = 'Punto 1 nivel de producción'


class ObservacionPunto2(models.Model):
    planta = models.IntegerField(choices=CHOICE_OBSERVACION_PUNTO1,
                                blank=True, null=True)
    uno = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)

    contador = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar = 0
        if self.uno == 1:
            contar += 1
        if self.dos == 1:
            contar += 1
        if self.tres == 1:
            contar += 1
        if self.cuatro == 1:
            contar += 1
        if self.cinco == 1:
            contar += 1
        if self.seis == 1:
            contar += 1
        if self.siete == 1:
            contar += 1
        if self.ocho == 1:
            contar += 1
        if self.nueve == 1:
            contar += 1
        if self.dies == 1:
            contar += 1

        self.contador = contar

        super(ObservacionPunto2, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Punto2"

    class Meta:
        verbose_name_plural = 'Punto 2'

class ObservacionPunto2Nivel(models.Model):
    planta = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)

    alta = models.IntegerField(editable=False, null=True, blank=True)
    media = models.IntegerField(editable=False, null=True, blank=True)
    baja = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar_alta = 0
        if self.uno == 1:
            contar_alta += 1
        if self.dos == 1:
            contar_alta += 1
        if self.tres == 1:
            contar_alta += 1
        if self.cuatro == 1:
            contar_alta += 1
        if self.cinco == 1:
            contar_alta += 1
        if self.seis == 1:
            contar_alta += 1
        if self.siete == 1:
            contar_alta += 1
        if self.ocho == 1:
            contar_alta += 1
        if self.nueve == 1:
            contar_alta += 1
        if self.dies == 1:
            contar_alta += 1
        self.alta = contar_alta

        contar_media = 0
        if self.uno == 2:
            contar_media += 1
        if self.dos == 2:
            contar_media += 1
        if self.tres == 2:
            contar_media += 1
        if self.cuatro == 2:
            contar_media += 1
        if self.cinco == 2:
            contar_media += 1
        if self.seis == 2:
            contar_media += 1
        if self.siete == 2:
            contar_media += 1
        if self.ocho == 2:
            contar_media += 1
        if self.nueve == 2:
            contar_media += 1
        if self.dies == 2:
            contar_media += 1
        self.media = contar_media

        contar_baja = 0
        if self.uno == 3:
            contar_baja += 1
        if self.dos == 3:
            contar_baja += 1
        if self.tres == 3:
            contar_baja += 1
        if self.cuatro == 3:
            contar_baja += 1
        if self.cinco == 3:
            contar_baja += 1
        if self.seis == 3:
            contar_baja += 1
        if self.siete == 3:
            contar_baja += 1
        if self.ocho == 3:
            contar_baja += 1
        if self.nueve == 3:
            contar_baja += 1
        if self.dies == 3:
            contar_baja += 1
        self.baja = contar_baja

        super(ObservacionPunto2Nivel, self).save(*args, **kwargs)


    def __unicode__(self):
        return u"Punto2 nivel produccion"

    class Meta:
        verbose_name_plural = 'Punto 2 nivel de producción'

class ObservacionPunto3(models.Model):
    planta = models.IntegerField(choices=CHOICE_OBSERVACION_PUNTO1,
                                blank=True, null=True)
    uno = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_SI_NO, blank=True, null=True)

    contador = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar = 0
        if self.uno == 1:
            contar += 1
        if self.dos == 1:
            contar += 1
        if self.tres == 1:
            contar += 1
        if self.cuatro == 1:
            contar += 1
        if self.cinco == 1:
            contar += 1
        if self.seis == 1:
            contar += 1
        if self.siete == 1:
            contar += 1
        if self.ocho == 1:
            contar += 1
        if self.nueve == 1:
            contar += 1
        if self.dies == 1:
            contar += 1

        self.contador = contar

        super(ObservacionPunto3, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Punto3"

    class Meta:
        verbose_name_plural = 'Punto 3'

class ObservacionPunto3Nivel(models.Model):
    planta = models.IntegerField(choices=CHOICE_PLANTAS3)
    uno = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dos = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    tres = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cuatro = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    cinco = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    seis = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    siete = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    ocho = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    nueve = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)
    dies = models.IntegerField(choices=CHOICE_PRODUCCION,
            blank=True, null=True)

    alta = models.IntegerField(editable=False, null=True, blank=True)
    media = models.IntegerField(editable=False, null=True, blank=True)
    baja = models.IntegerField(editable=False, null=True, blank=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def save(self, *args, **kwargs):
        contar_alta = 0
        if self.uno == 1:
            contar_alta += 1
        if self.dos == 1:
            contar_alta += 1
        if self.tres == 1:
            contar_alta += 1
        if self.cuatro == 1:
            contar_alta += 1
        if self.cinco == 1:
            contar_alta += 1
        if self.seis == 1:
            contar_alta += 1
        if self.siete == 1:
            contar_alta += 1
        if self.ocho == 1:
            contar_alta += 1
        if self.nueve == 1:
            contar_alta += 1
        if self.dies == 1:
            contar_alta += 1
        self.alta = contar_alta

        contar_media = 0
        if self.uno == 2:
            contar_media += 1
        if self.dos == 2:
            contar_media += 1
        if self.tres == 2:
            contar_media += 1
        if self.cuatro == 2:
            contar_media += 1
        if self.cinco == 2:
            contar_media += 1
        if self.seis == 2:
            contar_media += 1
        if self.siete == 2:
            contar_media += 1
        if self.ocho == 2:
            contar_media += 1
        if self.nueve == 2:
            contar_media += 1
        if self.dies == 2:
            contar_media += 1
        self.media = contar_media

        contar_baja = 0
        if self.uno == 3:
            contar_baja += 1
        if self.dos == 3:
            contar_baja += 1
        if self.tres == 3:
            contar_baja += 1
        if self.cuatro == 3:
            contar_baja += 1
        if self.cinco == 3:
            contar_baja += 1
        if self.seis == 3:
            contar_baja += 1
        if self.siete == 3:
            contar_baja += 1
        if self.ocho == 3:
            contar_baja += 1
        if self.nueve == 3:
            contar_baja += 1
        if self.dies == 3:
            contar_baja += 1
        self.baja = contar_baja

        super(ObservacionPunto3Nivel, self).save(*args, **kwargs)


    def __unicode__(self):
        return u"Punto3 nivel produccion"

    class Meta:
        verbose_name_plural = "Punto 3 nivel de producción"

class PisoPunto5(models.Model):
    estado = models.IntegerField(choices=CHOICE_PISO5,
            verbose_name="Estado de Piso",
            blank=True, null=True)
    conteo = models.FloatField('Conteo (números)')
    porcentaje = models.FloatField('% de cobertura')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 5"

    class Meta:
        verbose_name_plural = 'Cuadro para apuntar los datos de piso'

class ProblemasPrincipales(models.Model):
    observadas = MultiSelectField(choices=CHOICE_ENFERMEDADES,
            verbose_name='Las plagas y enfermedades observadas en la parcela')
    situacion = models.IntegerField(choices=CHOICE_SITUACION_PLAGAS,
                                                                blank=True, null=True,
                                                                verbose_name='Situación de las plagas en la parcela')
    principales = MultiSelectField(choices=CHOICE_ENFERMEDADES,
            verbose_name='Las plagas y enfermedades principales en la parcela')
    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"problemas principales"

    class Meta:
        verbose_name_plural = '¿Cuáles son los problemas principales en cuanto a las plagas y enfermedades en la parcela?'

class Punto6Plagas(models.Model):
    observaciones = MultiSelectField(choices=CHOICE_ENFERMEDADES_PUNTO6_1,
            verbose_name='Observaciones de suelo ')
    sombra = models.IntegerField(choices=CHOICE_ENFERMEDADES_PUNTO6_2,
            verbose_name="Observaciones de sombra", blank=True, null=True)
    manejo = MultiSelectField(choices=CHOICE_ENFERMEDADES_PUNTO6_3,
            verbose_name='Observaciones de manejo ')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 6"

    class Meta:
        verbose_name_plural = '¿Por qué hay problemas de plagas o enfermedades?'

class Punto7Plagas(models.Model):
    manejo = models.IntegerField(choices=CHOICE_ACCIONES_PUNTO7_1,
            verbose_name="Manejo de plagas y enfermedades", blank=True, null=True)
    parte = models.IntegerField(choices=CHOICE_ACCIONES_PUNTO7_2,
            verbose_name="En que parte", blank=True, null=True)
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA,
            verbose_name='En qué meses vamos a realizar el manejo')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 7"

    class Meta:
        verbose_name_plural = '¿Qué acciones vamos a realizar para reducir los daños de las plagas y enfermedades?'

class Punto8y9Plagas(models.Model):
    equipos = MultiSelectField(choices=CHOICE_ENFERMEDADES_PUNTO8,
            verbose_name='8.¿Tenemos los equipos necesarios para realizar manejo de plagas y enfermedades?')

    opcion = models.IntegerField(choices=CHOICE_SI_NO,
            verbose_name="9.¿Tenemos la formación para realizar el manejo de plagas y enfermedades?",
            blank=True, null=True)

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 8 y 9"

    class Meta:
        verbose_name_plural = 'Preguntas'

class PisoPunto6(models.Model):
    manejo = MultiSelectField(choices=CHOICE_PISO6_1,
            verbose_name='La competencia entre malas hierbas y las plantas de cacao?')
    estado = models.IntegerField(choices=CHOICE_PISO6_2,
            verbose_name="La cobertura del piso de cacaotal",
            blank=True, null=True)
    maleza = MultiSelectField(choices=CHOICE_PISO6_3,
            verbose_name='Tipo de malezas que compiten')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 6"

    class Meta:
        verbose_name_plural = '8. ¿Cuáles son los problemas principales en cuanto a manejo de piso en la parcela?'

class PisoPunto7(models.Model):
    suelo = MultiSelectField(choices=CHOICE_PISO7_1,
            verbose_name='Observaciones de suelo ')
    sombra = MultiSelectField(choices=CHOICE_PISO7_2,
            verbose_name='Observaciones de sombra')
    manejo = MultiSelectField(choices=CHOICE_PISO7_3,
            verbose_name='Observaciones de manejo')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 7"

    class Meta:
        verbose_name_plural = '9. Por qué hay problemas piso?'

class PisoPunto8(models.Model):
    piso = models.IntegerField(choices=CHOICE_PISO8,
            verbose_name="Manejo de piso",
            blank=True, null=True)
    parte = models.IntegerField(choices=CHOICE_ACCIONES_PUNTO7_2,
            verbose_name="En que parte",
            blank=True, null=True)
    meses = MultiSelectField(choices=CHOICES_FECHA_PODA,
            verbose_name='En qué meses vamos a realizar el manejo')

    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 8"

    class Meta:
        verbose_name_plural = '10. ¿Qué  acciones vamos a realizar para mejorar el manejo de piso de los cacaotales'

class PisoPunto10(models.Model):
    equipo = MultiSelectField(choices=CHOICE_PISO10,
            verbose_name='11.¿Tenemos los equipos necesarios para realizar manejo de piso?')
    formacion = models.IntegerField(choices=CHOICE_SI_NO,
            verbose_name="12.¿Tenemos la formación para realizar el manejo de piso?",
            blank=True, null=True)


    ficha = models.ForeignKey(FichaPisoPlaga)

    def __unicode__(self):
        return u"punto 11 y 12"

    class Meta:
        verbose_name_plural = 'Preguntas'

# -------- fin de piso y plagas---------------------------

# -------- inicio Cosecha ---------------------------------

class FichaCosecha(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='ficha_cosecha_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='ficha_cosecha_tecnico')

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self):
        self.year = self.fecha_visita.year
        super(FichaCosecha, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha Cosecha primaria"
        verbose_name_plural = "Ficha Cosecha primaria"

class CosechaConversacion1(models.Model):
    conversacion1 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_1,
                                verbose_name='1.1-¿Cómo se determina qué la mazorca está madura para cosecha? ')
    conversacion2 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_2,
                                verbose_name='1.2-¿Qué herramientas utiliza para el corte de las mazorcas maduras? ')
    conversacion3 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_3,
                                verbose_name='1.3-¿Qué criterios toma en cuenta para la selección de mazorcas antes del quiebre? ')
    conversacion4 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_4,
                                verbose_name='1.4-¿Qué herramientas utiliza para el quiebre de las mazorcas seleccionadas? ')

    ficha = models.ForeignKey(FichaCosecha)

    def __unicode__(self):
        return u"Conversación con la productora o el productor 1"

class CosechaConversacion2(models.Model):
    conversacion5 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_5,
                                verbose_name='1.5-¿Qué tipo de almacenamiento emplea después del quiebre de las mazorcas de cacao?  ')
    conversacion6 = models.FloatField('1.6-¿Cuánto tiempo tarda en llevar el cacao en baba al centro de acopio?')
    conversacion7 = MultiSelectField(choices=CHOICE_COSECHA_CONVERSACION_7,
                                verbose_name='1.7-¿Qué manejo realiza con las mazorcas de cacao enfermas? ')
    conversacion8 = models.IntegerField(choices=CHOICE_COSECHA_CONVERSACION_8,
                                verbose_name='1.8-¿Cada cuánto realizan los cortes? ')

    ficha = models.ForeignKey(FichaCosecha)

    def __unicode__(self):
        return u"Conversación con la productora o el productor 2"

class CosechaMesesFloracion(models.Model):
    mes = models.IntegerField(choices=CHOICE_COSECHA_9_MESES,
                                verbose_name='Meses')
    floracion = models.IntegerField(choices=CHOICE_COSECHA_9_FLORACION,
                                verbose_name='Floración')

    ficha = models.ForeignKey(FichaCosecha)

    def __unicode__(self):
        return u"¿Cuáles son las meses de mayor floración? "

class CosechaMesesCosecha(models.Model):
    mes = models.IntegerField(choices=CHOICE_COSECHA_9_MESES,
                                verbose_name='Meses')
    floracion = models.IntegerField(choices=CHOICE_COSECHA_10_COSECHA,
                                verbose_name='Cosecha')

    ficha = models.ForeignKey(FichaCosecha)

    def __unicode__(self):
        return u"¿Cuáles son las meses de mayor floración? "

class CosechaPunto1(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosecha)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaPunto1, self).save(*args, **kwargs)


    def __unicode__(self):
        return u"2.1 Punto 1"

class CosechaPunto2(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosecha)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaPunto2, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"2.2 Punto 2"

class CosechaPunto3(models.Model):
    mazorcas = models.IntegerField(choices=CHOICE_COSECHA_ESTIMADO_PUNTOS,
                                verbose_name='Mazorcas')
    planta_1 = models.FloatField()
    planta_2 = models.FloatField()
    planta_3 = models.FloatField()
    planta_4 = models.FloatField()
    planta_5 = models.FloatField()
    planta_6 = models.FloatField()
    planta_7 = models.FloatField()
    planta_8 = models.FloatField()
    planta_9 = models.FloatField()
    planta_10 = models.FloatField()

    total_platas = models.FloatField(editable=False, null=True, blank=True)
    contador = models.IntegerField(editable=False, default=0, null=True, blank=True)

    ficha = models.ForeignKey(FichaCosecha)

    def save(self, *args, **kwargs):
        self.total_platas = self.planta_1 + self.planta_2 + self.planta_3 + self.planta_4 + \
                                        self.planta_5 + self.planta_6 + self.planta_7 + self.planta_8 + self.planta_9 + self.planta_10
        contar = 0
        if self.planta_1 >= 0:
            contar += 1
        if self.planta_2 >= 0:
            contar += 1
        if self.planta_3 >= 0:
            contar += 1
        if self.planta_4 >= 0:
            contar += 1
        if self.planta_5 >= 0:
            contar += 1
        if self.planta_6 >= 0:
            contar += 1
        if self.planta_7 >= 0:
            contar += 1
        if self.planta_8 >= 0:
            contar += 1
        if self.planta_9 >= 0:
            contar += 1
        if self.planta_10 >= 0:
            contar += 1

        self.contador = contar

        super(CosechaPunto3, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"2.3 Punto 3"

class CosechaAreaPlantas(models.Model):
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    plantas = models.FloatField('Número de plantas existen en la parcela')

    ficha = models.ForeignKey(FichaCosecha)
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1.0
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        super(CosechaAreaPlantas, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Area y número de platas"

class CosechaAnalisis(models.Model):
    analisis1 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_1,
                                verbose_name='3.1-¿Cuál es el problema principal que afecta el rendimiento productivo de la parcela de cacao?')
    analisis2 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_2,
                                verbose_name='3.2-¿Cuál es la causa de la pérdida de producción en la parcela de cacao?  ')
    analisis3 = MultiSelectField(choices=CHOICE_COSECHA_ANALISIS_3,
                                verbose_name='3.3-¿Qué prácticas se pueden realizar en la parcela de cacao para mejorar la cosecha?  ')

    ficha = models.ForeignKey(FichaCosecha)

    def __unicode__(self):
        return u"Análisis sobre la cosecha y acciones"

#---------- fin de cosecha primaria ------------------

#----------- inicio de cierre de ciclo ------------------
class FichaCierre(models.Model):
    ciclo = models.ForeignKey(Ciclo, verbose_name='Ciclo de inicio')
    anio = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    productor = models.ForeignKey(
        Entrevistados,
        verbose_name='Nombre de productor o productora',
        related_name='ficha_cierre_productor')
    fecha_visita = models.DateField()
    tecnico = models.ForeignKey(
        Encuestadores,
        verbose_name='Nombre de técnico',
        related_name='ficha_cierre_tecnico')

    #campos ocultos para querys
    year = models.IntegerField(editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self):
        self.year = self.fecha_visita.year
        super(FichaCierre, self).save()

    def __unicode__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = "Ficha Cierre de ciclo"
        verbose_name_plural = "Ficha Cierre de ciclo"

class CierreManejo1(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_1_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_1_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_1_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_1_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.1"

    class Meta:
        verbose_name='1.1 Sombra'
        verbose_name_plural='1.1 Sombra'

class CierreManejo2(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_2_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_2_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_2_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_2_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.2"

    class Meta:
        verbose_name='1.2 Poda'
        verbose_name_plural='1.2 Poda'

class CierreManejo3(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_3_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_3_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_3_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_3_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.3"

    class Meta:
        verbose_name='1.3 Suelo'
        verbose_name_plural='1.3 Suelo'

class CierreManejo4(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_4_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_4_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_4_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_4_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.4"

    class Meta:
        verbose_name='1.4 Plaga'
        verbose_name_plural='1.4 Plaga'

class CierreManejo5(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_5_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_5_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_5_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_5_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.5"

    class Meta:
        verbose_name='1.5 Piso'
        verbose_name_plural='1.5 Piso'

class CierreManejo6(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_6_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_6_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_6_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_6_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.6"

    class Meta:
        verbose_name='1.6 Vivero'
        verbose_name_plural='1.6 Vivero'

class CierreManejo7(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_7_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_7_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_7_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_7_RESULTADO,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"1.7"

    class Meta:
        verbose_name='1.7 Cosecha'
        verbose_name_plural='1.7 Cosecha'

class CierreCosto1(models.Model):
    area = models.FloatField('Área de parcela SAF cacao')
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA)
    tipo = MultiSelectField(choices=CHOICE_CIERRE_COSTO_1,
                                verbose_name='Tipo de Cacao ')
    costo = models.FloatField('Costo de mano de obra por día')
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre)
    area_ha = models.FloatField(editable=False, null=True, blank=True)
    costo_us = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.costo_us = convert_money(Money(self.costo, str(self.get_moneda_display())), 'USD')
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1.0
        if self.unidad == 3:
            self.area_ha = self.area / 16.0
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000
        if self.unidad == 5:
            self.area_ha = self.area / 24.0

        super(CierreCosto1, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"2"

class CostoCacaoCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoCacaoCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de cacao'

class CostoMusaceasCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMusaceasCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de musaceas'

class CostoFrutalesCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoFrutalesCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de frutales'

class CostoMaderablesCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMaderablesCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de maderables'

class CostoArbolesCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoArbolesCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de árboles servicios'

class CostoCafeCierre(models.Model):
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CAFEL_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoCafeCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de Café'

class DatosCosechaCierre(models.Model):
    fecha = models.DateField()
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    comprador = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)

    cantidad_g = models.FloatField(editable=False, null=True, blank=True)
    precio_usd_g = models.FloatField(editable=False, null=True, blank=True)
    ficha = models.ForeignKey(FichaCierre,
                                                    on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        #musaceas-banano id(4) unidad cabeza(2)  1 cabeza= 6 kg banano
        if self.productos == 4 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 6
            self.precio_usd_g = float(self.precio_usd) / 6.0
        #musaceas-platano id(5) unidad cabeza(2)  1 Cabeza = 5.2 kg de plátano
        if self.productos == 5 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 5.2
            self.precio_usd_g = float(self.precio_usd) / 5.2
        #madera-caoba id(10) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 10 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-cortez id(11) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 11 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-roble id(12) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 12 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-melina id(13) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 13 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-granadillo id(14) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 14 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-leña id(14) unidad Carga (11) Conversión 1 carga =*0.1 m3
        if self.productos == 15 and self.unidad == 11:
            self.cantidad_g = self.cantidad * 0.1
            self.precio_usd_g = float(self.precio_usd) / 0.1

        super(DatosCosechaCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha'

class CicloTrabajoCierre(models.Model):
    pregunta1 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las visitas que hemos realizados han servido para aprender nuevas cosas? ')
    pregunta2 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las visitas que hemos realizados han servido para observar sobre diferentes aspectos de la parcela de cacao? ')
    pregunta3 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las observaciones y discusiones han servido para mejorar el manejo de las parcela de cacao?')
    pregunta4 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA,
                        verbose_name='¿Han podido implementar las acciones que se acordaron a partir de las visitas?')
    pregunta5 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA,
                        verbose_name='¿Qué piensa sobre la frecuencia de las visitas?')
    pregunta6 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA,
                        verbose_name='¿Qué piensa sobre el tiempo que dura cada visita?')
    pregunta7 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA,
                        verbose_name='¿Quiere seguir trabajando con las visitas para el segundo ciclo?')
    pregunta8 = models.IntegerField(choices=((1,'Si'),(2,'No'),),
                        verbose_name='Estaría usted interesado organizar un día de campo en su finca para que otras y otros productores vengan a visitar la parcela?')

    ficha = models.ForeignKey(FichaCierre)

    def __unicode__(self):
        return u"Cierre ciclo de trabajo"
