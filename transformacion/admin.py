# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

class Cobertura1Inline(admin.TabularInline):
    model = Cobertura1
    extra = 1
    max_num = 1

class Punto1Inline(admin.TabularInline):
    model = Punto1
    extra = 1
    #max_num = 1

class Cobertura2Inline(admin.TabularInline):
    model = Cobertura2
    extra = 1
    max_num = 1

class Punto2Inline(admin.TabularInline):
    model = Punto2
    extra = 1
    #max_num = 1

class Cobertura3Inline(admin.TabularInline):
    model = Cobertura3
    extra = 1
    max_num = 1

class Punto3Inline(admin.TabularInline):
    model = Punto3
    extra = 1
    #max_num = 1

class PodaPunto1Inline(admin.TabularInline):
    model = PodaPunto1
    extra = 1
    max_num = 3

class PodaPunto1BInline(admin.TabularInline):
    model = PodaPunto1B
    extra = 1
    max_num = 6

class PodaPunto1CInline(admin.TabularInline):
    model = PodaPunto1C
    extra = 1
    max_num = 1

class PodaPunto2Inline(admin.TabularInline):
    model = PodaPunto2
    extra = 1
    max_num = 3

class PodaPunto2BCInline(admin.TabularInline):
    model = PodaPunto2B
    extra = 1
    max_num = 6

class PodaPunto2CInline(admin.TabularInline):
    model = PodaPunto2C
    extra = 1
    max_num = 1

class PodaPunto3Inline(admin.TabularInline):
    model = PodaPunto3
    extra = 1
    max_num = 3

class PodaPunto3BCInline(admin.TabularInline):
    model = PodaPunto3B
    extra = 1
    max_num = 6

class PodaPunto3CInline(admin.TabularInline):
    model = PodaPunto3C
    extra = 1
    max_num = 1

class AnalisisSombraInline(admin.TabularInline):
    model = AnalisisSombra
    extra = 1
    max_num = 1

class AccionesSombraInline(admin.TabularInline):
    model = AccionesSombra
    extra = 1
    max_num = 1

class ReducirSombraInline(admin.TabularInline):
    model = ReducirSombra
    extra = 1
    max_num = 1

class AumentarSombraInline(admin.TabularInline):
    model = AumentarSombra
    extra = 1
    max_num = 1

class ManejoSombraInline(admin.TabularInline):
    model = ManejoSombra
    extra = 1
    max_num = 1

class AnalisisPodaInline(admin.TabularInline):
    model = AnalisisPoda
    extra = 1
    max_num = 1

class ManejoPodaInline(admin.TabularInline):
    model = ManejoPoda
    extra = 1
    max_num = 1

class FichaPodaSombraAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaPodaSombraAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaPodaSombraAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio',)
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [Cobertura1Inline,Punto1Inline,Cobertura2Inline,
                     Punto2Inline,Cobertura3Inline,Punto3Inline,
                     PodaPunto1Inline,PodaPunto1BInline,PodaPunto1CInline,
                     PodaPunto2Inline,PodaPunto2BCInline,PodaPunto2CInline,
                     PodaPunto3Inline,PodaPunto3BCInline,PodaPunto3CInline,
                     AnalisisSombraInline,AccionesSombraInline,
                     ReducirSombraInline,AumentarSombraInline,
                     ManejoSombraInline,AnalisisPodaInline,ManejoPodaInline]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo.nombre
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo__nombre'

    class Media:
        css = {
                    'all': ('css/ficha_sombra.css',)
        }

class Punto1SueloInline(admin.TabularInline):
    model = Punto1Suelo
    extra = 1
    max_num = 1

class PuntoASueloInline(admin.TabularInline):
    model = PuntoASuelo
    extra = 1
    max_num = 6

class PuntoBSueloInline(admin.TabularInline):
    model = PuntoBSuelo
    extra = 1
    max_num = 5

class Punto2ASueloInline(admin.TabularInline):
    model = Punto2ASuelo
    extra = 1
    max_num = 3

class Punto2BSueloInline(admin.TabularInline):
    model = Punto2BSuelo
    extra = 1
    max_num = 5

class Punto3SueloPunto1Inline(admin.TabularInline):
    model = Punto3SueloPunto1
    extra = 1
    max_num = 2

class Punto3SueloPunto2Inline(admin.TabularInline):
    model = Punto3SueloPunto2
    extra = 1
    max_num = 2

class Punto3SueloPunto3Inline(admin.TabularInline):
    model = Punto3SueloPunto3
    extra = 1
    max_num = 2

class Punto4SueloInline(admin.TabularInline):
    model = Punto4Suelo
    extra = 1
    max_num = 1

class Punto4SueloCosechaInline(admin.TabularInline):
    model = Punto4SueloCosecha
    extra = 1
    max_num = 3

class Punto4SueloSIInline(admin.TabularInline):
    model = Punto4SueloSI
    extra = 1
    max_num = 1

# class TipoFertilizantesInline(admin.TabularInline):
#     model = TipoFertilizantes
#     extra = 1
#     max_num = 1

class Punto5SueloAbonosInline(admin.TabularInline):
    model = Punto5SueloAbonos
    extra = 1
    #max_num = 1

class Punto6AnalisisSueloInline(admin.TabularInline):
    model = Punto6AnalisisSuelo
    extra = 1
    max_num = 9

class Punto7TipoSueloInline(admin.TabularInline):
    model = Punto7TipoSuelo
    extra = 1
    max_num = 1

class Punto8SueloPropuestaInline(admin.TabularInline):
    model = Punto8SueloPropuesta
    extra = 1
    #max_num = 1

class Punto9ErosionInline(admin.TabularInline):
    model = Punto9Erosion
    extra = 1
    max_num = 1

class Punto9DrenajeInline(admin.TabularInline):
    model = Punto9Drenaje
    extra = 1
    max_num = 1

class Punto9NutrientesInline(admin.TabularInline):
    model = Punto9Nutrientes
    extra = 1
    max_num = 1

class Punto9ExcesoInline(admin.TabularInline):
    model = Punto9Exceso
    extra = 1
    max_num = 1

class Punto9DesbalanceInline(admin.TabularInline):
    model = Punto9Desbalance
    extra = 1
    max_num = 1

class Punto9EnfermedadesInline(admin.TabularInline):
    model = Punto9Enfermedades
    extra = 1
    max_num = 1

class FichaSueloFertilidadAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaSueloFertilidadAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaSueloFertilidadAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio',)
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [Punto1SueloInline,PuntoASueloInline,PuntoBSueloInline,
                     Punto2ASueloInline,Punto2BSueloInline,Punto3SueloPunto1Inline,
                     Punto3SueloPunto2Inline,Punto3SueloPunto3Inline,
                     Punto4SueloInline,Punto4SueloCosechaInline,Punto4SueloSIInline,
                     Punto5SueloAbonosInline,Punto6AnalisisSueloInline,
                     Punto7TipoSueloInline,Punto8SueloPropuestaInline,Punto9ErosionInline,
                     Punto9DrenajeInline,Punto9NutrientesInline,Punto9ExcesoInline,
                     Punto9DesbalanceInline,Punto9EnfermedadesInline]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo.nombre
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo__nombre'

class CosechaSecundariaPunto1Inline(admin.TabularInline):
    model = CosechaSecundariaPunto1
    extra = 1
    max_num = 3

class CosechaSecundariaPunto2Inline(admin.TabularInline):
    model = CosechaSecundariaPunto2
    extra = 1
    max_num = 3

class CosechaSecundariaPunto3Inline(admin.TabularInline):
    model = CosechaSecundariaPunto3
    extra = 1
    max_num = 3

class CosechaSecundariaAreaPlantasInline(admin.TabularInline):
    model = CosechaSecundariaAreaPlantas
    extra = 1
    max_num = 1

class CosechaSecundariaAnalisisInline(admin.TabularInline):
    model = CosechaSecundariaAnalisis
    extra = 1
    max_num = 1

class InventarioParcelaInline(admin.TabularInline):
    model = InventarioParcela
    extra = 1

class FrutalesMusaceasPunto1Inline(admin.TabularInline):
    model = FrutalesMusaceasPunto1
    extra = 1

class MaderablesServicioPunto1Inline(admin.TabularInline):
    model = MaderablesServicioPunto1
    extra = 1

class FrutalesMusaceasPunto2Inline(admin.TabularInline):
    model = FrutalesMusaceasPunto2
    extra = 1

class MaderablesServicioPunto2Inline(admin.TabularInline):
    model = MaderablesServicioPunto2
    extra = 1

class FrutalesMusaceasPunto3Inline(admin.TabularInline):
    model = FrutalesMusaceasPunto3
    extra = 1

class MaderablesServicioPunto3Inline(admin.TabularInline):
    model = MaderablesServicioPunto3
    extra = 1

class FichaCosechaSecundariaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaCosechaSecundariaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaCosechaSecundariaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio')
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [InventarioParcelaInline,CosechaSecundariaPunto1Inline,
                     FrutalesMusaceasPunto1Inline,MaderablesServicioPunto1Inline,
                     CosechaSecundariaPunto2Inline,
                     FrutalesMusaceasPunto2Inline,MaderablesServicioPunto2Inline,
                     CosechaSecundariaPunto3Inline,
                     FrutalesMusaceasPunto3Inline,MaderablesServicioPunto3Inline,
                     CosechaSecundariaAreaPlantasInline,
                     CosechaSecundariaAnalisisInline]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo'

    class Media:
        css = {
                    'all': ('css/ficha_cosecha_secundaria.css',)
        }

    def get_organizacion_pertenece(self, obj):
        return obj.productor.organizacion_pertenece
    get_organizacion_pertenece.short_description = 'Organizacion pertenece'
    get_organizacion_pertenece.admin_order_field = 'productor__organizacion_pertenece'

class PlagasEnfermedadVistoInline(admin.TabularInline):
    model = PlagasEnfermedadVisto
    extra = 1
    max_num = 1

class PlagasEnfermedadAfectanInline(admin.TabularInline):
    model = PlagasEnfermedadAfectan
    extra = 1
    #max_num = 1

class AccionesEnfermedadInline(admin.TabularInline):
    model = AccionesEnfermedad
    extra = 1
    #max_num = 1

class PisoPunto1Inline(admin.TabularInline):
    model = PisoPunto1
    extra = 1
    max_num = 1

class PisoPunto3Inline(admin.TabularInline):
    model = PisoPunto3
    extra = 1
    #max_num = 1

class PisoPunto4Inline(admin.TabularInline):
    model = PisoPunto4
    extra = 1
    max_num = 1

class ObservacionPunto1Inline(admin.TabularInline):
    model = ObservacionPunto1
    extra = 1
    #max_num = 1

class ObservacionPunto1NivelInline(admin.TabularInline):
    model = ObservacionPunto1Nivel
    extra = 1
    max_num = 1

class ObservacionPunto2Inline(admin.TabularInline):
    model = ObservacionPunto2
    extra = 1
    #max_num = 1

class ObservacionPunto2NivelInline(admin.TabularInline):
    model = ObservacionPunto2Nivel
    extra = 1
    max_num = 1

class ObservacionPunto3Inline(admin.TabularInline):
    model = ObservacionPunto3
    extra = 1
    #max_num = 1

class ObservacionPunto3NivelInline(admin.TabularInline):
    model = ObservacionPunto3Nivel
    extra = 1
    max_num = 1

class PisoPunto5Inline(admin.TabularInline):
    model = PisoPunto5
    extra = 1
    #max_num = 1

class ProblemasPrincipalesInline(admin.TabularInline):
    model = ProblemasPrincipales
    extra = 1
    max_num = 1

class Punto6PlagasInline(admin.TabularInline):
    model = Punto6Plagas
    extra = 1
    max_num = 1

class Punto7PlagasInline(admin.TabularInline):
    model = Punto7Plagas
    extra = 1
    #max_num = 1

class Punto8y9PlagasInline(admin.TabularInline):
    model = Punto8y9Plagas
    extra = 1
    max_num = 1

class PisoPunto6Inline(admin.TabularInline):
    model = PisoPunto6
    extra = 1
    max_num = 1

class PisoPunto7Inline(admin.TabularInline):
    model = PisoPunto7
    extra = 1
    max_num = 1

class PisoPunto8Inline(admin.TabularInline):
    model = PisoPunto8
    extra = 1
    #max_num = 1

class PisoPunto10Inline(admin.TabularInline):
    model = PisoPunto10
    extra = 1
    max_num = 1


class EspeciesCosechaSecundariaAdmin(admin.ModelAdmin):
    list_display = ('nombre','tipo','unidad')
    list_filter = ['tipo']

admin.site.register(EspeciesCosechaSecundaria, EspeciesCosechaSecundariaAdmin)

class FichaPisoPlagaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaPisoPlagaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaPisoPlagaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio',)
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [PlagasEnfermedadVistoInline,PlagasEnfermedadAfectanInline,
                     AccionesEnfermedadInline,PisoPunto1Inline,PisoPunto3Inline,
                     PisoPunto4Inline,ObservacionPunto1Inline,ObservacionPunto1NivelInline,
                     ObservacionPunto2Inline,ObservacionPunto2NivelInline,
                     ObservacionPunto3Inline,ObservacionPunto3NivelInline,
                     PisoPunto5Inline,ProblemasPrincipalesInline,Punto6PlagasInline,
                     Punto7PlagasInline,Punto8y9PlagasInline,PisoPunto6Inline,
                     PisoPunto7Inline,PisoPunto8Inline,PisoPunto10Inline,
                     ]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo'

class CosechaConversacion1Inline(admin.TabularInline):
    model = CosechaConversacion1
    extra = 1
    max_num = 1

class CosechaConversacion2Inline(admin.TabularInline):
    model = CosechaConversacion2
    extra = 1
    max_num = 1

class CosechaMesesFloracionInline(admin.TabularInline):
    model = CosechaMesesFloracion
    extra = 1
    max_num = 12

class CosechaMesesCosechaInline(admin.TabularInline):
    model = CosechaMesesCosecha
    extra = 1
    max_num = 12

class CosechaPunto1Inline(admin.TabularInline):
    model = CosechaPunto1
    extra = 1
    max_num = 3

class CosechaPunto2Inline(admin.TabularInline):
    model = CosechaPunto2
    extra = 1
    max_num = 3

class CosechaPunto3Inline(admin.TabularInline):
    model = CosechaPunto3
    extra = 1
    max_num = 3

class CosechaAreaPlantasInline(admin.TabularInline):
    model = CosechaAreaPlantas
    extra = 1
    max_num = 1

class CosechaAnalisisInline(admin.TabularInline):
    model = CosechaAnalisis
    extra = 1
    max_num = 1

class FichaCosechaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaCosechaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaCosechaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio',)
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [CosechaConversacion1Inline,CosechaConversacion2Inline,
                     CosechaMesesFloracionInline,CosechaMesesCosechaInline,
                     CosechaPunto1Inline,CosechaPunto2Inline,CosechaPunto3Inline,
                     CosechaAreaPlantasInline,CosechaAnalisisInline
                     ]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo'

    class Media:
        css = {
                    'all': ('css/ficha_cosecha_secundaria.css',)
        }

class CierreManejo1Inlines(admin.TabularInline):
    model = CierreManejo1
    extra = 1
    max_num = 1

class CierreManejo2Inlines(admin.TabularInline):
    model = CierreManejo2
    extra = 1
    max_num = 1

class CierreManejo3Inlines(admin.TabularInline):
    model = CierreManejo3
    extra = 1
    max_num = 1

class CierreManejo4Inlines(admin.TabularInline):
    model = CierreManejo4
    extra = 1
    max_num = 1

class CierreManejo5Inlines(admin.TabularInline):
    model = CierreManejo5
    extra = 1
    max_num = 1

class CierreManejo6Inlines(admin.TabularInline):
    model = CierreManejo6
    extra = 1
    max_num = 1

class CierreManejo7Inlines(admin.TabularInline):
    model = CierreManejo7
    extra = 1
    max_num = 1

class CierreCosto1Inlines(admin.TabularInline):
    model = CierreCosto1
    extra = 1
    max_num = 1

class CostoCacaoCierreInlines(admin.TabularInline):
    model = CostoCacaoCierre
    extra = 1

class CostoMusaceasCierreInlines(admin.TabularInline):
    model = CostoMusaceasCierre
    extra = 1

class CostoFrutalesCierreInlines(admin.TabularInline):
    model = CostoFrutalesCierre
    extra = 1

class CostoMaderablesCierreInlines(admin.TabularInline):
    model = CostoMaderablesCierre
    extra = 1

class CostoArbolesCierreInlines(admin.TabularInline):
    model = CostoArbolesCierre
    extra = 1

class CostoCafeCierreInlines(admin.TabularInline):
    model = CostoCafeCierre
    extra = 1

class DatosCosechaCierreInlines(admin.TabularInline):
    model = DatosCosechaCierre
    extra = 1

class CicloTrabajoCierreInlines(admin.TabularInline):
    model = CicloTrabajoCierre
    extra = 1
    max_num = 1

class FichaCierreAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaCierreAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaCierreAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('productor','get_org_apoyo','ciclo','anio',)
    list_filter = ['productor__pais']
    search_fields = ('productor__nombre',)
    inlines = [CierreManejo1Inlines,CierreManejo2Inlines,
                     CierreManejo3Inlines,CierreManejo4Inlines,
                     CierreManejo5Inlines,CierreManejo6Inlines,
                     CierreManejo7Inlines,CierreCosto1Inlines,
                     CostoCacaoCierreInlines,CostoMusaceasCierreInlines,
                     CostoFrutalesCierreInlines,CostoMaderablesCierreInlines,
                     CostoArbolesCierreInlines,CostoCafeCierreInlines,
                     DatosCosechaCierreInlines,CicloTrabajoCierreInlines
                     ]

    def get_org_apoyo(self, obj):
        return obj.productor.organizacion_apoyo
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'productor__organizacion_apoyo'

# Register your models here.
admin.site.register(FichaPodaSombra,FichaPodaSombraAdmin)
admin.site.register(FichaSueloFertilidad,FichaSueloFertilidadAdmin)
admin.site.register(TipoFertilizantes)
admin.site.register(FichaCosechaSecundaria,FichaCosechaSecundariaAdmin)
admin.site.register(FichaPisoPlaga,FichaPisoPlagaAdmin)
admin.site.register(FichaCosecha,FichaCosechaAdmin)
admin.site.register(FichaCierre,FichaCierreAdmin)
