(function($) {

 $(document).ready(function() {

    $( "#id_year" ).on('change', function() {
        var selectYear = $('#id_year').val();
        console.log(selectYear);
         if(selectYear == '15' || selectYear =='16' || selectYear == '17' ){
               $('#cacao_set-group').hide()
               $('#musaceas_set-group').hide()
               $('#frutales_set-group').hide()
               $('#maderables_set-group').hide()
               $('#sombratemporal_set-group').hide()
               $('#datossuelo_set-group').hide()
               $('#analisissuelo_set-group').hide()
               $('#tipotexturasuelo_set-group').hide()
               $('#datospiso1_set-group').show()
               $('#datospiso2_set-group').show()
               $('#datospiso3_set-group').show()
               $('#accionesmanejopiso_set-group').show()
            }else{
                $('#cacao_set-group').show()
                $('#musaceas_set-group').show()
                $('#frutales_set-group').show()
                $('#maderables_set-group').show()
                $('#sombratemporal_set-group').show()
                $('#datossuelo_set-group').show()
                $('#analisissuelo_set-group').show()
                $('#tipotexturasuelo_set-group').show()
                $('#datospiso1_set-group').hide()
                $('#datospiso2_set-group').hide()
                $('#datospiso3_set-group').hide()
                $('#accionesmanejopiso_set-group').hide()
            }
        });

});

})(jQuery || django.jQuery);
