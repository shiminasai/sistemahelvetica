# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2018-12-26 20:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('lineabase', '0001_initial'),
        ('cuadernosmes0', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalisisSuelo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.FloatField()),
            ],
            options={
                'verbose_name_plural': 'An\xe1lisis de suelo',
            },
        ),
        migrations.CreateModel(
            name='Cacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_sembrada', models.FloatField()),
                ('cantidad_establecida', models.FloatField()),
                ('cantidad_resiembra', models.FloatField()),
                ('altura_plantas', models.FloatField(verbose_name='Alturas de las plantas mt')),
                ('estado_planta', models.IntegerField(choices=[(1, b'Bueno'), (2, b'Regular'), (3, b'Malo')])),
                ('clones_variedad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.ClonesVariedades')),
            ],
            options={
                'verbose_name_plural': 'Cacao',
            },
        ),
        migrations.CreateModel(
            name='CostoCacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('actividades', models.IntegerField(choices=[(1, b'Siembra/resiembra'), (2, b'Poda de cacao'), (3, b'Deschuponado'), (4, b'Poda de \xc3\xa1rboles'), (5, b'Aplicaci\xc3\xb3n de abono'), (6, b'Aplicaci\xc3\xb3n de insecticida'), (7, b'Aplicaci\xc3\xb3n de fungicida'), (8, b'Manejo de piso'), (9, b'Aplicaci\xc3\xb3n de riego'), (10, b'Obras de drenaje'), (11, b'Obras de conservaci\xc3\xb3n'), (12, b'Cosecha y Corte'), (13, b'Transporte de cosecha')])),
                ('mo_familiar', models.FloatField(verbose_name='Uso de MO familiar DP')),
                ('mo_contratada', models.FloatField(verbose_name='Uso de MO contratada DP')),
                ('insumos', models.FloatField()),
                ('costo_insumo', models.FloatField(verbose_name='Costo insumos')),
                ('unidad', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
            ],
            options={
                'verbose_name_plural': 'Datos costos de cacao',
            },
        ),
        migrations.CreateModel(
            name='CostoFrutales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('actividades', models.IntegerField(choices=[(1, b'Siembra/resiembra'), (2, b'Poda de frutales'), (3, b'Aplicaci\xc3\xb3n de abono'), (4, b'Aplicaci\xc3\xb3n de insecticida'), (5, b'Aplicaci\xc3\xb3n de fungicida'), (6, b'Cosecha y Corte'), (7, b'Transporte de cosecha')])),
                ('mo_familiar', models.FloatField(verbose_name='Uso de MO familiar')),
                ('mo_contratada', models.FloatField(verbose_name='Uso de MO contratada')),
                ('insumos', models.FloatField()),
                ('costo_insumo', models.FloatField(verbose_name='Costo insumos')),
                ('unidad', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
            ],
            options={
                'verbose_name_plural': 'Datos costos de frutales',
            },
        ),
        migrations.CreateModel(
            name='CostoMaderables',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('actividades', models.IntegerField(choices=[(1, b'Siembra/resiembra'), (2, b'Poda de maderables'), (3, b'Aplicaci\xc3\xb3n de abono'), (4, b'Aplicaci\xc3\xb3n de insecticida'), (5, b'Aplicaci\xc3\xb3n de fungicida'), (6, b'Cosecha y Corte'), (7, b'Transporte de cosecha')])),
                ('mo_familiar', models.FloatField(verbose_name='Uso de MO familiar')),
                ('mo_contratada', models.FloatField(verbose_name='Uso de MO contratada')),
                ('insumos', models.FloatField()),
                ('costo_insumo', models.FloatField(verbose_name='Costo insumos')),
                ('unidad', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
            ],
            options={
                'verbose_name_plural': 'Datos costos de maderables',
            },
        ),
        migrations.CreateModel(
            name='CostoMusaceas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('actividades', models.IntegerField(choices=[(1, b'Siembra/resiembra'), (2, b'Deshije y deshoja'), (3, b'Manejo de cabezas'), (4, b'Aplicaci\xc3\xb3n de abono'), (5, b'Aplicaci\xc3\xb3n de insecticida'), (6, b'Aplicaci\xc3\xb3n de fungicida'), (7, b'Cosecha y Corte'), (8, b'Transporte de cosecha')])),
                ('mo_familiar', models.FloatField(verbose_name='Uso de MO familiar')),
                ('mo_contratada', models.FloatField(verbose_name='Uso de MO contratada')),
                ('insumos', models.FloatField()),
                ('costo_insumo', models.FloatField(verbose_name='Costo insumos')),
                ('unidad', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
            ],
            options={
                'verbose_name_plural': 'Datos costos de musaceas',
            },
        ),
        migrations.CreateModel(
            name='CostoSombraTemporal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('actividades', models.IntegerField(choices=[(1, b'Siembra/resiembra'), (2, b'Cosecha y Corte'), (3, b'Transporte de cosecha')])),
                ('mo_familiar', models.FloatField(verbose_name='Uso de MO familiar')),
                ('mo_contratada', models.FloatField(verbose_name='Uso de MO contratada')),
                ('insumos', models.FloatField()),
                ('costo_insumo', models.FloatField(verbose_name='Costo insumos')),
                ('unidad', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
            ],
            options={
                'verbose_name_plural': 'Datos costos de sombra temporal',
            },
        ),
        migrations.CreateModel(
            name='CuadernoMesTres',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('encuestador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lineabase.Encuestadores', verbose_name='Nombre del t\xe9cnico')),
                ('entrevistado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lineabase.Entrevistados', verbose_name='Nombre productor/a')),
                ('organizacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lineabase.Organizacion', verbose_name='Nombre de la organizaci\xf3n')),
                ('year', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.Years', verbose_name='A\xf1o')),
            ],
            options={
                'verbose_name': 'Cuaderno Mes 3',
                'verbose_name_plural': 'Cuadernos de los meses 3',
            },
        ),
        migrations.CreateModel(
            name='DatosAnalisis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('variable', models.CharField(max_length=250)),
                ('unidad', models.CharField(blank=True, max_length=250, null=True)),
                ('valor_critico', models.FloatField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Datos an\xe1lisis',
            },
        ),
        migrations.CreateModel(
            name='DatosCosecha',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('productos', models.IntegerField(choices=[(1, b'Cacao en baba'), (2, b'Cosecha de granos'), (3, b'Musaceas-Banano'), (4, b'Musaceas-Pl\xc3\xa1tano'), (5, b'Frutales-Zapote'), (6, b'Frutales-C\xc3\xadtricos'), (7, b'Frutales-Aguacate'), (8, b'Madera-Caoba'), (9, b'Madera-Cortez'), (10, b'Madera-Roble'), (11, b'Madera-Melina'), (12, b'Madera-Granadillo')])),
                ('cantidad', models.FloatField()),
                ('unidad', models.IntegerField(choices=[(1, b'qq'), (2, b'Cabeza'), (3, b'Lb'), (4, b'Kg'), (5, b'Docena'), (6, b'Cien'), (7, b'Saco'), (8, b'M3'), (9, b'pt')])),
                ('precio', models.FloatField(verbose_name='Precio/Unidad')),
                ('moneda', models.IntegerField(choices=[(1, b'NIO'), (2, b'HNL'), (3, b'USD'), (4, b'DOP')])),
                ('comprador', multiselectfield.db.fields.MultiSelectField(choices=[(b'A', b'Intermediario'), (b'B', b'Cooperativa'), (b'C', b'Asociaci\xc3\xb3n'), (b'D', b'Empresa')], max_length=7, verbose_name='Comprador')),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
            ],
            options={
                'verbose_name_plural': 'Datos de cosecha',
            },
        ),
        migrations.CreateModel(
            name='DatosSuelo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uso_parcela', multiselectfield.db.fields.MultiSelectField(choices=[(b'A', b'Bosque'), (b'B', b'Potrero'), (b'C', b'Granos b\xc3\xa1sicos'), (b'D', b'Tacotal'), (b'E', b'Cacaotal viejo')], max_length=9, verbose_name='\xbfCu\xe1l era el uso de la parcela antes de establecer el cacao?')),
                ('limitantes', multiselectfield.db.fields.MultiSelectField(choices=[(b'A', b'Acidez / pH del suelo'), (b'B', b'Encharcamiento / Mal Drenaje'), (b'C', b'Enfermedades de ra\xc3\xadces'), (b'D', b'Deficiencia de nutrientes'), (b'E', b'Baja materia org\xc3\xa1nica'), (b'F', b'Baja actividad biol\xc3\xb3gica y presencia de lombrices'), (b'G', b'Erosi\xc3\xb3n de suelo'), (b'H', b'Compactaci\xc3\xb3n e baja infiltraci\xc3\xb3n de agua'), (b'I', b'Ning\xc3\xban limitante')], max_length=17, verbose_name='\xbfCu\xe1les son los limitantes productivos del suelo de la parcela?')),
                ('plano', models.FloatField(blank=True, null=True, verbose_name='% de area plano')),
                ('ondulado', models.FloatField(blank=True, null=True, verbose_name='% de area ondulado')),
                ('pendiente', models.FloatField(blank=True, null=True, verbose_name='% de area pendiente fuerte')),
                ('muy_baja', models.FloatField(blank=True, null=True, verbose_name='% infiltracion muy baja')),
                ('baja', models.FloatField(blank=True, null=True, verbose_name='% infiltracion baja')),
                ('regular', models.FloatField(blank=True, null=True, verbose_name='% infiltracion regular')),
                ('buena', models.FloatField(blank=True, null=True, verbose_name='% infiltracion buena')),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
            ],
            options={
                'verbose_name_plural': 'Datos del suelo',
            },
        ),
        migrations.CreateModel(
            name='Frutales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_sembrada', models.FloatField()),
                ('cantidad_establecida', models.FloatField()),
                ('cantidad_resiembra', models.FloatField()),
                ('altura_plantas', models.FloatField(verbose_name='Alturas de las plantas mt')),
                ('estado_planta', models.IntegerField(choices=[(1, b'Bueno'), (2, b'Regular'), (3, b'Malo')])),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
                ('especies', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.EspeciesFrutales')),
            ],
            options={
                'verbose_name_plural': 'Frutales',
            },
        ),
        migrations.CreateModel(
            name='Maderables',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_sembrada', models.FloatField()),
                ('cantidad_establecida', models.FloatField()),
                ('cantidad_resiembra', models.FloatField()),
                ('altura_plantas', models.FloatField(verbose_name='Alturas de las plantas mt')),
                ('estado_planta', models.IntegerField(choices=[(1, b'Bueno'), (2, b'Regular'), (3, b'Malo')])),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
                ('especies', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.EspeciesMaderables')),
            ],
            options={
                'verbose_name_plural': 'Maderables',
            },
        ),
        migrations.CreateModel(
            name='Musaceas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_sembrada', models.FloatField()),
                ('cantidad_establecida', models.FloatField()),
                ('cantidad_resiembra', models.FloatField()),
                ('altura_plantas', models.FloatField(verbose_name='Alturas de las plantas mt')),
                ('estado_planta', models.IntegerField(choices=[(1, b'Bueno'), (2, b'Regular'), (3, b'Malo')])),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
                ('tipo_musaceas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.TiposMusaceas')),
            ],
            options={
                'verbose_name_plural': 'Musaceas',
            },
        ),
        migrations.CreateModel(
            name='SombraTemporal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_sembrada', models.FloatField()),
                ('cantidad_establecida', models.FloatField()),
                ('cantidad_resiembra', models.FloatField()),
                ('altura_plantas', models.FloatField(verbose_name='Alturas de las plantas mt')),
                ('estado_planta', models.IntegerField(choices=[(1, b'Bueno'), (2, b'Regular'), (3, b'Malo')])),
                ('clones_variedad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes0.ClonesVariedades')),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
            ],
            options={
                'verbose_name_plural': 'Sombras temporales',
            },
        ),
        migrations.CreateModel(
            name='TipoTexturaSuelo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.IntegerField(choices=[(1, 'Ultisol (rojo)'), (2, 'Andisol (volc\xe1nico)'), (3, 'Vertisol')])),
                ('textura', models.IntegerField(choices=[(1, 'Arcilloso '), (2, 'Limoso'), (3, 'Arenoso '), (4, 'Arcilloso-Limoso  ')])),
                ('cuaderno_tres', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres')),
            ],
            options={
                'verbose_name_plural': 'Tipo y textura del suelo',
            },
        ),
        migrations.AddField(
            model_name='costosombratemporal',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='costomusaceas',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='costomaderables',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='costofrutales',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='costocacao',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='cacao',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='analisissuelo',
            name='cuaderno_tres',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.CuadernoMesTres'),
        ),
        migrations.AddField(
            model_name='analisissuelo',
            name='variable',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cuadernosmes3.DatosAnalisis'),
        ),
    ]
