# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from lineabase.models import (Entrevistados,
                                                         Encuestadores,
                                                         Organizacion)
from cuadernosmes0.choices_static import *
from cuadernosmes0.models import (ClonesVariedades,
                                                           EspeciesFrutales,
                                                           EspeciesMaderables,
                                                           TiposMusaceas,
                                                           EspeciesArbolesServicios,
                                                           Years,
                                                           Ciclo,)
from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money
# Create your models here.

@python_2_unicode_compatible
class CuadernoMesTres(models.Model):
    year = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    fecha = models.DateField()
    ciclo = models.ForeignKey(Ciclo, on_delete=models.CASCADE)
    entrevistado = models.ForeignKey(Entrevistados,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre productor/a')
    encuestador = models.ForeignKey(Encuestadores,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre del técnico')
    # organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE,
    #                                                                 verbose_name='Nombre de la organización',
    #                                                                 null=True, blank=True)

    years = models.IntegerField(editable=False, verbose_name='Año')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.years = self.fecha.year
        super(CuadernoMesTres, self).save(*args, **kwargs)

    def __str__(self):
        return self.entrevistado.nombre

    class Meta:
        verbose_name = 'Cuaderno Mes 3'
        verbose_name_plural = 'Cuadernos de los meses 3'

class Cacao(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                                                          on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    cantidad_establecida = models.FloatField()
    cantidad_resiembra = models.FloatField()
    altura_plantas = models.FloatField('Alturas de las plantas mt')
    estado_planta = models.IntegerField(choices=CHOICE_ESTADO_PLANTA)

    class Meta:
        verbose_name_plural = 'Cacao'

class Musaceas(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    tipo_musaceas = models.ForeignKey(TiposMusaceas,
                                                                          on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    cantidad_establecida = models.FloatField()
    cantidad_resiembra = models.FloatField()
    altura_plantas = models.FloatField('Alturas de las plantas mt')
    estado_planta = models.IntegerField(choices=CHOICE_ESTADO_PLANTA)

    class Meta:
        verbose_name_plural = 'Musaceas'

class Frutales(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                                            on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    cantidad_establecida = models.FloatField()
    cantidad_resiembra = models.FloatField()
    altura_plantas = models.FloatField('Alturas de las plantas mt')
    estado_planta = models.IntegerField(choices=CHOICE_ESTADO_PLANTA)

    class Meta:
        verbose_name_plural = 'Frutales'

class Maderables(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                                            on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    cantidad_establecida = models.FloatField()
    cantidad_resiembra = models.FloatField()
    altura_plantas = models.FloatField('Alturas de las plantas mt')
    estado_planta = models.IntegerField(choices=CHOICE_ESTADO_PLANTA)

    class Meta:
        verbose_name_plural = 'Maderables'

class SombraTemporal(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(EspeciesArbolesServicios,
                                                            on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    cantidad_establecida = models.FloatField()
    cantidad_resiembra = models.FloatField()
    altura_plantas = models.FloatField('Alturas de las plantas mt')
    estado_planta = models.IntegerField(choices=CHOICE_ESTADO_PLANTA)

    class Meta:
        verbose_name_plural = 'Arboles de servicios'

class DatosSuelo(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    uso_parcela = MultiSelectField('¿Cuál era el uso de la parcela antes de establecer el cacao?',
                            choices=CHOICE_USO_PARCELA)
    limitantes = MultiSelectField('¿Cuáles son los limitantes productivos del suelo de la parcela?',
                            choices=CHOICE_LIMITANTES_PRODUCTIVOS)
    plano = models.FloatField("% de area plano", null=True, blank=True)
    ondulado = models.FloatField("% de area ondulado", null=True, blank=True)
    pendiente = models.FloatField("% de area pendiente fuerte", null=True, blank=True)
    muy_baja = models.FloatField("% infiltracion muy baja", null=True, blank=True)
    baja = models.FloatField("% infiltracion baja", null=True, blank=True)
    regular = models.FloatField("% infiltracion regular", null=True, blank=True)
    buena = models.FloatField("% infiltracion buena", null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Datos del suelo'

@python_2_unicode_compatible
class DatosAnalisis(models.Model):
    variable = models.CharField(max_length=250)
    unidad = models.CharField(max_length=250, null=True, blank=True)
    valor_critico = models.FloatField(null=True, blank=True)

    def __str__(self):
        return self.variable

    class Meta:
        verbose_name_plural = "Datos análisis"

@python_2_unicode_compatible
class AnalisisSuelo(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    variable = models.ForeignKey(DatosAnalisis, on_delete=models.CASCADE)
    valor = models.FloatField()

    def __str__(self):
        return u"Análisis de suelo"

    class Meta:
        verbose_name_plural = 'Análisis de suelo'

@python_2_unicode_compatible
class TipoTexturaSuelo(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    tipo = models.IntegerField(choices=(
                                        (1,'Ultisol (rojo)'),
                                        (2, 'Andisol (volcánico)'),
                                        (3, 'Vertisol'),))
    textura = models.IntegerField(choices=(
                                        (1,'Arcilloso '),
                                        (2, 'Limoso'),
                                        (3, 'Arenoso '),
                                        (4, 'Arcilloso-Limoso  '),))

    def __str__(self):
        return u"Tipo y textura de suelo"

    class Meta:
        verbose_name_plural = 'Tipo y textura del suelo'

class PlanManejoFertilizante(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    tipo_fertilizacion = models.IntegerField(choices=CHOICE_TIPO_FERTILIZA)
    modo_fertilizacion = models.IntegerField(choices=CHOICE_MODO_FERTILIZA)
    intensidad_fertilizacion = models.IntegerField(choices=CHOICE_INTENSIDAD_FERTILIZA)
    numero_plantas = models.FloatField('Número de plantas')
    unidad = models.IntegerField(choices=CHOICE_MEDIDA_PLANTA)

    numero_planta_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.numero_planta_ha = self.numero_plantas * 1.4
        if self.unidad == 2:
            self.numero_planta_ha = self.numero_plantas
        if self.unidad == 3:
            self.numero_planta_ha = self.numero_plantas * 16
        if self.unidad == 4:
            self.numero_planta_ha = self.numero_plantas * 0.1859
        super(PlanManejoFertilizante, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Plan de manejo de fertilidad de suelo'

class ProductoSuelo(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Nombres de productos o formulas para suelo'

class AbonoAplicadoSuelo(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    producto = models.ForeignKey(ProductoSuelo, on_delete=models.CASCADE)
    dosis = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_SUELO_DOSIS)
    frecuencia_veces = models.FloatField('Frecuencia(cuántas veces al año)')

    dosis_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        child1 = PlanManejoFertilizante.objects.get(cuaderno_tres=self.cuaderno_tres)
        if self.unidad == 1:
            self.dosis_ha = (self.dosis * 1.4 * self.frecuencia_veces) / 20.0
        if self.unidad == 2:
            self.dosis_ha = (self.dosis * self.frecuencia_veces) / 20.0
        if self.unidad == 3:
            self.dosis_ha = (self.dosis * 16 * self.frecuencia_veces) / 20.0
        if self.unidad == 4:
            self.dosis_ha = (self.dosis * child1.numero_planta_ha * self.frecuencia_veces) / (100 * 20)
        if self.unidad == 5:
            self.dosis_ha = (self.dosis *child1.numero_planta_ha * self.frecuencia_veces) / (16 * 100 * 20)
        if self.unidad == 6:
            self.dosis_ha = (self.dosis *child1.numero_planta_ha * self.frecuencia_veces) / (0.1859 * 100 * 20)
        super(AbonoAplicadoSuelo, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Fertilizantes o Abono orgánico aplicados al suelo'

class ProductoFoliar(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Nombres de productos o formulas para foliar'

class AbonoAplicadoFoliar(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    producto = models.ForeignKey(ProductoFoliar, on_delete=models.CASCADE)
    dosis = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_FOLIAR_DOSIS)
    frecuencia_veces = models.FloatField('Frecuencia(cuántas veces al año)')

    dosis_lt_ha = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.unidad == 1:
            self.dosis_lt_ha = (self.dosis * 1.4) * self.frecuencia_veces
        if self.unidad == 2:
            self.dosis_lt_ha = self.dosis * self.frecuencia_veces
        if self.unidad == 3:
            self.dosis_lt_ha = (self.dosis * 16) * self.frecuencia_veces
        super(AbonoAplicadoFoliar, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Fertilizantes o Abono orgánico aplicados foliar'

class CostoManoObraDia(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    valor_mo_dia = models.FloatField('Valor de mano de obra por día')
    valor_mo_dia_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL,
                                                        verbose_name='Moneda')

    def save(self, *args, **kwargs):
        self.valor_mo_dia_usd = convert_money(Money(self.valor_mo_dia, str(self.get_unidad_display())), 'USD')
        super(CostoManoObraDia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Costo de mano de obra por dia"

class CostoCacao(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de cacao'

class CostoMusaceas(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMusaceas, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de musaceas'

class CostoFrutales(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoFrutales, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de frutales'

class CostoMaderables(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMaderables, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de maderables'

class CostoSombraTemporal(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoSombraTemporal, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de árboles servicios'

class DatosCosecha(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    comprador = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)

    cantidad_g = models.FloatField(editable=False, null=True, blank=True)
    precio_usd_g = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        #musaceas-banano id(4) unidad cabeza(2)  1 cabeza= 6 kg banano
        if self.productos == 4 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 6
            self.precio_usd_g = float(self.precio_usd) / 6.0
        #musaceas-platano id(5) unidad cabeza(2)  1 Cabeza = 5.2 kg de plátano
        if self.productos == 5 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 5.2
            self.precio_usd_g = float(self.precio_usd) / 5.2
        #madera-caoba id(10) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 10 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-cortez id(11) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 11 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-roble id(12) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 12 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-melina id(13) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 13 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-granadillo id(14) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 14 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-leña id(14) unidad Carga (11) Conversión 1 carga =*0.1 m3
        if self.productos == 15 and self.unidad == 11:
            self.cantidad_g = self.cantidad * 0.1
            self.precio_usd_g = float(self.precio_usd) / 0.1

        super(DatosCosecha, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha'


#nuevos modelos para el año 2

class DatosPiso1(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    potencialmente = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_POTENCIALMENTE,
                                    verbose_name='Malezas Potencialmente Dañinos')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Malezas Potencialmente Dañinos'

class DatosPiso2(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    cobertura_noble = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_COBERTURA,
                                    verbose_name='Malezas de Cobertura Nobles')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Malezas de Cobertura Nobles'

class DatosPiso3(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    mulch = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_MULCH,
                                    verbose_name='Mulch de maleza')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Mulch de maleza'

class AccionesManejoPiso(models.Model):
    cuaderno_tres = models.ForeignKey(CuadernoMesTres,
                                                                      on_delete=models.CASCADE)
    manejo_piso = models.IntegerField(choices=CHOICE_MANEJO_PISO)
    meses = MultiSelectField('En qué meses vamos a realizar el manejo(MC)',
                    choices=CHOICE_MESES_ANIO)

    class Meta:
        verbose_name_plural = 'Acciones a realizar para el manejo de piso de cacaotales '
